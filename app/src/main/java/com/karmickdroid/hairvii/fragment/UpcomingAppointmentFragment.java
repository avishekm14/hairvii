package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.ContainerActivity;
import com.karmickdroid.hairvii.activity.MyAppointmentActivity;
import com.karmickdroid.hairvii.adapter.ServiceRecycleAdapter;
import com.karmickdroid.hairvii.adapter.UpcomingAppointmentRecycleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.AppointServiceModel;
import com.karmickdroid.hairvii.model.AppointmentCancelModel;
import com.karmickdroid.hairvii.model.AppointmentViewModel;
import com.karmickdroid.hairvii.model.CancelAppointment;
import com.karmickdroid.hairvii.model.CompleteAppointment;
import com.karmickdroid.hairvii.model.PendingAppointment;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.widgets.AppointmentCancelPopupDialog;
import com.karmickdroid.hairvii.widgets.AppointmentViewPopupDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpcomingAppointmentFragment extends Fragment implements UpcomingAppointmentRecycleAdapter.OnServiceClickListener, AppointmentCancelPopupDialog.CustomDialogCallback {
    View mView;
    RecyclerView upcoming_recycleView;
    List<PendingAppointment> mList;
    private UpcomingAppointmentRecycleAdapter mAdapter;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

    public UpcomingAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_upcoming_appointment, container, false);
        mList = Paper.book().read("pendingAppointmentList");

        init(mView);
        return mView;
    }

    private void init(View mView) {
        upcoming_recycleView = (RecyclerView) mView.findViewById(R.id.upcoming_recycleView);
        upcoming_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        upcoming_recycleView.setLayoutManager(layoutManager);

        mAdapter = new UpcomingAppointmentRecycleAdapter(getActivity(), mList, this);
        upcoming_recycleView.setAdapter(mAdapter);
    }

    @Override
    public void onViewClick(int position) {
        requestViewData(position);
    }

    private void requestViewData(int position) {
        Body.Builder builder = new Body.Builder()
                .add("appointment_id", mList.get(position).getId());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_VIEW)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_view");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<AppointmentViewModel>() {
                                }.getType();

                                String data = responceObject.getJSONObject("Details").toString();
                                AppointmentViewModel viewModel = gson.fromJson(data, listType);

                                Gson gson1 = new Gson();
                                Type listType1 = new TypeToken<List<AppointServiceModel>>() {
                                }.getType();

                                String data1 = responceObject.getJSONObject("Details").getJSONArray("services").toString();
                                List<AppointServiceModel> serviceModelList = gson1.fromJson(data1, listType1);

                                viewModel.setAppointmentServiceList(serviceModelList);

                                viewModel.setCancellation_policy_string(responceObject.getJSONObject("Details").optJSONObject("cancellation_policy"));

                                AppointmentViewPopupDialog dialog = new AppointmentViewPopupDialog();
                                dialog.setModelData(getActivity(), viewModel);
                                dialog.show(getChildFragmentManager(), "AppointmentCancel");

                            } else {
                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onCancelClick(int position) {
        try {
            Date serviceDate = df.parse(mList.get(position).getAppointment_endtime());
            Date todayDate = new Date();
            if(todayDate.compareTo(serviceDate)>0) {

            }else{
                requestCancelData(position);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfirm(final int position) {
        try {
            Date serviceDate = df.parse(mList.get(position).getAppointment_endtime());
            Date todayDate = new Date();
            if(todayDate.compareTo(serviceDate)>0 && !mList.get(position).getIs_complete_customer().equalsIgnoreCase("Y")) {
                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Hairvii", "Do you really want to complete this appointment?","Yes",
                        true,true,new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {
                        requestCompleteAppointment(position);
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void requestCompleteAppointment(int position) {
        UserModel userModel = Paper.book().read("user");

        Body.Builder builder = new Body.Builder()
                .add("customer_id", userModel.getCustomer_id())
                .add("appointment_id", mList.get(position).getId());
        Body bodyRequest = new Body(builder);

        DBG.d("LoginActivity", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_COMPLETE)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_complete");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Hairvii", responceObject.optString("message"), "Ok", true, new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {
                                        ((MyAppointmentActivity) getActivity()).requestJson();
                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });

                            } else {
                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void requestCancelData(int position) {
        Body.Builder builder = new Body.Builder()
                .add("appointment_id", mList.get(position).getId());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_CANCEL_VIEW)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_cancel_view");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<AppointmentCancelModel>() {
                                }.getType();

                                String data = responceObject.getJSONObject("Details").toString();
                                AppointmentCancelModel cancelModel = gson.fromJson(data, listType);

                                AppointmentCancelPopupDialog dialog = new AppointmentCancelPopupDialog();
                                dialog.setOnCustomDialogCallback(UpcomingAppointmentFragment.this,getActivity(), cancelModel);
                                dialog.show(getChildFragmentManager(), "AppointmentCancel");

                            } else {
                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onItemClick(AppointmentCancelModel mModel, String msg) {
        UserModel userModel = Paper.book().read("user");

        Body.Builder builder = new Body.Builder()
                .add("appointment_id", mModel.getAppointment_id())
                .add("customer_id", userModel.getCustomer_id())
                .add("cancellation_reason", msg);
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_CANCEL)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_cancel");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Hairvii", responceObject.optString("message"), "Ok", true, new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {
                                        ((MyAppointmentActivity) getActivity()).requestJson();
                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });

                            } else {
                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }
}
