package com.karmickdroid.hairvii.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.widgets.WebViewDialog;

import io.paperdb.Paper;

/**
 * Created by Administrator on 3/5/2016.
 */
public class MapFragment extends Fragment implements View.OnClickListener {

    private GoogleMap googleMap;
    protected Context context;
    private SupportMapFragment mapFragment;
    private ServiceDetailModel model;
    View mView;
    TextView tv_service_provider_add;
    private EditText gallery;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_map, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        tv_service_provider_add = (TextView) mView.findViewById(R.id.tv_service_provider_add);

        String address ="";

        if(!model.getAddr1().isEmpty()){
            address=address+model.getAddr1();
        }

        if(!model.getAddr1().isEmpty() && !model.getAddr2().isEmpty()){
            address=address+","+model.getAddr2();
        }

        if(model.getAddr1().isEmpty() && !model.getAddr2().isEmpty()){
            address=address+model.getAddr2();
        }

        if(!model.getArea_name().isEmpty())
        address=address+","+model.getArea_name();

        if(!model.getCity().isEmpty())
            address=address+","+model.getCity();

        if(!model.getPincode().isEmpty())
            address=address+",\nZipcode/Pincode"+model.getPincode();

        tv_service_provider_add.setText(""+address);

    }


    @Override
    public void onResume() {
        super.onResume();

        initializeMap();
    }

    /**
     * function to load map If map is not created it will create it for you
     */
    private void initializeMap() {
        DBG.w("init >>>>> ", "initializeMap called");

        googleMap = null;
        if (googleMap == null) {
            mapFragment = SupportMapFragment.newInstance();
            FragmentTransaction fragmentTransaction =
                    getChildFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mapFrame, mapFragment);
            fragmentTransaction.commit();
            // Changed on 02/06/2015
            googleMap = mapFragment.getMap();


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setMapDefaults();
                    updateMarkerOnResume();
                }
            }, 10L);
        }
    }

    private void setMapDefaults() {
        try {
            Log.v("setMapDefaults", "############updateMarkerOnResume called############# " + googleMap);
            if (googleMap == null) {
                googleMap = mapFragment.getMap();
                Log.v("setMapDefaults", "@@@@@@@@@@@updateMarkerOnResume if block @@@@@@@@@@@ " + googleMap);
            }
            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(true);

            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);

            // Enable / Disable my location button
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);

            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(true);

            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);

            // Enable / Disable Rotate blue marker
            // googleMap.getUiSettings().setMyLocationButtonEnabled(false);

            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updateMarkerOnResume() {

        try {
            Log.v("TAG", "############updateMarkerOnResume called############# " + googleMap);
            if (googleMap == null) {
                googleMap = mapFragment.getMap();
                Log.v("TAG", "@@@@@@@@@@@updateMarkerOnResume if block @@@@@@@@@@@ " + googleMap);
            }

            LatLng curentLatLong = new LatLng(Double.parseDouble(model.getLatitude()), Double.parseDouble(model.getLongitude()));
            googleMap.addMarker(new MarkerOptions()
                    .position(curentLatLong)
                    .title("We are here"));
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            float zoomLevel = 16.0f;
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curentLatLong, zoomLevel));
            googleMap.getUiSettings().setScrollGesturesEnabled(true);

        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            DBG.printStackTrace(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    public void showTermsDialog(String title, String message){

        WebViewDialog.DialogBuiler gDialog = new WebViewDialog.DialogBuiler();
        gDialog.setContext(getActivity())
                .setServiceList(message)
                .showDialog();
    }
}
