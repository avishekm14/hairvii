package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.ContainerActivity;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment implements RequestCallback {
    View mView;
    WebView browser;

    public FaqFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_faq, container, false);
        init();
        return mView;
    }

    private void init() {
        browser = (WebView) mView.findViewById(R.id.terms);
        browser.getSettings().setJavaScriptEnabled(true);

        if(((ContainerActivity)getActivity()).isDataAvailable()) {
            requesServer();
        }else{
            ((ContainerActivity)getActivity()).showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((ContainerActivity)getActivity()).setHeader("How It Works");
    }

    private void requesServer() {
        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.FAQ)
                .fromString()
                .execute(getActivity(), "HomeFragment", this);
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("showpage");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
               JSONObject jObj = responceObject.getJSONObject("Details");
               String web_content = jObj.optString("page_content");

               browser.loadData(web_content, "text/html; charset=UTF-8", null);
            } else {
                ((ContainerActivity)getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }
}
