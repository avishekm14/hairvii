package com.karmickdroid.hairvii.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.activity.SelectLoginActivity;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import io.paperdb.Paper;

/**
 * Created by Administrator on 3/5/2016.
 */
public class RatingFragment extends Fragment implements RequestCallback {

    RatingBar overall_rating, panctual_rating, value_rating, service_rating;
    EditText et_your_review;
    Button btn_book_now;
    View mView;
    private ServiceDetailModel model;
    private UserModel userModel;

    public RatingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_rating, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        overall_rating = (RatingBar) mView.findViewById(R.id.overall_rating);
        panctual_rating = (RatingBar) mView.findViewById(R.id.panctual_rating);
        value_rating = (RatingBar) mView.findViewById(R.id.value_rating);
        service_rating = (RatingBar) mView.findViewById(R.id.service_rating);
        et_your_review = (EditText) mView.findViewById(R.id.et_your_review);
        btn_book_now = (Button) mView.findViewById(R.id.btn_book_now);

        btn_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userModel = Paper.book().read("user");
                ((DetailActivity)getActivity()).setHideSoftKeyboard();

                if (userModel != null) {
                    if (checkForRating()) {
                        sendRequest();
                    }
                } else {
                    Intent loginFirst = new Intent(getActivity(), SelectLoginActivity.class);
                    startActivity(loginFirst);
                }
            }
        });
    }

    private boolean checkForRating() {
        if(overall_rating.getRating()==0){
            Toast.makeText(getContext(), "Please Rate Overall Service",Toast.LENGTH_LONG).show();
            return false;
        }else if(panctual_rating.getRating()==0){
            Toast.makeText(getContext(), "Please Rate Punctuality",Toast.LENGTH_LONG).show();
            return false;
        }else if(value_rating.getRating()==0){
            Toast.makeText(getContext(), "Please Rate Value Of Money",Toast.LENGTH_LONG).show();
            return false;
        }else if(service_rating.getRating()==0){
            Toast.makeText(getContext(), "Please Rate Service Provided",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private void sendRequest() {
        userModel = Paper.book().read("user");

        Body.Builder builder = new Body.Builder()
                .add("customer_id", "" + userModel.getCustomer_id())
                .add("business_id", "" + model.getId())
                .add("review_note", "" + et_your_review.getText().toString().trim())
                .add("punctuality_rate", "" + (int) panctual_rating.getRating())
                .add("value_rate", ""+(int)value_rating.getRating())
                .add("service_rate", ""+(int)service_rating.getRating())
                .add("overall_rating", ""+(int)overall_rating.getRating())
                .add("employee_id", "0");
        Body bodyRequest = new Body(builder);

        DBG.d("LoginActivity", userModel.getCustomer_id()+" ,"+model.getId()+" ,"+(int)panctual_rating.getRating()
        +" ,"+(int)value_rating.getRating()+" ,"+(int)service_rating.getRating()+" ,"+(int)overall_rating.getRating()+" ,"+ et_your_review.getText().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.RATING)
                .fromString()
                .execute((DetailActivity)getActivity(), "RatingFragment", this);
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));


        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("rate_business");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                resetData();
                Paper.book().write("require_update", true);

                ((DetailActivity)getActivity()).showCustomeDialog("Hairvii", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {
                        ((DetailActivity)getActivity()).requestDetail();
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            } else {
                ((DetailActivity)getActivity()).showCustomeDialog("Hairvii", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            ((DetailActivity)getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    private void resetData(){
        et_your_review.setText("");
        overall_rating.setRating(0);
        value_rating.setRating(0);
        service_rating.setRating(0);
        panctual_rating.setRating(0);
    }

}
