package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.ReviewRecycleAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;

import java.util.ArrayList;

import io.paperdb.Paper;

public class ReviewFragment extends Fragment {
    View mView;
    RecyclerView review_layout;
    ReviewRecycleAdapter mAdapter;
    private ServiceDetailModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_review, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        review_layout = (RecyclerView) mView.findViewById(R.id.review_layout);
        review_layout.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        review_layout.setLayoutManager(layoutManager);
        mAdapter = new ReviewRecycleAdapter(getActivity(), model.getReviewList());
        review_layout.setAdapter(mAdapter);
    }

}
