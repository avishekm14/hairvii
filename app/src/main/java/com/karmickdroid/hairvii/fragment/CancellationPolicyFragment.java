package com.karmickdroid.hairvii.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.CancelPolicyLinearAdapter;
import com.karmickdroid.hairvii.adapter.OperationSlotLinearAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class CancellationPolicyFragment extends Fragment {
    View mView;
    private ServiceDetailModel model;
    private CancelPolicyLinearAdapter mAdapter;
    private AdapterLinearLayout cancel_list;

    public CancellationPolicyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_cancellation_policy, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        cancel_list = (AdapterLinearLayout) mView.findViewById(R.id.cancel_list);
        mAdapter = new CancelPolicyLinearAdapter(getActivity(), model.getCancellation_policies());
        cancel_list.setAdapter(mAdapter);
    }

}
