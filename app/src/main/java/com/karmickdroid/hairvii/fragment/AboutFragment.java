package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.adapter.FeatureLinearAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.util.HairType;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.util.List;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {
    View mView;
    private ServiceDetailModel model;
    WebView browser;
    RelativeLayout features, opening_hour, cancellation_policy;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_about, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        browser = (WebView) mView.findViewById(R.id.terms);
        features = (RelativeLayout) mView.findViewById(R.id.features);
        opening_hour = (RelativeLayout) mView.findViewById(R.id.opening_hour);
        cancellation_policy = (RelativeLayout) mView.findViewById(R.id.cancellation_policy);

        if(!model.getAbout_note().isEmpty()) {
            browser.getSettings().setJavaScriptEnabled(true);
            browser.loadData(model.getAbout_note(), "text/html; charset=UTF-8", null);
        }else{
            browser.setVisibility(View.GONE);
        }

        features.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailActivity)getActivity()).changeFragment(new FeatureFragment(), 0, 0, "FeatureFragment");
            }
        });

        opening_hour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailActivity)getActivity()).changeFragment(new OperationSlotFragment(), 0, 0, "OperationSlotFragment");
            }
        });

        cancellation_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DetailActivity)getActivity()).changeFragment(new CancellationPolicyFragment(), 0, 0, "CancellationPolicyFragment");
            }
        });
    }

}
