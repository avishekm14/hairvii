package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.GalleryAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.widgets.AdapterHorizontalLinearLayout;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment {

    View mView;
    AdapterHorizontalLinearLayout linearLayout;
    GalleryAdapter mAdapter;
    private ServiceDetailModel model;

    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_gallery, container, false);
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        init(mView);
        return mView;
    }

    private void init(View mView) {
        linearLayout = (AdapterHorizontalLinearLayout) mView.findViewById(R.id.gallery_items);

        mAdapter = new GalleryAdapter(getActivity(), model.getGalleryList());
        linearLayout.setAdapter(mAdapter);
    }

}
