package com.karmickdroid.hairvii.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.FeatureLinearAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.util.HairType;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.util.List;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeatureFragment extends Fragment {
    View mView;
    TextView weblink, phone, licence_no, licence_reg_state, kid_friendly, walkin, parking_available,
            handicap_access, tv_available, beer_and_wine, home_service;
    AdapterLinearLayout business_type, hair_type;
    FeatureLinearAdapter mAdapter;
    private List<HairType> hairTypeList;
    private ServiceDetailModel model;

    public FeatureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        hairTypeList = Paper.book().read("hairtype");

        mView = inflater.inflate(R.layout.fragment_feature, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        weblink = (TextView) mView.findViewById(R.id.weblink);
        phone = (TextView) mView.findViewById(R.id.phone);
        licence_no = (TextView) mView.findViewById(R.id.licence_no);
        licence_reg_state = (TextView) mView.findViewById(R.id.licence_reg_state);
        kid_friendly = (TextView) mView.findViewById(R.id.kid_friendly);
        walkin = (TextView) mView.findViewById(R.id.walkin);
        parking_available = (TextView) mView.findViewById(R.id.parking_available);
        handicap_access = (TextView) mView.findViewById(R.id.handicap_access);
        tv_available = (TextView) mView.findViewById(R.id.tv_available);
        beer_and_wine = (TextView) mView.findViewById(R.id.beer_and_wine);
        home_service = (TextView) mView.findViewById(R.id.home_service);

        business_type = (AdapterLinearLayout) mView.findViewById(R.id.business_type);
        hair_type = (AdapterLinearLayout) mView.findViewById(R.id.hair_type);

        mAdapter = new FeatureLinearAdapter(getActivity(), true, model.getServicesList(),hairTypeList);
        business_type.setAdapter(mAdapter);

        mAdapter = new FeatureLinearAdapter(getActivity(), false, model.getServicesList(),hairTypeList);
        hair_type.setAdapter(mAdapter);

        weblink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                if (!model.getWebsite().startsWith("http://") && !model.getWebsite().startsWith("https://"))
                i.setData(Uri.parse("http://" + model.getWebsite()));
                else {
                    i.setData(Uri.parse("" + model.getWebsite()));
                }
                startActivity(i);
            }
        });

        phone.setText("" + model.getMobile_no());
        licence_no.setText(""+model.getLicense_no());
        licence_reg_state.setText(""+model.getLicense_reg_state());

        kid_friendly.setText("" + getStringValue(model.getKids_friendly()));
        walkin.setText(""+getStringValue(model.getAccept_walkin()));
        parking_available.setText(""+getStringValue(model.getParking_available()));
        handicap_access.setText(""+getStringValue(model.getHandicap_access()));
        tv_available.setText(""+getStringValue(model.getTv()));
        beer_and_wine.setText(""+getStringValue(model.getBeer_n_wine_bar()));

        if(model.getExtra_hours_flag().equalsIgnoreCase("1")){
            home_service.setText("Yes");
        }else{
            home_service.setText("No");
        }
    }

    private String getStringValue(String str){
        if(str.equalsIgnoreCase("y")){
            return "Yes";
        }else{
            return "No";
        }
    }

}
