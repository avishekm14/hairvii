package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.adapter.ServicesLinearAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment implements ServicesLinearAdapter.OnItemClickListener {
    View mView;
    AdapterLinearLayout services_list;
    ServicesLinearAdapter mAdapter;
    private ServiceDetailModel model;
    List<Services> servicesList = new ArrayList<>();
    private TextView errorText;

    public ServicesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.fragment_services, container, false);
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        init(mView);
        return mView;
    }

    private void init(View mView) {
        services_list = (AdapterLinearLayout) mView.findViewById(R.id.services_list);
        errorText = (TextView) mView.findViewById(R.id.errorText);
        servicesList = model.getServicesList();

        if(servicesList.size()>0) {
            errorText.setVisibility(View.GONE);

            mAdapter = new ServicesLinearAdapter(getActivity(), servicesList);
            mAdapter.setOnItemClickListener(this);
            services_list.setAdapter(mAdapter);
        }else{
            errorText.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onItemClick(int position) {

        for(int i=0;i<servicesList.size();i++){
            Services services = servicesList.get(i);
            if(i==position){
                services.setFlag(true);
            }else{
                services.setFlag(false);
            }
        }

        mAdapter.notifyDataChanged(servicesList);
    }

    @Override
    public void onBookNow(String id, String name) {
        BookAppointmentFragment frag = new BookAppointmentFragment();
        Bundle bundle = new Bundle();
        bundle.putString("staff_name", "");
        bundle.putString("staff_id", "");
        bundle.putString("services_name", name);
        bundle.putString("services_id", id);
        frag.setArguments(bundle);
        ((DetailActivity) getActivity()).changeFragment(frag, 0, 0, "BookAppointmentFragment");
    }


}
