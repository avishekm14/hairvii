package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.ContainerActivity;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.CONST;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogFragment extends Fragment {
    View mView;
    WebView browser;
    private UserModel userModel;

    public BlogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_blog, container, false);
        userModel = Paper.book().read("user");
        init(mView);
        return mView;
    }

    private void init(View mView) {
        browser = (WebView) mView.findViewById(R.id.terms);
        browser.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        browser.getSettings().setJavaScriptEnabled(true);
        browser.loadUrl(CONST.REST_API.BLOG + userModel.getCustomer_id());
    }

    @Override
    public void onResume() {
        super.onResume();

        ((ContainerActivity)getActivity()).setHeader("Blog");
    }
}
