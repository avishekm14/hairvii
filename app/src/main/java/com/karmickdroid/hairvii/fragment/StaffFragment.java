package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.adapter.OurStaffRecycleAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class StaffFragment extends Fragment implements OurStaffRecycleAdapter.OnServiceListClickListener {
    View mView;
    RecyclerView our_staff_recycleView;
    OurStaffRecycleAdapter mAdapter;
    private ServiceDetailModel model;
    private TextView errorText;

    public StaffFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView= inflater.inflate(R.layout.fragment_staff, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        our_staff_recycleView = (RecyclerView) mView.findViewById(R.id.our_staff_recycleView);
        errorText = (TextView) mView.findViewById(R.id.errorText);

        our_staff_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        our_staff_recycleView.setLayoutManager(layoutManager);

        if(model.getStaffsList().size()>0) {
            errorText.setVisibility(View.GONE);

            mAdapter = new OurStaffRecycleAdapter(getActivity(), model.getStaffsList(), this);
            our_staff_recycleView.setAdapter(mAdapter);
        }else{
            errorText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onServiceClick(int position) {
        BookAppointmentFragment frag = new BookAppointmentFragment();
        Bundle bundle = new Bundle();
        bundle.putString("staff_name", model.getStaffsList().get(position).getEmployee_name());
        bundle.putString("staff_id", model.getStaffsList().get(position).getId());
        bundle.putString("services_name", "");
        bundle.putString("services_id", "");
        frag.setArguments(bundle);
        ((DetailActivity) getActivity()).changeFragment(frag, 0, 0, "BookAppointmentFragment");
    }

    @Override
    public void onCall(int position) {

    }

}
