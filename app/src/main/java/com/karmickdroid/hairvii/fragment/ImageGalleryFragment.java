package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.ContainerActivity;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.adapter.OurStaffRecycleAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageGalleryFragment extends Fragment  {
    View mView;
    ViewPager gallery_items;
    ScreenSlidePagerAdapter mAdapter;
    private ServiceDetailModel model;
    private ImageView iv_left, iv_right;
    private TextView errorText;

    public ImageGalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView= inflater.inflate(R.layout.fragment_gallery_exp, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        gallery_items = (ViewPager) mView.findViewById(R.id.gallery_items);
        iv_left = (ImageView) mView.findViewById(R.id.iv_left);
        iv_right = (ImageView) mView.findViewById(R.id.iv_right);
        errorText = (TextView) mView.findViewById(R.id.errorText);

        if(model.getGalleryList().size()==0){
            iv_left.setVisibility(View.GONE);
            iv_right.setVisibility(View.GONE);

            errorText.setVisibility(View.VISIBLE);
        }else{
            iv_left.setVisibility(View.VISIBLE);
            iv_right.setVisibility(View.VISIBLE);
            errorText.setVisibility(View.GONE);

            mAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
            gallery_items.setAdapter(mAdapter);

            iv_left.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (gallery_items.getCurrentItem() > 0)
                        gallery_items.setCurrentItem(gallery_items.getCurrentItem() - 1, true);
                    else
                        gallery_items.setCurrentItem(model.getGalleryList().size() - 1, true);
                }
            });

            iv_right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (gallery_items.getCurrentItem() < model.getGalleryList().size() - 1)
                        gallery_items.setCurrentItem(gallery_items.getCurrentItem() + 1, true);
                    else
                        gallery_items.setCurrentItem(0, true);
                }
            });
        }

    }

    /**
     * A simple pager adapter that represents (NUM_OF_IMAGE) ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ImageFragment imageFragment= new ImageFragment();
            imageFragment.setGalleryResource(getActivity(),model.getGalleryList().get(position));
            return imageFragment;
        }

        @Override
        public int getCount() {
            return model.getGalleryList().size();
        }
    }
}
