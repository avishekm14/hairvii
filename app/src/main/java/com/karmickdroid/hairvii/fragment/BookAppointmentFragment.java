package com.karmickdroid.hairvii.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.AppointmentActivity;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.activity.SelectLoginActivity;
import com.karmickdroid.hairvii.adapter.SpinnerAdapter;
import com.karmickdroid.hairvii.adapter.SpinnerSimpleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.Staffs;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.model.TimeSlot;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.widgets.CustomTimeSlotDialog;
import com.karmickdroid.hairvii.widgets.CustomTimeSlotValidationDialog;
import com.karmickdroid.hairvii.widgets.PickerDialog;
import com.karmickdroid.hairvii.widgets.SearchKeyValue;
import com.karmickdroid.hairvii.widgets.SearchSpinner;
import com.karmickdroid.hairvii.widgets.SearchSpinnerOnTop;
import com.karmickdroid.hairvii.widgets.SearchSpinnerWithCheckbox;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import io.paperdb.Paper;


public class BookAppointmentFragment extends Fragment implements View.OnClickListener, CustomTimeSlotValidationDialog.OnSubmitClickListener, CustomTimeSlotDialog.OnSubmitClickListener, SpinnerAdapter.CustomSearchDialogCallback {
    View mView;
    EditText et_book_date;
    Spinner et_services, et_service_provider;
    Button btn_search;
    private ServiceDetailModel model;
    private String serviceProviderId = "";
    private String serviceProviderName = "";
    private String serviceId = "";
    private String serviceName = "";
    private List<TimeSlot> timeSlots;
    private SearchSpinnerWithCheckbox sObj;
    private SpinnerAdapter mSpinnerAdapter;
    private SpinnerSimpleAdapter mSpinnerAdapter1;
    List<Staffs> staffsList;

    public BookAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_book_appointment, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        if (getArguments() != null) {
            serviceProviderName = getArguments().getString("staff_name");
            serviceProviderId = getArguments().getString("staff_id");
            serviceName = getArguments().getString("services_name");
            serviceId = getArguments().getString("services_id");
        }

        et_book_date = (EditText) mView.findViewById(R.id.et_book_date);
        et_service_provider = (Spinner) mView.findViewById(R.id.et_service_provider);
        et_services = (Spinner) mView.findViewById(R.id.et_services);
        btn_search = (Button) mView.findViewById(R.id.btn_search);

        int i=0;
        List<Services> servicesList = model.getServicesList();
        ArrayList<SubServices> keyValList = new ArrayList<>();

        if (null != servicesList && !servicesList.isEmpty()) {
            for (Services business : servicesList) {
                if(!serviceId.isEmpty()) {
                    for (SubServices subServices : business.getSubServicesList()) {
                        if (subServices.getId().equalsIgnoreCase(serviceId)) {
                            subServices.setChecked(true);
                        }
                        keyValList.add(subServices);
                    }
                }else {
                    for (SubServices subServices : business.getSubServicesList()) {
                        if (i==0) {
                            /*subServices.setChecked(true);
                            serviceId= subServices.getId();
                            serviceName= subServices.getService_name();*/
                        }
                        i++;
                        keyValList.add(subServices);
                    }
                }
            }
        }

        staffsList = model.getStaffsList();
        final List<Staffs> updateStaffList = new ArrayList<>();
        Staffs staffs = new Staffs();
        if (!serviceProviderId.isEmpty()) {
            staffs.setId(serviceProviderId);
            staffs.setEmployee_name(serviceProviderName);
            updateStaffList.add(staffs);
        }else{
            staffs.setId("");
            staffs.setEmployee_name("Select Service Provider");
            updateStaffList.add(staffs);
        }
        updateStaffList.addAll(staffsList);

        mSpinnerAdapter = new SpinnerAdapter(getActivity(),
                R.layout.row_spinner, serviceName, keyValList, this);
        et_services.setAdapter(mSpinnerAdapter);

        mSpinnerAdapter1 = new SpinnerSimpleAdapter(getActivity(),
                R.layout.row_spinner, serviceProviderName, updateStaffList);
        et_service_provider.setAdapter(mSpinnerAdapter1);

        et_service_provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    serviceProviderId = updateStaffList.get(position).getId();
                }

                DBG.d("AAAAAA", "" + serviceProviderId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_book_date.setOnClickListener(this);
        //et_service_provider.setOnClickListener(this);
        //et_services.setOnClickListener(this);
        btn_search.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_book_date:
                DatePickerDialog dialog = showDatePickerDialog();
                dialog.show();
                break;
            case R.id.et_service_provider:
                //showServiceProviderList();
                break;
            case R.id.et_services:
                //showServicesList();
                break;
            case R.id.btn_search:
                UserModel userModel = Paper.book().read("user");
                if (userModel != null) {
                    if (checkError())
                        getTimeSlot();
                } else {
                    Intent loginFirst = new Intent(getActivity(), SelectLoginActivity.class);
                    startActivity(loginFirst);
                }
                break;
        }
    }

    private boolean checkError() {
        if (((DetailActivity) getActivity()).getTrim(R.id.et_book_date).isEmpty()) {
            Toast.makeText(getActivity(), "Select A Date First To Proceed.", Toast.LENGTH_SHORT).show();
            return false;
        } else if (serviceId.isEmpty()) {
            Toast.makeText(getActivity(), "Select At Least One Service To Proceed.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private DatePickerDialog showDatePickerDialog() {
        try {

            DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

                // when dialog box is closed, below method will be called.
                public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                    String year1 = String.valueOf(selectedYear);
                    String month1 = String.valueOf(selectedMonth + 1);
                    String day1 = String.valueOf(selectedDay);

                    et_book_date.setText(year1 + "-" + month1 + "-" + day1);
                }
            };

            Calendar cal = Calendar.getInstance(TimeZone.getDefault());
            DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                    R.style.AppTheme,
                    datePickerListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));

            datePicker.setCancelable(false);
            datePicker.setTitle("Select the date");
            datePicker.getDatePicker().setMinDate(cal.getTimeInMillis());

            return datePicker;
        } catch (Exception e) {

        }
        return null;
    }


    public void showServiceProviderList() {
        List<Staffs> staffsList = model.getStaffsList();

        /*List<ListModel> modelList = new ArrayList<>();
        for (ListModel mModel : businessList) {
            if (Integer.parseInt(mModel.getParent_id()) != 0) {
                modelList.add(mModel);
            }
        }*/

        ArrayList<SearchKeyValue> keyValList = new ArrayList<>();

        if (null != staffsList && !staffsList.isEmpty()) {
            for (Staffs business : staffsList) {
                keyValList.add(new SearchKeyValue("" + business.getId(), "" + business.getEmployee_name()));
            }

            SearchSpinner sObj = new SearchSpinner();
            sObj.searchSpinner(getActivity(), et_service_provider, keyValList, false, new SearchSpinner.CustomSearchDialogCallback() {
                @Override
                public void onItemClick(SearchKeyValue svObj) {
                    serviceProviderId = svObj.getKey();
                }
            });
        }
    }

    public void showServicesList() {
        if (sObj == null) {
            List<Services> servicesList = model.getServicesList();
            ArrayList<SubServices> keyValList = new ArrayList<>();
            if (null != servicesList && !servicesList.isEmpty()) {
                for (Services business : servicesList) {
                    for (SubServices subServices : business.getSubServicesList()) {
                        if (subServices.getId().equalsIgnoreCase(serviceId)) {
                            subServices.setChecked(true);
                        }
                        keyValList.add(subServices);
                    }
                }

                sObj = new SearchSpinnerWithCheckbox();
                sObj.searchSpinner(getActivity(), et_services, keyValList, false, new SearchSpinnerWithCheckbox.CustomSearchDialogCallback() {
                    @Override
                    public void onItemClick(String services, String servicesIds) {
                        //et_services.setText("" +services);
                        serviceId = servicesIds;
                    }
                });
            }
        } else {
            sObj.show(getActivity());
        }
    }

    public void getTimeSlot() {
        UserModel userModel = Paper.book().read("user");
        if (serviceProviderId.isEmpty()) {
            serviceProviderId = "0";
        }

        Body.Builder builder = new Body.Builder()
                .add("customer_id", userModel.getCustomer_id())
                .add("appo_dt", et_book_date.getText().toString())
                .add("services", serviceId)
                .add("employee_id", serviceProviderId)
                .add("timezone", TimeZone.getDefault().getID())
                .add("business_id", model.getId());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.TIME_SLOT)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("get_available_timeslots");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<TimeSlot>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                timeSlots = gson.fromJson(data, listType);

                                if (timeSlots.size() > 0) {
                                    String flag = responceObject.optString("home_service_business");
                                    CustomTimeSlotDialog dialog = new CustomTimeSlotDialog();
                                    dialog.setOnSubmitClickListener(timeSlots,flag, BookAppointmentFragment.this);
                                    dialog.setCancelable(false);
                                    dialog.show(getChildFragmentManager(), "CustomTimeSlotDialog");
                                } else {
                                    ((DetailActivity) getActivity()).showCustomeDialog("Hairvii", "No Available Time Slot Is Present.", new BaseActivity.OnOkCancelListner() {
                                        @Override
                                        public void onOk() {

                                        }

                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                                }
                            } else {
                                ((DetailActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((DetailActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onClick(int i) {
        UserModel userModel = Paper.book().read("user");
        if (serviceProviderId.isEmpty()) {
            serviceProviderId = "0";
        }

        DBG.d("AAAAAA", "appo_time" + timeSlots.get(i).getTime() + " extra_time" + timeSlots.get(i).getExtra_time()+ " timezone"+TimeZone.getDefault().getID());

        Body.Builder builder = new Body.Builder()
                .add("customer_id", userModel.getCustomer_id())
                .add("appo_dt", et_book_date.getText().toString())
                .add("timezone", TimeZone.getDefault().getID())
                .add("services", serviceId)
                .add("employee_id", timeSlots.get(i).getEmployee_id())
                .add("appo_time", timeSlots.get(i).getTime())
                .add("business_id", model.getId());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.VALIDATE_TIME_SLOT)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("validate_appotime");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                CustomTimeSlotValidationDialog dialog = new CustomTimeSlotValidationDialog();
                                dialog.setOnSubmitClickListener(BookAppointmentFragment.this, ((String) o));
                                dialog.setCancelable(false);
                                dialog.show(getChildFragmentManager(), "Appointment");

                            } else {
                                ((DetailActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((DetailActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onClick(String json, String comment, String Address) {
        Intent nextPage = new Intent(getActivity(), AppointmentActivity.class);
        nextPage.putExtra("json", json);
        nextPage.putExtra("comment", comment);
        nextPage.putExtra("address", Address);
        startActivity(nextPage);
        getActivity().overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
        getActivity().finish();
    }

    @Override
    public void onItemClick(String Service, String ids) {
        serviceName = Service;
        serviceId = ids;
        DBG.d("AAAAAA", "" + ids);
    }

}
