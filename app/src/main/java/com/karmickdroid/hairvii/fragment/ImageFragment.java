package com.karmickdroid.hairvii.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Gallery;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Administrator on 10/13/2015.
 * this fragment is use to display product image in view pager
 * <p/>
 * it get imageId from ProductDiscriptionActivty as argument
 */
public class ImageFragment extends Fragment {
    View mCurrenView;
    ImageView gallery_img = null;
    Gallery gallery;
    Context ctx;

    public ImageFragment() {

    }

    public void setGalleryResource(Context context, Gallery gallery){
        this.gallery = gallery;
        this.ctx = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mCurrenView = inflater.inflate(R.layout.gallery_img, null);
        gallery_img = (ImageView) mCurrenView.findViewById(R.id.iv_profile_pic);

        boolean flag = false;

        final TextView txtTitle = (TextView) mCurrenView.findViewById(R.id.tv_description);

        URI uri = null;
        try {
            uri = new URI(gallery.getGallery_image().replaceAll(" ", "%20"));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (!uri.toString().isEmpty())
            Picasso.with(ctx).load(uri.toString()).into(gallery_img);
        else
            Picasso.with(ctx).load(R.drawable.banner).into(gallery_img);

        if (gallery.getLinked_to_service().equalsIgnoreCase("y")) {
            final String strq = gallery.getService_description() + "[" + gallery.getService_online_price() + "]\n" + gallery.getDescription();
            String str = "";
            if (strq != null && !strq.isEmpty()) {
                if (strq.length() >= 40) {
                    str = strq.substring(0, 38) + "...";
                } else {
                    str = strq;
                }
            }

            if (str != null && !str.isEmpty()) {
                txtTitle.setText("" + str);
            } else {
                txtTitle.setVisibility(View.GONE);
            }

            txtTitle.setTag(true);
            txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean flag = (Boolean) ((TextView) v).getTag();
                    if (flag) {
                        ((TextView) v).setTag(false);
                        txtTitle.setText("" + gallery.getService_description() + "[" + gallery.getService_online_price() + "]\n" + gallery.getDescription());
                    } else {
                        ((TextView) v).setTag(true);
                        final String strq = gallery.getService_description() + "[" + gallery.getService_online_price() + "]\n" + gallery.getDescription();
                        String str = "";
                        if (strq != null && !strq.isEmpty()) {
                            if (strq.length() >= 40) {
                                str = strq.substring(0, 38) + "...";
                            } else {
                                str = strq;
                            }
                        }

                        txtTitle.setText("" + str);
                    }
                }
            });
        } else {

            if (gallery.getImage_title() != null && !gallery.getImage_title().isEmpty()) {
                txtTitle.setText("" + gallery.getImage_title());
            } else {
                txtTitle.setVisibility(View.GONE);
            }

        }

        return mCurrenView;
    }

}
