package com.karmickdroid.hairvii.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.ContainerActivity;
import com.karmickdroid.hairvii.activity.NearByActivity;
import com.karmickdroid.hairvii.activity.ServiceDetailActivity;
import com.karmickdroid.hairvii.adapter.ServiceRecycleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.AdsModel;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.model.NearBy;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.SpecialModel;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.paperdb.Paper;

public class HomeFragment extends Fragment implements ServiceRecycleAdapter.OnServiceClickListener,
        RequestCallback {

    RecyclerView service_recycleView;
    ServiceRecycleAdapter mAdapter;
    View mView;
    List<ListModel> listModels;
    List<SpecialModel> specialList;
    LocationManager locationManager;
    ProgressDialog PD;
    LocationListener locationListener;
    int service_women = 0, service_men = 0, service_kids = 0, category_id = -1;
    int lastClickLocation = 0;
    boolean nearbyFlag = false;
    List<ServiceDetailModel> mModelList = new ArrayList<>();
    private List<AdsModel> adsList;
    Timer t;
    ImageView ads;
    private int i = 0;
    private String link = "";
    private String ad_id = "";
    StringBuilder ads_id = new StringBuilder("");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.activity_home, container, false);
        UserModel mUser = Paper.book().read("user");
        if(mUser!=null){
            saveDeviceId(mUser.getCustomer_id());
        }
        AppUtilities.setContext(getContext());
        init();
        return mView;
    }

    private void init() {
        listModels = new ArrayList<>();
        specialList = new ArrayList<>();

        ads = (ImageView) mView.findViewById(R.id.ads);
        ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (link != null && !link.isEmpty())
                    openBrowser(link);
            }
        });
        service_recycleView = (RecyclerView) mView.findViewById(R.id.service_recycleView);
        service_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        service_recycleView.setLayoutManager(layoutManager);

        mAdapter = new ServiceRecycleAdapter(getActivity(), listModels, specialList, this);
        service_recycleView.setAdapter(mAdapter);

        if (((ContainerActivity) getActivity()).isDataAvailable()) {
            if (Paper.book().read("ads") != null) {
                setAds();
            }
            doRequestList();
        } else {
            ((ContainerActivity) getActivity()).showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onServiceClick(int position) {
        Paper.book().write("model", listModels.get(position));
        Paper.book().write("normal_model", true);

        lastClickLocation = position;

        if (((ContainerActivity) getActivity()).checkLocationService()) {
            checkLocation();
        } else {
            ((ContainerActivity) getActivity()).showCustomeDialog("Turn on the location service", "This app needs to know your current location.",
                    "Allow", true, new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {
                            ((ContainerActivity) getActivity()).openSettings();
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }

    }

    @Override
    public void onSpecialClick(int position) {

        /*if (position == 4) {
            nearbyFlag = true;
        } else {*/
            Paper.book().write("model", specialList.get(position));
            Paper.book().write("normal_model", false);

            lastClickLocation = position;
        //}

        if (((ContainerActivity) getActivity()).checkLocationService()) {
            checkLocation();
        } else {
            ((ContainerActivity) getActivity()).showCustomeDialog("Turn on the location service", "This app needs to know your current location.",
                    "Allow", true, new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {
                            ((ContainerActivity) getActivity()).openSettings();
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }

    }

    private void getNearByLocations() {

        Body.Builder builder = new Body.Builder()
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.NEARBY_PLACE)
                .fromString()
                .execute(getActivity(), "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("nearby");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<NearBy>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                List<NearBy> nearByList = gson.fromJson(data, listType);

                                if (nearByList.size() > 0) {
                                    Paper.book().write("nearby_list", nearByList);

                                    Intent nextPage = new Intent(getActivity(), NearByActivity.class);
                                    startActivity(nextPage);
                                    getActivity().overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                                } else {
                                    ((ContainerActivity) getActivity()).showCustomeDialog("Hairvii", "Currently There Is No List Available", new BaseActivity.OnOkCancelListner() {
                                        @Override
                                        public void onOk() {

                                        }

                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                                }

                            } else {
                                ((ContainerActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void changeActivity() {
        Intent nextPage = new Intent(getActivity(), ServiceDetailActivity.class);

        if (Paper.book().read("normal_model")) {
            nextPage.putExtra("title", "" + listModels.get(lastClickLocation).getCategory_name());
        } else {
            nextPage.putExtra("title", "" + specialList.get(lastClickLocation).getCategory_name());
        }

        startActivity(nextPage);
        getActivity().overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
    }

    public void doRequestList() {
        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.LIST)
                .fromString()
                .execute(getActivity(), "HomeFragment", this);
    }

    public void saveDeviceId(String id) {
        DBG.d("AAAAAA", ads_id.toString() + " responce" + ((ContainerActivity) getActivity()).getIpAddress());
        Body.Builder builder = new Body.Builder()
                .add("customer_id", "" + id)
                .add("device_id", "" + Paper.book().read("token"))
                .add("device_type", "A");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.SET_DEVICE_ID)
                .fromString(false)
                .execute(getActivity(), "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("AAAAAA", "responce" + ((String) o));
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    public void saveAdsImpression() {
        DBG.d("AAAAAA", ads_id.toString() + " responce" + ((ContainerActivity) getActivity()).getIpAddress());
        Body.Builder builder = new Body.Builder()
                .add("ip_address", "" + ((ContainerActivity) getActivity()).getIpAddress())
                .add("imp_date", "" + new SimpleDateFormat("yyyy/MM/dd").format(new Date()))
                .add("addid", "" + ads_id.toString());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.ADS_IMPRESSION)
                .fromString(false)
                .execute(getActivity(), "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("AAAAAA", "responce" + ((String) o));

                        ads_id = new StringBuilder("");
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    public void saveAdsCount(String ad_id, final String str) {
        DBG.d("AAAAAA", "responce" + ad_id);

        Body.Builder builder = new Body.Builder()
                .add("ip_address", "" + ((ContainerActivity) getActivity()).getIpAddress())
                .add("ad_id", "" + ad_id);
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.ADS_CLICK)
                .fromString(false)
                .execute(getActivity(), "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("AAAAAA", "responce" + ((String) o));
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("list");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                Gson gson = new Gson();
                Type listType = new TypeToken<List<ListModel>>() {
                }.getType();

                String data = responceObject.getJSONArray("Details").toString();
                listModels = gson.fromJson(data, listType);

                Paper.book().write("business", listModels);

                List<ListModel> modelList = new ArrayList<>();
                for (ListModel mModel : listModels) {
                    if (Integer.parseInt(mModel.getPin_to_mobile_app()) == 1) {
                        modelList.add(mModel);
                    }
                }
                listModels = modelList;
                createSpecialList();
                mAdapter.notifyDataChange(modelList, specialList);

                if (Paper.book().read("ads") == null) {
                    loadAdd();
                }

            } else {
                ((ContainerActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createSpecialList() {
        specialList = new ArrayList<>();

        SpecialModel specialModel3 = new SpecialModel();
        specialModel3.setId("903");
        specialModel3.setCategory_name(AppUtilities.getStringFromResource(R.string.men_salon));
        specialModel3.setCategory_icon(R.drawable.men_img);
        specialModel3.setServiceType(AppUtilities.getStringFromResource(R.string.men_salon));

        specialList.add(specialModel3);

        SpecialModel specialModel1 = new SpecialModel();
        specialModel1.setId("901");
        specialModel1.setCategory_name(AppUtilities.getStringFromResource(R.string.women));
        specialModel1.setCategory_icon(R.drawable.women_img);
        specialModel1.setServiceType(AppUtilities.getStringFromResource(R.string.women));

        specialList.add(specialModel1);

        SpecialModel specialModel2 = new SpecialModel();
        specialModel2.setId("902");
        specialModel2.setCategory_name(AppUtilities.getStringFromResource(R.string.child_salon));
        specialModel2.setCategory_icon(R.drawable.child_img);
        specialModel2.setServiceType(AppUtilities.getStringFromResource(R.string.child_salon));

        specialList.add(specialModel2);

        SpecialModel specialModel = new SpecialModel();
        specialModel.setId("900");
        specialModel.setCategory_name(AppUtilities.getStringFromResource(R.string.all));
        specialModel.setCategory_icon(R.drawable.all_img);
        specialModel.setServiceType(AppUtilities.getStringFromResource(R.string.all));

        specialList.add(specialModel);

        /*SpecialModel specialModel4 = new SpecialModel();
        specialModel4.setId("904");
        specialModel4.setCategory_name(AppUtilities.getStringFromResource(R.string.nerby));
        specialModel4.setCategory_icon(R.drawable.banner);
        specialModel4.setServiceType(AppUtilities.getStringFromResource(R.string.nerby));

        specialList.add(specialModel4);*/
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    public void checkLocation() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToReadLocation();
        } else {
            locationService();
        }
    }

    // Identifier for the permission request
    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;

    // Called when the user is performing an action which requires the app to read the
    // user's Current location

    public void getPermissionToReadLocation() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {

            }

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    READ_LOCATION_PERMISSIONS_REQUEST);
        } else {
            locationService();
        }
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationService();
            } else {
                checkLocation();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void locationService() {
        try {
            final ProgressDialog pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading..");
            //pd.setCancelable(false);
            if (Paper.book().read("first_request") == null)
                pd.show();
            else {
                /*if(nearbyFlag){
                    getNearByLocations();
                }else {*/
                    requestServer();
               // }
            }

            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            // Define a listener that responds to location updates
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    if (Paper.book().read("first_request") == null)
                        pd.dismiss();

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    DBG.d("AAAAAA", location.getLatitude() + " -- " + location.getLongitude());
                    Paper.book().write("lat", location.getLatitude());
                    Paper.book().write("long", location.getLongitude());

                    Paper.book().delete("lat1");
                    Paper.book().delete("long1");

                    try {
                        if (locationManager != null && locationListener != null) {
                            locationManager.removeUpdates(locationListener);
                        }
                    } catch (SecurityException e) {

                    }

                    if (Paper.book().read("first_request") == null) {
                        Paper.book().write("first_request", false);
                        /*if(nearbyFlag){
                            getNearByLocations();
                        }else {*/
                            requestServer();
                        //}
                    } else {
                        Paper.book().write("first_request", false);
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            // Register the listener with the Location Manager to receive location updates
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        } catch (SecurityException e) {
            DBG.printStackTrace(e);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (((ContainerActivity) getActivity()).checkLocationService()) {
            checkLocation();
        } else {
            ((ContainerActivity) getActivity()).showCustomeDialog("Turn on the location service", "This app needs to know your current location.",
                    "Allow", true, new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {
                            ((ContainerActivity) getActivity()).openSettings();
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }
    }

    @Override
    public void onPause() {
        super.onPause();


        try {
            if (locationManager != null && locationListener != null) {
                locationManager.removeUpdates(locationListener);
            }
        } catch (SecurityException e) {

        }

        saveAdsImpression();
    }

    private void requestServer() {
        getData();
        //22.56670000/88.36670000
        Body.Builder builder = new Body.Builder()
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"))
                .add("country_id", "0")
                .add("category_id", "" + category_id)
                .add("kids_friendly", "0")
                .add("parking_available", "0")
                .add("beer_n_wine_bar", "0")
                .add("tv", "0")
                .add("home_service", "0")
                .add("wifi_access", "0")
                .add("handicap_access", "0")
                .add("accept_walkin", "0")
                .add("service_men", "" + service_men)
                .add("service_women", "" + service_women)
                .add("service_kids", "" + service_kids)
                .add("user_rating", "0")
                .add("distance", "")
                .add("hair_type", "");
        Body bodyRequest = new Body(builder);
        DBG.d("AAAAAA", Paper.book().read("lat") + " " + Paper.book().read("long") + " " + category_id + " " + service_men + " " + service_women + " " + service_kids);
        DBG.d("AAAAAA", ""+bodyRequest.getBody());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS)
                .fromString()
                .execute(getActivity(), "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("businesses");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<ServiceDetailModel>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                DBG.d("LoginActivity", "" + responceObject.getJSONArray("Details").toString());
                                mModelList.clear();
                                mModelList = gson.fromJson(data, listType);

                                if (mModelList.size() > 0) {
                                    Paper.book().write("selected_list", mModelList);
                                    changeActivity();
                                } else {
                                    ((ContainerActivity) getActivity()).showCustomeDialog("Hairvii", "Currently There Is No List Available", new BaseActivity.OnOkCancelListner() {
                                        @Override
                                        public void onOk() {

                                        }

                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                                }

                            } else {
                                ((ContainerActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void getData() {
        Boolean flag = Paper.book().read("normal_model");
        if (flag) {
            ListModel listModel = Paper.book().read("model");
            category_id = Integer.parseInt(listModel.getId());
            service_women = 0;
            service_kids = 0;
            service_men = 0;
        } else {
            SpecialModel specialModel = Paper.book().read("model");

            if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.all))) {
                category_id = 0;
                service_women = 0;
                service_kids = 0;
                service_men = 0;
            }

            if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.women))) {
                category_id = 0;
                service_women = 1;
                service_kids = 0;
                service_men = 0;
            }

            if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.men_salon))) {
                category_id = 0;
                service_men = 1;
                service_women = 0;
                service_kids = 0;
            }

            if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.child_salon))) {
                category_id = 0;
                service_kids = 1;
                service_men = 0;
                service_women = 0;
            }

        }
    }

    private void loadAdd() {
        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.ADS)
                .fromString()
                .execute(getActivity(), "Ads", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("mobileads");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<AdsModel>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                adsList = gson.fromJson(data, listType);

                                Paper.book().write("ads", adsList);
                                setAds();
                            } else {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void setAds() {
        adsList = Paper.book().read("ads");

        t = new Timer();
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {
                                      //Called each time when 10000 milliseconds (10 second) (the period parameter)
                                      if (getActivity() != null) {
                                          getActivity().runOnUiThread(new Runnable() {
                                              @Override
                                              public void run() {
                                                  ads.setVisibility(View.VISIBLE);

                                                  if (ads_id.toString().isEmpty())
                                                      ads_id.append(adsList.get(i).getId());
                                                  else
                                                      ads_id.append("," + adsList.get(i).getId());

                                                  if (Integer.parseInt(adsList.get(i).getType()) == 2) {
                                                      Picasso.with(getActivity())
                                                              .load(R.drawable.add)
                                                              .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                              .into(ads);
                                                  } else {
                                                      Picasso.with(getActivity())
                                                              .load(adsList.get(i).getAd_content())
                                                              .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                              .into(ads);
                                                  }

                                                  ad_id = adsList.get(i).getId();
                                                  link = adsList.get(i).getUrl();

                                                  if (i < adsList.size() - 1) {
                                                      i++;
                                                  } else {
                                                      i = 0;
                                                  }
                                              }
                                          });
                                      }
                                  }

                              },
                //Set how long before to start calling the TimerTask (in milliseconds)
                0,
                //Set the amount of time between each execution (in milliseconds)
                10000);
    }

    private void openBrowser(String str) {
        saveAdsCount(ad_id, str);

        Intent i = new Intent(Intent.ACTION_VIEW);
        if (!str.startsWith("http://") && !str.startsWith("https://"))
            i.setData(Uri.parse("http://" + str));
        else
            i.setData(Uri.parse(str));

        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((ContainerActivity) getActivity()).setHeader("Home");
    }
}
