package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.MyAppointmentActivity;
import com.karmickdroid.hairvii.adapter.CancelAppointmentRecycleAdapter;
import com.karmickdroid.hairvii.adapter.UpcomingAppointmentRecycleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.AppointServiceModel;
import com.karmickdroid.hairvii.model.AppointmentViewModel;
import com.karmickdroid.hairvii.model.CancelAppointment;
import com.karmickdroid.hairvii.model.PendingAppointment;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.widgets.AppointmentViewPopupDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class CanceldAppointmentFragment extends Fragment implements CancelAppointmentRecycleAdapter.OnServiceClickListener {
    View mView;
    RecyclerView cancel_recycleView;
    List<CancelAppointment> mList;
    private CancelAppointmentRecycleAdapter mAdapter;

    public CanceldAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_canceld_appointment, container, false);
        mList = Paper.book().read("cancelAppointmentList");

        init(mView);
        return mView;
    }

    private void init(View mView) {
        cancel_recycleView = (RecyclerView) mView.findViewById(R.id.cancel_recycleView);
        cancel_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        cancel_recycleView.setLayoutManager(layoutManager);

        mAdapter = new CancelAppointmentRecycleAdapter(getActivity(), mList, this);
        cancel_recycleView.setAdapter(mAdapter);
    }

    @Override
    public void onViewClick(int position) {
        Body.Builder builder = new Body.Builder()
                .add("appointment_id", mList.get(position).getId());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_VIEW)
                .fromString(true)
                .execute(getActivity(), "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_view");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<AppointmentViewModel>() {
                                }.getType();

                                String data = responceObject.getJSONObject("Details").toString();
                                AppointmentViewModel viewModel = gson.fromJson(data, listType);

                                Gson gson1 = new Gson();
                                Type listType1 = new TypeToken<List<AppointServiceModel>>() {
                                }.getType();

                                String data1 = responceObject.getJSONObject("Details").getJSONArray("services").toString();
                                List<AppointServiceModel> serviceModelList = gson1.fromJson(data1, listType1);

                                viewModel.setAppointmentServiceList(serviceModelList);

                                AppointmentViewPopupDialog dialog = new AppointmentViewPopupDialog();
                                dialog.setModelData(getActivity(), viewModel);
                                dialog.show(getChildFragmentManager(), "AppointmentCancel");

                            } else {
                                ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            ((MyAppointmentActivity) getActivity()).showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onCancelClick(int position) {

    }
}
