package com.karmickdroid.hairvii.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.FeatureLinearAdapter;
import com.karmickdroid.hairvii.adapter.OperationSlotLinearAdapter;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 */
public class OperationSlotFragment extends Fragment {
    View mView;
    private ServiceDetailModel model;
    private AdapterLinearLayout time_slot;
    private OperationSlotLinearAdapter mAdapter;

    public OperationSlotFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        mView = inflater.inflate(R.layout.fragment_operation_slot, container, false);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        time_slot = (AdapterLinearLayout) mView.findViewById(R.id.time_slot);
        mAdapter = new OperationSlotLinearAdapter(getActivity(), model.getOperationHoursList());
        time_slot.setAdapter(mAdapter);
    }

}
