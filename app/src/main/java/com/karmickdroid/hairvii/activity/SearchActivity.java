package com.karmickdroid.hairvii.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.PlaceArrayAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class SearchActivity extends BaseActivity implements View.OnClickListener, BaseActivity.OnBackClick
        , GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, RequestCallback {

    RelativeLayout rl_bottom;
    AutoCompleteTextView search_location;
    private int GOOGLE_API_CLIENT_ID = 0;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private GoogleApiClient mGoogleApiClient;
    LatLng queried_location;
    LocationManager locationManager;
    ProgressDialog PD;
    LocationListener locationListener;
    List<ServiceDetailModel> mModelList = new ArrayList<>();
    LinearLayout ll_use_location;
    boolean choiceFlag=false;

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                DBG.d("AAAAAA", "Place query did not complete. Error: " +
                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            queried_location = place.getLatLng();
            DBG.d("AAAAAA", "Latitude is :" + queried_location.latitude);
            DBG.d("AAAAAA", "Longitude is : " + queried_location.longitude);
        }
    };

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            DBG.d("AAAAAA", "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i("AAAAAA", "Fetching details for ID: " + item.placeId);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        init();

        if (checkLocationService()) {
            checkLocation();
        } else {
            showCustomeDialog("Turn on the location service", "This app needs to know your current location.",
                    "Allow", true, new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {
                            openSettings();
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.search));
        setVisible(R.id.toolbarActivity_imvBack, this);

        search_location = (AutoCompleteTextView) findViewById(R.id.search_location);
        ll_use_location = (LinearLayout) findViewById(R.id.ll_use_location);
        rl_bottom = (RelativeLayout) findViewById(R.id.rl_bottom);

        rl_bottom.setOnClickListener(this);
        ll_use_location.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_bottom:
                setHideSoftKeyboard();
                if (queried_location != null) {
                    Paper.book().read("normal_model",false);
                    requestServer1();
                } else {
                    Toast.makeText(SearchActivity.this, "Enter A Location For Search.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.ll_use_location:
                if (Paper.book().read("lat") != null) {
                    requestServer();
                } else {
                    checkLocation();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();*/
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
            DBG.d("AAAAAA", "Google Places API connected.");
        } catch (NullPointerException e) {
            DBG.printStackTrace(e);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        DBG.d("AAAAAA", "Google Places API connection suspended.");
    }

    /* Google Place API */
    private void initializePlaceApiWithAdapter() {
        final LatLngBounds BOUNDS_MOUNTAIN_VIEW;
        try {

            BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
                    new LatLng(Double.parseDouble("" + Paper.book().read("lat")),
                            Double.parseDouble("" + Paper.book().read("long"))),
                    new LatLng(Double.parseDouble("" + Paper.book().read("lat")),
                            Double.parseDouble("" + Paper.book().read("long") + 1)));


            mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                    BOUNDS_MOUNTAIN_VIEW, null);
            search_location.setAdapter(mPlaceArrayAdapter);

            /* Places */
            mGoogleApiClient = new GoogleApiClient.Builder(SearchActivity.this)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                    .addConnectionCallbacks(this)
                    .build();

            search_location.setOnItemClickListener(mAutocompleteClickListener);

            search_location.setThreshold(1);

            search_location.performValidation();

        } catch (NumberFormatException | NullPointerException | IllegalStateException e) {
            DBG.printStackTrace(e);
        }
    }

    // Identifier for the permission request
    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;

    // Called when the user is performing an action which requires the app to read the
    // user's Current location

    public void getPermissionToReadLocation() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {

            }

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    READ_LOCATION_PERMISSIONS_REQUEST);
        } else {
            locationService();
        }
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationService();
            } else {
                checkLocation();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void locationService() {
        try {
            final ProgressDialog pd = new ProgressDialog(SearchActivity.this);
            pd.setMessage("Loading..");
            //pd.setCancelable(false);
            if (Paper.book().read("first_request") == null)
                pd.show();
            else {
                initializePlaceApiWithAdapter();
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // Define a listener that responds to location updates
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    if (Paper.book().read("first_request") == null)
                        pd.dismiss();

                    if (ActivityCompat.checkSelfPermission(SearchActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(SearchActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    DBG.d("AAAAAA", location.getLatitude() + " -- " + location.getLongitude());
                    Paper.book().write("lat", location.getLatitude());
                    Paper.book().write("long", location.getLongitude());
                    try {
                        if (locationManager != null && locationListener != null) {
                            locationManager.removeUpdates(locationListener);
                        }
                    } catch (SecurityException e) {

                    }

                    if (Paper.book().read("first_request") == null) {
                        Paper.book().write("first_request", false);
                        initializePlaceApiWithAdapter();
                    } else {
                        Paper.book().write("first_request", false);
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            // Register the listener with the Location Manager to receive location updates
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        } catch (SecurityException e) {
            DBG.printStackTrace(e);
        }
    }

    public void checkLocation() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToReadLocation();
        } else {
            locationService();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (checkLocationService()) {
            checkLocation();
        } else {
            showCustomeDialog("Turn on the location service", "This app needs to know your current location.",
                    "Allow", true, new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {
                            openSettings();
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }
    }


    private void requestServer() {
        choiceFlag= false;

        Paper.book().delete("lat1");
        Paper.book().delete("long1");
        //22.56670000/88.36670000
        Body.Builder builder = new Body.Builder()
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"))
                .add("country_id", "0")
                .add("category_id", "0")
                .add("kids_friendly", "0")
                .add("parking_available", "0")
                .add("beer_n_wine_bar", "0")
                .add("tv", "0")
                .add("home_service", "0")
                .add("wifi_access", "0")
                .add("handicap_access", "0")
                .add("accept_walkin", "0")
                .add("service_men", "0")
                .add("service_women", "0")
                .add("service_kids", "0")
                .add("user_rating", "0")
                .add("distance", "")
                .add("hair_type", "");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS)
                .fromString()
                .execute(SearchActivity.this, "Login", SearchActivity.this);
    }

    private void requestServer1() {
        choiceFlag= true;
        //22.56670000/88.36670000
        Paper.book().write("lat1", queried_location.latitude);
        Paper.book().write("long1", queried_location.longitude);
        DBG.d("AAAAAA","lat1:"+queried_location.latitude+" long:"+queried_location.longitude);

        Body.Builder builder = new Body.Builder()
                .add("latitude", "" +queried_location.latitude)
                .add("longitude", "" +queried_location.longitude)
                .add("country_id", "0")
                .add("category_id", "0")
                .add("kids_friendly", "0")
                .add("parking_available", "0")
                .add("beer_n_wine_bar", "0")
                .add("tv", "0")
                .add("home_service", "0")
                .add("wifi_access", "0")
                .add("handicap_access", "0")
                .add("accept_walkin", "0")
                .add("service_men", "0")
                .add("service_women", "0")
                .add("service_kids", "0")
                .add("user_rating", "0")
                .add("distance", "")
                .add("hair_type", "");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS)
                .fromString()
                .execute(SearchActivity.this, "Login", SearchActivity.this);
    }

    private void changeActivity() {
        Intent nextPage = new Intent(SearchActivity.this, ServiceDetailActivity.class);
        Paper.book().write("normal_model", true);
        Paper.book().delete("model");
        if(!search_location.getText().toString().isEmpty() && choiceFlag) {
            nextPage.putExtra("title", "" + search_location.getText().toString());
        }
        else{
            nextPage.putExtra("title", "Nearby Shops");
        }
        nextPage.putExtra("flag","100");
        startActivity(nextPage);
        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
        //finish();
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("businesses");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                Gson gson = new Gson();
                Type listType = new TypeToken<List<ServiceDetailModel>>() {
                }.getType();

                String data = responceObject.getJSONArray("Details").toString();
                DBG.d("LoginActivity", "" + responceObject.getJSONArray("Details").toString());
                mModelList.clear();
                mModelList = gson.fromJson(data, listType);

                if (mModelList.size() > 0) {
                    Paper.book().write("selected_list", mModelList);
                    changeActivity();
                } else {
                    showCustomeDialog("Hairvii", "Currently There Is No List Available", new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }

            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }
}
