package com.karmickdroid.hairvii.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.NearByRecycleAdapter;
import com.karmickdroid.hairvii.adapter.ServiceListRecycleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.NearBy;
import com.karmickdroid.hairvii.model.NearByDetail;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class NearByActivity extends BaseActivity implements BaseActivity.OnBackClick, NearByRecycleAdapter.OnServiceListClickListener {
    List<NearBy> nearBylist;
    private RecyclerView service_recycleView;
    private NearByRecycleAdapter mAdapter;
    LocationManager locationManager;
    ProgressDialog PD;
    LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //nearBylist = Paper.book().read("nearby_list");
        nearBylist = new ArrayList<>();
        setContentView(R.layout.activity_near_by);
        init();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, getString(R.string.nerby));
        setVisible(R.id.toolbarActivity_imvBack, this);

        service_recycleView = (RecyclerView) findViewById(R.id.service_recycleView);
        service_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        service_recycleView.setLayoutManager(layoutManager);

        mAdapter = new NearByRecycleAdapter(this, nearBylist, this);
        service_recycleView.setAdapter(mAdapter);

        checkLocation();
    }

    public void checkLocation() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToReadLocation();
        } else {
            locationService();
        }
    }

    // Identifier for the permission request
    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;

    // Called when the user is performing an action which requires the app to read the
    // user's Current location

    public void getPermissionToReadLocation() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(NearByActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(NearByActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {

            }

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    READ_LOCATION_PERMISSIONS_REQUEST);
        } else {
            locationService();
        }
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationService();
            } else {
                checkLocation();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void locationService() {
        try {
            final ProgressDialog pd = new ProgressDialog(NearByActivity.this);
            pd.setMessage("Loading..");
            //pd.setCancelable(false);
            if (Paper.book().read("first_request") == null)
                pd.show();
            else {
                getNearByLocations();
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // Define a listener that responds to location updates
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    if (Paper.book().read("first_request") == null)
                        pd.dismiss();

                    if (ActivityCompat.checkSelfPermission(NearByActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(NearByActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    DBG.d("AAAAAA", location.getLatitude() + " -- " + location.getLongitude());
                    Paper.book().write("lat", location.getLatitude());
                    Paper.book().write("long", location.getLongitude());
                    try {
                        if (locationManager != null && locationListener != null) {
                            locationManager.removeUpdates(locationListener);
                        }
                    } catch (SecurityException e) {

                    }

                    if (Paper.book().read("first_request") == null) {
                        Paper.book().write("first_request", false);
                        getNearByLocations();
                    } else {
                        Paper.book().write("first_request", false);
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            // Register the listener with the Location Manager to receive location updates
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        } catch (SecurityException e) {
            DBG.printStackTrace(e);
        }
    }

    private void getNearByLocations() {

        Body.Builder builder = new Body.Builder()
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.NEARBY_PLACE)
                .fromString()
                .execute(NearByActivity.this, "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("nearby");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<NearBy>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                nearBylist = gson.fromJson(data, listType);

                                if (nearBylist.size() > 0) {
                                   mAdapter.notifyDataChange(nearBylist);
                                } else {
                                    showCustomeDialog("Hairvii", "Currently There Is No List Available", new BaseActivity.OnOkCancelListner() {
                                        @Override
                                        public void onOk() {

                                        }

                                        @Override
                                        public void onCancel() {

                                        }
                                    });
                                }

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }


    @Override
    public void onBack() {

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onServiceClick(int position) {
        NearBy nearBy = nearBylist.get(position);
        String api = "https://maps.googleapis.com/maps/api/place/details/json?placeid="+nearBy.getPlace_id()+"&key=AIzaSyBNW5UrDUKNv_CzgceTyigZ70UeNpGZXyo";

        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(api)
                .fromString()
                .execute(NearByActivity.this, "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("result");

                            if (jsonObject.optString("status").equalsIgnoreCase("ok")) {

                                NearByDetail nearByDetail = new NearByDetail();
                                nearByDetail.setName("" + responceObject.optString("name"));
                                nearByDetail.setFormatted_address("" + responceObject.optString("formatted_address"));
                                nearByDetail.setFormatted_phone_number("" + responceObject.optString("international_phone_number"));
                                nearByDetail.setWebsite("" + responceObject.optString("website"));
                                nearByDetail.setIcon("" + responceObject.optString("icon"));
                                nearByDetail.setLat("" + responceObject.getJSONObject("geometry").getJSONObject("location").getString("lat"));
                                nearByDetail.setLng("" + responceObject.getJSONObject("geometry").getJSONObject("location").getString("lng"));
                                //nearByDetail.setOpen_now(responceObject.optJSONObject("opening_hours").optBoolean("open_now"));

                                List<String> slot = new ArrayList<String>();
                                if(responceObject.has("opening_hours") && !responceObject.isNull("opening_hours")) {
                                    for (int i = 0; i < responceObject.optJSONObject("opening_hours").getJSONArray("weekday_text").length(); i++) {
                                        slot.add(responceObject.optJSONObject("opening_hours").getJSONArray("weekday_text").getString(i));
                                    }
                                }
                                nearByDetail.setTimeSlot(slot);

                                Paper.book().write("nearby_detail", nearByDetail);

                                Intent intent = new Intent(NearByActivity.this, NearByDetailActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);

                            } else {
                                showCustomeDialog("Hairvii", "No Details Available For This Business", new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }
}
