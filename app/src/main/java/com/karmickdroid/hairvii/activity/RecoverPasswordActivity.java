package com.karmickdroid.hairvii.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import io.paperdb.Paper;

public class RecoverPasswordActivity extends BaseActivity implements BaseActivity.OnBackClick, RequestCallback {
    EditText et_email;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recover_password);
        init();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.forget_password));
        setVisible(R.id.toolbarActivity_imvBack, this);

        et_email = (EditText) findViewById(R.id.et_get_email);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHideSoftKeyboard();
                if (isDataAvailable()) {
                    if(checkError())
                      doRequestLogin();
                } else {
                    showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }
            }
        });
    }

    private boolean checkError() {
        if (getTrim(R.id.et_get_email).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Email Should Not Be Empty",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void doRequestLogin() {
        Body.Builder builder = new Body.Builder()
                .add("email_id", getTrim(R.id.et_get_email));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.GET_PASSWORD)
                .fromString()
                .execute(RecoverPasswordActivity.this, "Login", this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();
        
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("forgot_password");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                showCustomeDialog("Hairvii", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {
                        goToNextScreen();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    private void goToNextScreen() {
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }
}
