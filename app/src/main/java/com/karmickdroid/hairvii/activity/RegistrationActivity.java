package com.karmickdroid.hairvii.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.forms.Form;
import com.karmickdroid.hairvii.libs.forms.SubmitHandler;
import com.karmickdroid.hairvii.libs.forms.validators.EmailValidator;
import com.karmickdroid.hairvii.libs.forms.validators.PhoneValidator;
import com.karmickdroid.hairvii.libs.forms.validators.RequiredValidator;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.CountryModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.util.CustomEditView;
import com.karmickdroid.hairvii.widgets.SearchKeyValue;
import com.karmickdroid.hairvii.widgets.SearchSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener, RequestCallback, BaseActivity.OnBackClick {
    Button mRegistration;
    CustomEditView et_location, et_first_name, et_re_password, et_pincode, et_email, et_phone, et_password;
    SearchSpinner sObj;
    private String request_code;
    private String countryId="1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);
        AppUtilities.setContext(this);

        if(getIntent().getExtras()!=null)
            request_code= getIntent().getExtras().getString("requestCode");

        init();
    }

    private void init() {
        getDeviceId();
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.registration));
        setVisible(R.id.toolbarActivity_imvBack, this);

        mRegistration = (Button) findViewById(R.id.btn_register);
        et_re_password = (CustomEditView) findViewById(R.id.et_re_password);
        et_first_name = (CustomEditView) findViewById(R.id.et_first_name);
        et_pincode = (CustomEditView) findViewById(R.id.et_pincode);
        et_email = (CustomEditView) findViewById(R.id.et_email);
        et_phone = (CustomEditView) findViewById(R.id.et_phone);
        et_password = (CustomEditView) findViewById(R.id.et_password);
        et_location = (CustomEditView) findViewById(R.id.et_location);

        et_first_name.setOnClickListener(this);
        et_re_password.setOnClickListener(this);
        et_location.setOnClickListener(this);
        mRegistration.setOnClickListener(this);

        if (Paper.book().read("country") == null) {
            doRequestCountry();
        }
    }

    private void doRegister() {
        if(isDataAvailable()) {
            if (checkError())
                doRequestRegistration();
        }else{
            showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    private boolean checkError() {
        if(getTrim(R.id.et_first_name).isEmpty()){
            et_first_name.requestFocus();

            Toast.makeText(getApplicationContext(), "Name Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(getTrim(R.id.et_location).isEmpty()){
            Toast.makeText(getApplicationContext(), "Country Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(getTrim(R.id.et_pincode).isEmpty()){
            et_pincode.requestFocus();

            Toast.makeText(getApplicationContext(), "Zipcode/Postcode Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(!validate(getTrim(R.id.et_email)) && !getTrim(R.id.et_email).isEmpty()){
            et_email.requestFocus();

            Toast.makeText(getApplicationContext(), "Enter A Valid Email Id.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(!validate1(getTrim(R.id.et_phone)) && !getTrim(R.id.et_phone).isEmpty()){
            et_phone.requestFocus();

            Toast.makeText(getApplicationContext(), "Phone Number Should Be Valid.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(getTrim(R.id.et_password).isEmpty()){
            et_password.requestFocus();

            Toast.makeText(getApplicationContext(), "Password Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(getTrim(R.id.et_re_password).isEmpty()){
            et_re_password.requestFocus();

            Toast.makeText(getApplicationContext(), "RePassword Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public boolean validate(String thisname) {
        String regexStrforEmail = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if(!thisname.matches(regexStrforEmail))
            return false;

        return true;
    }


    public void doRequestCountry() {
        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.COUNTRY)
                .fromString()
                .execute(RegistrationActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));
                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("country_list");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<CountryModel>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                List<CountryModel> countryList = gson.fromJson(data, listType);

                                Paper.book().write("country", countryList); // Primitive
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void doRequestRegistration() {
        Body.Builder builder = new Body.Builder()
                .add("cust_fname", getTrim(R.id.et_first_name))
                .add("country_id", ""+countryId)
                .add("pincode", getTrim(R.id.et_pincode))
                .add("email_id", getTrim(R.id.et_email))
                .add("mobile_no", getTrim(R.id.et_phone))
                .add("userkey", getTrim(R.id.et_password))
                .add("re_userkey", getTrim(R.id.et_re_password))
                .add("device_id", (String) Paper.book().read("token"))
                .add("device_type", "A");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.REGISTRATION)
                .fromString()
                .execute(RegistrationActivity.this, "Login", RegistrationActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_location:
                showCountryList();
                break;
            case R.id.btn_register:
                setHideSoftKeyboard();
                doRegister();
                break;
        }
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("registration");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                Intent nextPage = new Intent(RegistrationActivity.this, LoginActivity.class);
                if(request_code!=null && !request_code.isEmpty()){
                    startActivityForResult(nextPage, Integer.parseInt(request_code));
                }else {
                    startActivity(nextPage);
                }
                overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
                finish();

            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        Intent nextPage = new Intent(RegistrationActivity.this, SelectLoginActivity.class);

        if(request_code!=null && !request_code.isEmpty()){
            startActivityForResult(nextPage, Integer.parseInt(request_code));
        }else {
            startActivity(nextPage);
        }

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();

        Intent nextPage = new Intent(RegistrationActivity.this, SelectLoginActivity.class);

        if(request_code!=null && !request_code.isEmpty()){
            startActivityForResult(nextPage, Integer.parseInt(request_code));
        }else {
            startActivity(nextPage);
        }

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    public void showCountryList() {
        List<CountryModel> disciplines = Paper.book().read("country");

        ArrayList<SearchKeyValue> keyValList = new ArrayList<>();

        if (null != disciplines && !disciplines.isEmpty()) {
            for (CountryModel discipline : disciplines) {
                keyValList.add(new SearchKeyValue("" + discipline.getId(), "" + discipline.getName()));
            }

            sObj = new SearchSpinner();
            sObj.searchSpinner(this, et_location, keyValList, true, new SearchSpinner.CustomSearchDialogCallback() {
                @Override
                public void onItemClick(SearchKeyValue svObj) {
                    et_location.setText("" + svObj.getValue());
                    countryId = svObj.getKey();
                }
            });
        }
    }

}
