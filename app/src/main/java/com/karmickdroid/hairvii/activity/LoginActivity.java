package com.karmickdroid.hairvii.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.forms.Form;
import com.karmickdroid.hairvii.libs.forms.SubmitHandler;
import com.karmickdroid.hairvii.libs.forms.validators.RequiredValidator;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import io.paperdb.Paper;

public class LoginActivity extends BaseActivity implements View.OnClickListener, BaseActivity.OnBackClick,
        RequestCallback {
    Button mLogin;
    EditText et_email_phone, et_password;
    private String request_code;
    TextView tv_forget_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_detail);

        if (getIntent().getExtras() != null)
            request_code = getIntent().getExtras().getString("requestCode");

        AppUtilities.setContext(this);
        init();
    }

    private void init() {
        getDeviceId();
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.login));
        setVisible(R.id.toolbarActivity_imvBack, this);
        mLogin = (Button) findViewById(R.id.btn_login);
        tv_forget_password = (TextView) findViewById(R.id.tv_forget_password);
        tv_forget_password.setOnClickListener(this);

        doLogin();
    }

    private void doLogin() {
        Form form = new Form((Activity) this);

        form.addFormElement(R.id.et_email_phone)
                .setName("Email Or Phone")
                .addValidator(new RequiredValidator());

        form.addFormElement(R.id.et_password)
                .setName("Password")
                .addValidator(new RequiredValidator());

        form.addSubmit(R.id.btn_login, this)
                .setName("submit")
                .addSubmitHandler(new SubmitHandler() {
                    @Override
                    public void submit(Form form) {
                        setHideSoftKeyboard();
                        if(isDataAvailable()) {
                            doRequestLogin();
                        }else{
                            showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }
                });
    }

    public void doRequestLogin() {
        Body.Builder builder = new Body.Builder()
                .add("email_mobile", getTrim(R.id.et_email_phone))
                .add("userkey", getTrim(R.id.et_password))
                .add("device_id", (String) Paper.book().read("token"))
                .add("device_type", "A");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.LOGIN_URL)
                .fromString()
                .execute(LoginActivity.this, "Login", LoginActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_forget_password:
                Intent nextPage = new Intent(LoginActivity.this, RecoverPasswordActivity.class);
                startActivity(nextPage);
                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        Intent nextPage = new Intent(LoginActivity.this, SelectLoginActivity.class);
        if (request_code != null && !request_code.isEmpty()) {
            startActivityForResult(nextPage, Integer.parseInt(request_code));
        } else {
            startActivity(nextPage);
        }
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();

        Intent nextPage = new Intent(LoginActivity.this, SelectLoginActivity.class);

        if (request_code != null && !request_code.isEmpty()) {
            startActivityForResult(nextPage, Integer.parseInt(request_code));
        } else {
            startActivity(nextPage);
        }

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("login");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                Gson gson = new Gson();
                Type listType = new TypeToken<UserModel>() {
                }.getType();

                String data = responceObject.getJSONObject("Details").toString();
                UserModel user = gson.fromJson(data, listType);

                Paper.book().write("user", user); // Primitive

                goToNextScreen();
            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    private void goToNextScreen() {
       /* Intent nextPage = new Intent(LoginActivity.this, SearchActivity.class);
        startActivity(nextPage);*/
        Paper.book().write("result_code", true);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK);
        } else {
            getParent().setResult(Activity.RESULT_OK);
        }
        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onRequestError(RequestError error) {

    }
}
