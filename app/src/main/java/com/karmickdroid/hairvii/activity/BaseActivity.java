package com.karmickdroid.hairvii.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.service.RegistrationIntentService;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class BaseActivity extends AppCompatActivity {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtilities.setContext(this);

        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);
        AppUtilities.DEVICE_SCREEN_WIDTH = dm.widthPixels;
        DBG.d("AAAAAA", "" + AppUtilities.DEVICE_SCREEN_WIDTH);
    }

    public void setHeader(int id, String text) {
        TextView header = (TextView) findViewById(id);
        header.setText("" + text);
    }

    public void setHeader(int id, String text, float size) {
        TextView header = (TextView) findViewById(id);
        header.setTextSize(size);
        header.setText("" + text);
    }

    public void setVisible(int id, final OnBackClick mOnBackClick) {
        ImageView mBack = (ImageView) findViewById(id);
        mBack.setVisibility(View.VISIBLE);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnBackClick.onBack();
            }
        });
    }

    public interface OnBackClick {
        void onBack();
    }

    @NonNull
    public String getTrim(int id) {
        EditText v = (EditText) findViewById(id);
        return v.getText().toString().trim();
    }

    public void showCustomeDialog(String title, String message, final OnOkCancelListner onOkCancelListner) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Netural "Cancel" Button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User pressed Cancel button. Write Logic Here
                onOkCancelListner.onOk();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void showCustomeDialog(String title, String message, String buttonOk, final boolean flag, final OnOkCancelListner onOkCancelListner) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Netural "Cancel" Button
        alertDialog.setPositiveButton(buttonOk, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User pressed Cancel button. Write Logic Here
                onOkCancelListner.onOk();
            }
        });

        if (flag) {
            alertDialog.setCancelable(false);
        }
        // Showing Alert Message
        alertDialog.show();
    }

    public void showCustomeDialog(String title, String message, String buttonOk, boolean flag1, final boolean flag, final OnOkCancelListner onOkCancelListner) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Netural "Cancel" Button
        alertDialog.setPositiveButton(buttonOk, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User pressed Cancel button. Write Logic Here
                onOkCancelListner.onOk();
            }
        });

        if (flag) {
            alertDialog.setCancelable(false);
        }

        if (flag1) {
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // User pressed Cancel button. Write Logic Here
                    onOkCancelListner.onCancel();
                }
            });
        }
        // Showing Alert Message
        alertDialog.show();
    }

    public interface OnOkCancelListner {
        void onOk();
        void onCancel();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                DBG.d("AAAAAA", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void getDeviceId() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    public int getHorizontalRatio(int pt) {

        int ptm = (int) (((double) pt / (double) 320) * AppUtilities.DEVICE_SCREEN_WIDTH);
        if (ptm == 0) {
            ptm = 1;
        }
        return ptm;
    }

    public ProgressDialog PD(boolean showLoadingText, String loaderText) {
        ProgressDialog PD = new ProgressDialog(this);
        PD.setMessage("" + loaderText);
        PD.setCancelable(false);
        PD.show();
        // dialog.setMessage(Message);
        return PD;
    }

    public boolean isDataAvailable() {
        ConnectivityManager conxMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo mobileNwInfo = conxMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNwInfo = conxMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return ((mobileNwInfo == null ? false : mobileNwInfo.isConnected()) || (wifiNwInfo == null ? false : wifiNwInfo.isConnected()));
    }

    public void setHideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            View view = getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public String getIpAddress() {
        try {
            WifiManager wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);
            if(wifiMgr.isWifiEnabled()) {
                WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
                int ip = wifiInfo.getIpAddress();
                String wifiIpAddress = String.format("%d.%d.%d.%d",
                        (ip & 0xff),
                        (ip >> 8 & 0xff),
                        (ip >> 16 & 0xff),
                        (ip >> 24 & 0xff));

                return wifiIpAddress;
            }

            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    DBG.d("AAAAAA", "111 inetAddress.getHostAddress(): " + inetAddress.getHostAddress());
//the condition after && is missing in your snippet, checking instance of inetAddress
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        DBG.d("AAAAAA", "111 return inetAddress.getHostAddress(): " + inetAddress.getHostAddress());
                        return inetAddress.getHostAddress();
                    }

                }
            }


        } catch (Exception ex) {

        }
        return null;
    }

    public boolean validate1(String thisname) {
        String regexStrforPhn = "^[0-9]*$";

        if(thisname.length()>13)
            return false;
        if(thisname.length()<10)
            return false;
        if(!thisname.matches(regexStrforPhn))
            return false;

        return true;
    }

    public boolean checkLocationService() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            return false;
        }

        return true;
    }

    public void openSettings() {
        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 1);
    }
}
