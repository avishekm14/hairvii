package com.karmickdroid.hairvii.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import io.paperdb.Paper;

public class ReferralActivity extends BaseActivity implements BaseActivity.OnBackClick,
        View.OnClickListener, RequestCallback {

    EditText et_name, et_email;
    Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_referral);
        init();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.refferal));
        setVisible(R.id.toolbarActivity_imvBack, this);

        et_name = (EditText) findViewById(R.id.et_name);
        et_email = (EditText) findViewById(R.id.et_email);
        btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(this);
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    private boolean checkError() {
        if (getTrim(R.id.et_name).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Referral Name Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }else if(getTrim(R.id.et_email).isEmpty()){
            Toast.makeText(getApplicationContext(), "Referral Email Id Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                setHideSoftKeyboard();
                if(checkError()){
                    doRequestToServer();
                }
                break;
        }
    }

    private void doRequestToServer() {
        UserModel mModel = Paper.book().read("user");

        Body.Builder builder = new Body.Builder()
                .add("customer_id", mModel.getCustomer_id())
                .add("referral_name", getTrim(R.id.et_name))
                .add("referral_email", getTrim(R.id.et_email));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.REFFERAL)
                .fromString()
                .execute(ReferralActivity.this, "Login", this);
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("add_referral");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                et_name.setText("");
                et_email.setText("");

                showCustomeDialog("Hairvii", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });

            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }
}
