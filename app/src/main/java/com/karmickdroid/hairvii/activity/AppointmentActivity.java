package com.karmickdroid.hairvii.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.ServicesBookedLinearAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.AppointmentService;
import com.karmickdroid.hairvii.model.MonthModel;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.model.TimeSlot;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.widgets.PickerDialog;
import com.karmickdroid.hairvii.widgets.SearchKeyValue;
import com.karmickdroid.hairvii.widgets.SearchSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class AppointmentActivity extends BaseActivity implements BaseActivity.OnBackClick, View.OnClickListener {
    List<AppointmentService> mList;
    String json, comment, address;
    TextView tx_amount, processing_fee, total_price;
    EditText et_card_number, et_expiry_month, et_expiry_year, et_ccv;
    Button btn_register;
    JSONObject jsonObject, responceObject, responceObject1;
    String month, year;
    private double totalFeeRoundOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_appointment);
        json = getIntent().getExtras().getString("json");
        comment = getIntent().getExtras().getString("comment");
        address = getIntent().getExtras().getString("address");
        init();
        parseJson();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, "Payment");
        setVisible(R.id.toolbarActivity_imvBack, this);

        tx_amount = (TextView) findViewById(R.id.tx_amount);
        processing_fee = (TextView) findViewById(R.id.processing_fee);
        total_price = (TextView) findViewById(R.id.total_price);
        btn_register = (Button) findViewById(R.id.btn_register);
        et_expiry_month = (EditText) findViewById(R.id.et_expiry_month);
        et_expiry_year = (EditText) findViewById(R.id.et_expiry_year);

        btn_register.setOnClickListener(this);
        et_expiry_year.setOnClickListener(this);
        et_expiry_month.setOnClickListener(this);
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();

        showCustomeDialog("Hairvii", "This Will Cancel All Your Order.\nAre You Still Want To Proceed?", "Yes", true, true, new OnOkCancelListner() {
            @Override
            public void onOk() {
                Intent back = new Intent(AppointmentActivity.this, DetailActivity.class);
                startActivity(back);
                overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
                finish();
            }

            @Override
            public void onCancel() {

            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        setHideSoftKeyboard();

        showCustomeDialog("Hairvii", "This Will Cancel All Your Order.\nAre You Still Want To Proceed?", "Yes", true, true, new OnOkCancelListner() {
            @Override
            public void onOk() {
                Intent back = new Intent(AppointmentActivity.this, DetailActivity.class);
                startActivity(back);
                overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
                finish();
            }

            @Override
            public void onCancel() {

            }
        });
    }

    private void parseJson() {
        try {
            jsonObject = new JSONObject(((String) json));
            responceObject = jsonObject.getJSONObject("validate_appotime");
            responceObject1 = responceObject.getJSONObject("Details");

            int amount = Integer.parseInt(responceObject1.getJSONObject("Appointment").optString("total_amount"));
            float process_percent = Float.parseFloat(responceObject1.getJSONObject("Appointment").optString("processing_fee_percent"));
            float additional_processing_fee = Float.parseFloat(responceObject1.getJSONObject("Appointment").optString("additional_processing_fee"));
            float process_fee = (float) (amount * (process_percent / 100));
            DBG.d("AAAAAA", ""+String.format("%.2f", (process_fee+additional_processing_fee)));

            Gson gson = new Gson();
            Type listType = new TypeToken<List<AppointmentService>>() {
            }.getType();

            String data = responceObject1.getJSONArray("AppointmentService").toString();
            mList = gson.fromJson(data, listType);

            tx_amount.setText("$" + amount);
            processing_fee.setText("$" + String.format("%.2f", (process_fee+additional_processing_fee)));
            total_price.setText("$" + String.format("%.2f", (amount+process_fee+additional_processing_fee)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkError() {
        if (getTrim(R.id.et_card_number).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Card Number Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_expiry_month).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Expiry Month Of Card.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_expiry_year).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter Expiry Year Of Card.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_expiry_year).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter CVV Number Of Card.",
                    Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void requestServer() throws JSONException {
        DBG.d("AAAAAA", "appo_time" + responceObject.getJSONObject("postvar").optString("appo_time") +
                " extra_time" + responceObject.getJSONObject("postvar").optString("extra_time"));

        Body.Builder builder = new Body.Builder()
                .add("customer_id", responceObject.getJSONObject("postvar").optString("customer_id"))
                .add("appo_dt", responceObject.getJSONObject("postvar").optString("appo_dt"))
                .add("services", responceObject.getJSONObject("postvar").optString("services"))
                .add("employee_id", responceObject.getJSONObject("postvar").optString("employee_id"))
                .add("business_id", responceObject.getJSONObject("postvar").optString("business_id"))
                .add("appo_time", responceObject.getJSONObject("postvar").optString("appo_time"))
                .add("appo_source", "A")
                .add("extra_time", responceObject.getJSONObject("postvar").optString("extra_time"))
                .add("total_amount", responceObject1.getJSONObject("Appointment").optString("total_amount"))
                .add("card_no", getTrim(R.id.et_card_number))
                .add("card_expiry", month + "/" + year)
                .add("cvv", getTrim(R.id.et_ccv))
                .add("home_service_address", ""+address)
                .add("special_note", "" + comment);
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_BOOKING)
                .fromString(true)
                .execute(AppointmentActivity.this, "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_booking");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                showCustomeDialog("Hairvii", responceObject.optString("message"), "Ok", true, new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {
                                        finish();
                                        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    public void showExpiryMonth() {
        List<MonthModel> monthList = new ArrayList<>();
        MonthModel obj;
        obj = new MonthModel();
        obj.setMonthId("01");
        obj.setMonthName("January(01)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("02");
        obj.setMonthName("February(02)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("03");
        obj.setMonthName("March(03)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("04");
        obj.setMonthName("April(04)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("05");
        obj.setMonthName("May(05)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("06");
        obj.setMonthName("June(06)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("07");
        obj.setMonthName("July(07)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("08");
        obj.setMonthName("August(08)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("09");
        obj.setMonthName("September(09)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("10");
        obj.setMonthName("October(10)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("11");
        obj.setMonthName("November(11)");
        monthList.add(obj);

        obj = new MonthModel();
        obj.setMonthId("12");
        obj.setMonthName("December(12)");
        monthList.add(obj);

        ArrayList<SearchKeyValue> keyValList = new ArrayList<>();

        for (MonthModel business : monthList) {
            keyValList.add(new SearchKeyValue("" + business.getMonthId(), "" + business.getMonthName()));
        }

        SearchSpinner sObj = new SearchSpinner();
        sObj.searchSpinner(AppointmentActivity.this, et_expiry_month, keyValList, false, new SearchSpinner.CustomSearchDialogCallback() {
            @Override
            public void onItemClick(SearchKeyValue svObj) {
                et_expiry_month.setText("" + svObj.getValue());
                month = svObj.getKey();
            }
        });
    }

    public void showExpiryYear() {

        ArrayList<SearchKeyValue> keyValList = new ArrayList<>();
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        final int year1 = cal.get(Calendar.YEAR);

        for (int i = 0; i < 10; i++) {
            keyValList.add(new SearchKeyValue("" + (year1 + i), "" + (year1 + i)));
        }

        final SearchSpinner sObj = new SearchSpinner();
        sObj.searchSpinner(AppointmentActivity.this, et_expiry_year, keyValList, false, new SearchSpinner.CustomSearchDialogCallback() {
            @Override
            public void onItemClick(SearchKeyValue svObj) {
                et_expiry_year.setText("" + svObj.getValue());
                year = svObj.getKey();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_expiry_month:
                showExpiryMonth();
                break;
            case R.id.et_expiry_year:
                showExpiryYear();
                break;
            case R.id.btn_register:
                setHideSoftKeyboard();
                if (checkError()) {
                    try {
                        requestServer();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

}
