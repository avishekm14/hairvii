package com.karmickdroid.hairvii.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.fragment.AboutFragment;
import com.karmickdroid.hairvii.fragment.BookAppointmentFragment;
import com.karmickdroid.hairvii.fragment.FeatureFragment;
import com.karmickdroid.hairvii.fragment.GalleryFragment;
import com.karmickdroid.hairvii.fragment.ImageGalleryFragment;
import com.karmickdroid.hairvii.fragment.MapFragment;
import com.karmickdroid.hairvii.fragment.RatingFragment;
import com.karmickdroid.hairvii.fragment.ReviewFragment;
import com.karmickdroid.hairvii.fragment.ServicesFragment;
import com.karmickdroid.hairvii.fragment.StaffFragment;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.Gallery;
import com.karmickdroid.hairvii.model.OperationHours;
import com.karmickdroid.hairvii.model.PopupModel;
import com.karmickdroid.hairvii.model.ReviewModel;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SlotTime;
import com.karmickdroid.hairvii.model.Staffs;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.widgets.CustomPopupDialogClass;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class DetailActivity extends BaseActivity implements BaseActivity.OnBackClick, View.OnClickListener, CustomPopupDialogClass.CustomDialogCallback {

    private static final String TAB_1_TAG = "tab_1";
    private static final String TAB_2_TAG = "tab_2";
    private static final String TAB_3_TAG = "tab_3";
    private static final String TAB_4_TAG = "tab_4";
    private static final String TAB_5_TAG = "tab_5";

    private FragmentTabHost mTabHost;
    public ServiceDetailModel model;
    TextView tv_service_provider, tv_service_provider_add;

    RelativeLayout rl_tab1, rl_tab2, rl_tab3, rl_tab4, rl_tab5;
    ImageView img_tab1, img_tab2, img_tab3, img_tab4, img_tab5;
    TextView txt_tab1, txt_tab2, txt_tab3, txt_tab4, txt_tab5;
    private UserModel userModel;
    ImageView banner;
    private ImageView iv_settings;
    TextView rating_count;
    RatingBar ratingbar;

    final int RATE_REQUEST_CODE = 2;
    final int FAVOURITES_REQUEST_CODE = 3;
    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);
        model = (ServiceDetailModel) Paper.book().read("model_detail");
        init();
    }

    private void init() {
        if (model.getBusiness_name() != null && !model.getBusiness_name().isEmpty()) {
            String str;
            if (model.getBusiness_name().length() >= 23) {
                str = model.getBusiness_name().substring(0, 20) + "...";
            } else {
                str = model.getBusiness_name();
            }

            setHeader(R.id.toolbarActivity_tvHeading, str);
        }

        setVisible(R.id.toolbarActivity_imvBack, this);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        iv_settings = (ImageView) findViewById(R.id.toolbarActivity_imvSettings);
        iv_settings.setVisibility(View.VISIBLE);
        iv_settings.setImageResource(R.drawable.ic_more);
        iv_settings.setOnClickListener(this);

        tv_service_provider = (TextView) findViewById(R.id.tv_service_provider);
        tv_service_provider_add = (TextView) findViewById(R.id.tv_service_provider_add);
        banner = (ImageView) findViewById(R.id.banner);

        rating_count = (TextView) findViewById(R.id.rating_count);
        ratingbar = (RatingBar) findViewById(R.id.ratingbar);

        rating_count.setText("" + Integer.parseInt(model.getRating()));
        ratingbar.setRating(Integer.parseInt(model.getRating()));

        String flag1=model.getBanner_image();
        URI uri = null;
        try {
            uri = new URI(flag1.replaceAll(" ", "%20"));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(!uri.toString().isEmpty())
        Picasso.with(this).load(uri.toString()).placeholder(R.drawable.banner).into(banner);
        else
            Picasso.with(this).load(R.drawable.banner).placeholder(R.drawable.banner).into(banner);

        //Tab 1
        rl_tab1 = (RelativeLayout) findViewById(R.id.rl_tab1);
        img_tab1 = (ImageView) findViewById(R.id.img_tab1);
        txt_tab1 = (TextView) findViewById(R.id.txt_tab1);
        img_tab1.setBackgroundResource(R.drawable.tab_one_active);
        txt_tab1.setTextColor(getResources().getColor(R.color.text_color_yellow));
        rl_tab1.setOnClickListener(this);

        //Tab 2
        rl_tab2 = (RelativeLayout) findViewById(R.id.rl_tab2);
        img_tab2 = (ImageView) findViewById(R.id.img_tab2);
        txt_tab2 = (TextView) findViewById(R.id.txt_tab2);
        img_tab2.setBackgroundResource(R.drawable.tab_two_inactive);
        txt_tab2.setTextColor(getResources().getColor(R.color.text_color_white));
        rl_tab2.setOnClickListener(this);

        //Tab 3
        rl_tab3 = (RelativeLayout) findViewById(R.id.rl_tab3);
        img_tab3 = (ImageView) findViewById(R.id.img_tab3);
        txt_tab3 = (TextView) findViewById(R.id.txt_tab3);
        if (!model.getBusiness_type().equalsIgnoreCase("i")) {
            img_tab3.setBackgroundResource(R.drawable.tab_three_inactive);

        } else {
            img_tab3.setBackgroundResource(R.drawable.ic_profile_empty);
            txt_tab3.setText("My Profile");
        }
        txt_tab3.setTextColor(getResources().getColor(R.color.text_color_white));
        rl_tab3.setOnClickListener(this);

        //Tab 4
        rl_tab4 = (RelativeLayout) findViewById(R.id.rl_tab4);
        img_tab4 = (ImageView) findViewById(R.id.img_tab4);
        txt_tab4 = (TextView) findViewById(R.id.txt_tab4);
        img_tab4.setBackgroundResource(R.drawable.tab_four_inactive);
        txt_tab4.setTextColor(getResources().getColor(R.color.text_color_white));
        rl_tab4.setOnClickListener(this);

        //Tab 5
        rl_tab5 = (RelativeLayout) findViewById(R.id.rl_tab5);
        img_tab5 = (ImageView) findViewById(R.id.img_tab5);
        txt_tab5 = (TextView) findViewById(R.id.txt_tab5);
        img_tab5.setBackgroundResource(R.drawable.tab_five_inactive);
        txt_tab5.setTextColor(getResources().getColor(R.color.text_color_white));
        rl_tab5.setOnClickListener(this);

        String address = "";

        if (!model.getAddr1().isEmpty()) {
            address = address + model.getAddr1();
        }

        if (!model.getAddr1().isEmpty() && !model.getAddr2().isEmpty()) {
            address = address + "," + model.getAddr2();
        }

        if (model.getAddr1().isEmpty() && !model.getAddr2().isEmpty()) {
            address = address + model.getAddr2();
        }

        tv_service_provider.setText("" + model.getBusiness_name());
        tv_service_provider_add.setText("" + address);

        changeFragment(new AboutFragment(), 0, 0, "AboutFragment");
    }

    @Override
    protected void onResume() {
        super.onResume();

        userModel = Paper.book().read("user");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rl_tab1:
                resetResource();
                img_tab1.setBackgroundResource(R.drawable.tab_one_active);
                txt_tab1.setTextColor(getResources().getColor(R.color.text_color_yellow));

                changeFragment(new AboutFragment(), 0, 0, "AboutFragment");
                break;
            case R.id.rl_tab2:
                resetResource();
                img_tab2.setBackgroundResource(R.drawable.tab_two_active);
                txt_tab2.setTextColor(getResources().getColor(R.color.text_color_yellow));

                changeFragment(new ServicesFragment(), 0, 0, "ServicesFragment");
                break;
            case R.id.rl_tab3:
                resetResource();
                txt_tab3.setTextColor(getResources().getColor(R.color.text_color_yellow));
                if (!model.getBusiness_type().equalsIgnoreCase("i")) {
                    img_tab3.setBackgroundResource(R.drawable.tab_three_active);
                } else {
                    img_tab3.setBackgroundResource(R.drawable.ic_profile);
                }
                changeFragment(new StaffFragment(), 0, 0, "StaffFragment");
                break;
            case R.id.rl_tab4:
                resetResource();
                img_tab4.setBackgroundResource(R.drawable.tab_four_active);
                txt_tab4.setTextColor(getResources().getColor(R.color.text_color_yellow));

                changeFragment(new BookAppointmentFragment(), 0, 0, "BookAppointmentFragment");
                break;
            case R.id.rl_tab5:
                resetResource();
                img_tab5.setBackgroundResource(R.drawable.tab_five_active);
                txt_tab5.setTextColor(getResources().getColor(R.color.text_color_yellow));

                /*Intent nextPage = new Intent(DetailActivity.this,GalleryActivity.class);
                startActivity(nextPage);*/
                changeFragment(new ImageGalleryFragment(), 0, 0, "ImageGalleryFragment");
                break;
            case R.id.toolbarActivity_imvSettings:
                CustomPopupDialogClass popupWindow =
                        new CustomPopupDialogClass(DetailActivity.this, getResource());
                popupWindow.onPopupButtonClick(v);
                popupWindow.setOnCustomDialogCallback(this);
                break;
        }
    }

    private void saveAsFavourites(int status) {
        userModel = Paper.book().read("user");

        Body.Builder builder = new Body.Builder()
                .add("customer_id", userModel.getCustomer_id())
                .add("business_id", model.getId())
                .add("status", "" + status);
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.FAVOURITES)
                .fromString()
                .execute(DetailActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("set_unset_favourites");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                model.setIs_favourite(responceObject.getJSONObject("Details").optString("stauts"));
                            } else {
                                showCustomeDialog("Hairvii", responceObject.optString("message"), new OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void shareCount() {
        userModel = Paper.book().read("user");

        Body.Builder builder = new Body.Builder()
                .add("customer_id", userModel.getCustomer_id())
                .add("business_id", model.getId());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.SHARE)
                .fromString(false)
                .execute(DetailActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("share_app");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                            } else {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void resetResource() {
        img_tab1.setBackgroundResource(R.drawable.tab_one_inactive);
        txt_tab1.setTextColor(getResources().getColor(R.color.text_color_white));

        img_tab2.setBackgroundResource(R.drawable.tab_two_inactive);
        txt_tab2.setTextColor(getResources().getColor(R.color.text_color_white));

        if (!model.getBusiness_type().equalsIgnoreCase("i")) {
            img_tab3.setBackgroundResource(R.drawable.tab_three_inactive);
        } else {
            img_tab3.setBackgroundResource(R.drawable.ic_profile_empty);
        }
        txt_tab3.setTextColor(getResources().getColor(R.color.text_color_white));

        img_tab4.setBackgroundResource(R.drawable.tab_four_inactive);
        txt_tab4.setTextColor(getResources().getColor(R.color.text_color_white));

        img_tab5.setBackgroundResource(R.drawable.tab_five_inactive);
        txt_tab5.setTextColor(getResources().getColor(R.color.text_color_white));
    }

    // Identifier for the permission request
    private static final int CALL_PERMISSIONS_REQUEST = 2;

    public void getPermissionToCall() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

            }

            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PERMISSIONS_REQUEST);
        } else {
            makeCall();
        }

    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == CALL_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void checkCall() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToCall();
        } else {
            makeCall();
        }
    }

    private void makeCall() {
        if (!model.getMobile_no().isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + model.getMobile_no()));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        } else {
            showCustomeDialog("", "Number is not available for this store.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    /**
     * Fragment transaction
     *
     * @param fragment
     * @param a1
     * @param a2
     * @param tag
     */
    public void changeFragment(Fragment fragment, int a1, int a2, String tag) {
        try {

            if (tag.equalsIgnoreCase("BookAppointmentFragment")) {
                resetResource();
                img_tab4.setBackgroundResource(R.drawable.tab_four_active);
                txt_tab4.setTextColor(getResources().getColor(R.color.text_color_yellow));
            }

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            if (a1 != 0 && a2 != 0) {
                ft.setCustomAnimations(a1, a2);
            }

            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(tag);

            if (currentFragment != null && currentFragment.isVisible()) {
                DBG.d("ENTER INTO NOT NULL CURRENT FRAGMENT", "");

            } else {
                ft.replace(R.id.realtabcontent, fragment, tag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.addToBackStack(tag);
                ft.commit();
            }

        } catch (IllegalStateException | NullPointerException e) {

        }
    }

    public void shareContent() {
        Uri pictureUri = Uri.parse(model.getShare_url());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, ""+model.getShare_url());
        startActivityForResult(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)), 100);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBack() {
        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        DBG.d("LoginActivity", requestCode + " : " + resultCode);
        if (requestCode == 100) {
            //if(resultCode == RESULT_OK){
            Paper.book().write("require_update", true);
            shareCount();
            //}
        } else if (requestCode == RATE_REQUEST_CODE && Paper.book().read("result_code") != null) {
            Paper.book().delete("result_code");
            resetResource();
            changeFragment(new RatingFragment(), 0, 0, "RatingFragment");
        } else if (requestCode == FAVOURITES_REQUEST_CODE && Paper.book().read("result_code") != null) {
            Paper.book().delete("result_code");
            saveAsFavourites(1);
        }

    }

    @Override
    public void onItemClick(int pos) {
        switch (pos) {
            case 0:
                checkCall();
                break;
            case 1:
                resetResource();
                changeFragment(new MapFragment(), 0, 0, "MapFragment");
                break;
            case 2:
                if (userModel != null) {
                    shareContent();
                } else {
                    Intent loginFirst = new Intent(DetailActivity.this, SelectLoginActivity.class);
                    startActivity(loginFirst);
                    overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                }
                break;
            case 3:
                if (userModel != null) {
                    resetResource();
                    changeFragment(new RatingFragment(), 0, 0, "RatingFragment");
                } else {
                    Intent loginFirst = new Intent(DetailActivity.this, SelectLoginActivity.class);
                    loginFirst.putExtra("requestCode", "" + RATE_REQUEST_CODE);
                    startActivityForResult(loginFirst, RATE_REQUEST_CODE);
                    overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                }
                break;
            case 4:
                if (Integer.parseInt(model.getIs_favourite()) == 0) {
                    showCustomeDialog("Hairvii", "Add to Favorites?",
                            "Yes", true, false, new OnOkCancelListner() {
                                @Override
                                public void onOk() {
                                    if (userModel != null) {
                                        saveAsFavourites(1);
                                    } else {
                                        Intent loginFirst = new Intent(DetailActivity.this, SelectLoginActivity.class);
                                        loginFirst.putExtra("requestCode", "" + FAVOURITES_REQUEST_CODE);
                                        startActivityForResult(loginFirst, FAVOURITES_REQUEST_CODE);
                                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                                    }
                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                } else {
                    showCustomeDialog("Hairvii", "Do You Want To Remove This Business From Your Favorites?",
                            "Yes", true, false, new OnOkCancelListner() {
                                @Override
                                public void onOk() {
                                    if (userModel != null) {
                                        saveAsFavourites(0);
                                    } else {
                                        Intent loginFirst = new Intent(DetailActivity.this, SelectLoginActivity.class);
                                        loginFirst.putExtra("requestCode", "" + FAVOURITES_REQUEST_CODE);
                                        startActivityForResult(loginFirst, FAVOURITES_REQUEST_CODE);
                                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                                    }
                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                }
                break;
            case 5:
                resetResource();
                changeFragment(new FeatureFragment(), 0, 0, "FeatureFragment");
                break;
            case 6:
                resetResource();
                changeFragment(new ReviewFragment(), 0, 0, "ReviewFragment");
                break;
        }
    }


    private ArrayList<PopupModel> getResource() {
        ArrayList<PopupModel> models = new ArrayList<>();

        PopupModel model6 = new PopupModel();
        model6.setRes(R.drawable.call1);
        model6.setTitle("Call");
        models.add(model6);

        PopupModel model1 = new PopupModel();
        model1.setRes(R.drawable.map1);
        model1.setTitle("Map");
        models.add(model1);

        PopupModel model2 = new PopupModel();
        model2.setRes(R.drawable.share1);
        model2.setTitle("Share");
        models.add(model2);

        PopupModel model3 = new PopupModel();
        model3.setRes(R.drawable.rate1);
        model3.setTitle("Rating");
        models.add(model3);

        PopupModel model4 = new PopupModel();
        DBG.d("AAAAAA", model.getIs_favourite());
        if (Integer.parseInt(model.getIs_favourite()) == 1) {
            model4.setRes(R.drawable.favourite1_fill);
        } else {
            model4.setRes(R.drawable.favourite1);
        }
        model4.setTitle("Favorite");
        models.add(model4);

        PopupModel model5 = new PopupModel();
        model5.setRes(R.drawable.ic_features);
        model5.setTitle("Features & Facilities");
        models.add(model5);

        PopupModel model7 = new PopupModel();
        model7.setRes(R.drawable.ic_review);
        model7.setTitle("Reviews");
        models.add(model7);

        return models;
    }

    public void requestDetail() {
        UserModel userModel = Paper.book().read("user");
        String customer_id = "0";
        if (userModel != null) {
            customer_id = userModel.getCustomer_id();
        } else {
            customer_id = "0";
        }

        Body.Builder builder = new Body.Builder()
                .add("business_id", "" + model.getId())
                .add("customer_id", "" + customer_id)
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS_DETAIL)
                .fromString()
                .execute(DetailActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("business_details");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                JSONObject jsonData = responceObject.getJSONArray("Details").getJSONObject(0);

                                Gson gson = new Gson();
                                Type listType = new TypeToken<ServiceDetailModel>() {
                                }.getType();

                                String data = jsonData.toString();
                                ServiceDetailModel model = gson.fromJson(data, listType);

                                Type listType1 = new TypeToken<List<Gallery>>() {
                                }.getType();

                                JSONArray jsonObject1 = jsonData.getJSONArray("gallery");

                                String data1 = jsonObject1.toString();
                                DBG.d("LoginActivity", "" + data1);
                                List<Gallery> galleryList = gson.fromJson(data1, listType1);

                                JSONArray jsonObject2 = jsonData.getJSONArray("OperationHours");
                                List<OperationHours> operationHoursList = new ArrayList<OperationHours>();

                                for (int i = 0; i < jsonObject2.length(); i++) {
                                    JSONObject jObj = jsonObject2.getJSONObject(i);

                                    OperationHours operationHours = new OperationHours();
                                    operationHours.setId(jObj.getString("id"));
                                    operationHours.setBusiness_id(jObj.getString("business_id"));
                                    operationHours.setWeekday(jObj.getString("weekday"));
                                    operationHours.setSlot_name(jObj.getString("slot_name"));
                                    operationHours.setDay(jObj.getString("day"));

                                    Type listType3 = new TypeToken<List<SlotTime>>() {
                                    }.getType();

                                    String data3 = jObj.getJSONArray("timeslot").toString();
                                    List<SlotTime> subServicesList = gson.fromJson(data3, listType3);

                                    operationHours.setSlotTimeList(subServicesList);
                                    operationHoursList.add(operationHours);
                                }

                                JSONArray serviceArray = jsonData.getJSONArray("services");
                                List<Services> services = new ArrayList<Services>();

                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject jObj = serviceArray.getJSONObject(i);

                                    Services service = new Services();
                                    service.setCategory_id(jObj.getString("category_id"));
                                    service.setCategory_name(jObj.getString("category_name"));

                                    Type listType3 = new TypeToken<List<SubServices>>() {
                                    }.getType();

                                    String data3 = jObj.getJSONArray("category_details").toString();
                                    List<SubServices> subServicesList = gson.fromJson(data3, listType3);

                                    service.setSubServicesList(subServicesList);
                                    services.add(service);
                                }

                                Type listType4 = new TypeToken<List<Staffs>>() {
                                }.getType();

                                JSONArray jsonObject4 = jsonData.getJSONArray("staffs");

                                String data4 = jsonObject4.toString();
                                List<Staffs> staffsList = gson.fromJson(data4, listType4);

                                Type listType5 = new TypeToken<List<ReviewModel>>() {
                                }.getType();

                                JSONArray jsonObject5 = jsonData.getJSONArray("reviews");

                                String data5 = jsonObject5.toString();
                                List<ReviewModel> reviewModels = gson.fromJson(data5, listType5);

                                ArrayList<String> arrayList = new ArrayList<String>();
                                for(int i=0; i<jsonData.getJSONArray("cancellation_policies").length(); i++){
                                    arrayList.add(jsonData.getJSONArray("cancellation_policies").getString(i));
                                }

                                model.setGalleryList(galleryList);
                                model.setOperationHoursList(operationHoursList);
                                model.setServicesList(services);
                                model.setStaffsList(staffsList);
                                model.setReviewList(reviewModels);
                                model.setCancellation_policies(arrayList);

                                Paper.book().write("model_detail", model);

                                Intent intent = new Intent(DetailActivity.this, DetailActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

}
