package com.karmickdroid.hairvii.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.NavDrawerListAdapter;
import com.karmickdroid.hairvii.fragment.BlogFragment;
import com.karmickdroid.hairvii.fragment.FaqFragment;
import com.karmickdroid.hairvii.fragment.HomeFragment;
import com.karmickdroid.hairvii.fragment.RatingFragment;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.BoldTextView;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;
import com.karmickdroid.hairvii.widgets.ImageCircleTransform;
import com.karmickdroid.hairvii.widgets.NavDrawerModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class ContainerActivity extends BaseActivity {
    public Toolbar toolbar;
    public NavigationView nav_view;
    public DrawerLayout drawer_layout;
    public BoldTextView tv_username;
    private AdapterLinearLayout listView;
    private List<NavDrawerModel> drawerModels = new ArrayList<>();
    private NavDrawerListAdapter navDrawerListAdapter;
    private ImageView iv_profile_pic;
    private TextView toolbar_title;
    private int BLOG_REQUEST_CODE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_container);
        init();
        getDeviceId();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view = (NavigationView) findViewById(R.id.nav_view);
        tv_username = (BoldTextView) findViewById(R.id.tv_username);
        listView = (AdapterLinearLayout) nav_view.findViewById(R.id.lst_menu_items);
        iv_profile_pic = (ImageView) nav_view.findViewById(R.id.iv_profile_pic);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        toolbar.setNavigationIcon(R.drawable.menu_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawer_layout.isDrawerOpen(Gravity.LEFT)) {
                    drawer_layout.openDrawer(Gravity.LEFT);
                } else {
                    drawer_layout.closeDrawer(Gravity.LEFT);
                }
            }
        });

        changeFragmentFromActivity(new HomeFragment(), 0, 0, "ServiceFragment");
    }

    public void setHeader(String str) {
        toolbar_title.setText("" + str);
    }

    /**
     * Fragment transaction
     *
     * @param fragment
     * @param a1
     * @param a2
     * @param tag
     */
    public void changeFragmentFromActivity(Fragment fragment, int a1, int a2, String tag) {
        try {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            if (a1 != 0 && a2 != 0) {
                ft.setCustomAnimations(a1, a2);
            }

            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(tag);

            //if (currentFragment != null && currentFragment.isVisible()) {
            //DBG.d("ENTER INTO NOT NULL CURRENT FRAGMENT", "");

            //} else {
            ft.replace(R.id.container_frame, fragment, tag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.addToBackStack(tag);
            ft.commit();
            // }

        } catch (IllegalStateException | NullPointerException e) {

        }
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("ServiceFragment");
        Fragment currentFragment1 = getSupportFragmentManager().findFragmentByTag("FaqFragment");

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            if (currentFragment != null && currentFragment.isVisible()) {
                DBG.d("Home Fragment", "Hello");
                Paper.book().delete("ads");
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (Paper.book().read("user") == null) {
            updateNavigationItem(false);

        } else {
            updateNavigationItem(true);

        }
    }

    public void updateNavigationItem(boolean flag) {
        if (flag) {

            UserModel user = Paper.book().read("user");
            tv_username.setText("" + user.getCustomer_name());
            tv_username.setVisibility(View.VISIBLE);
            iv_profile_pic.setVisibility(View.VISIBLE);

            if (user.getLogin_via().equalsIgnoreCase("F") || user.getLogin_via().equalsIgnoreCase("G")) {

                Picasso.with(this)
                        .load(""+user.getProfile_pic())
                        .transform(new ImageCircleTransform())
                        .error(R.drawable.no_user)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(iv_profile_pic);

                drawerModels = getNavDrawerModelsAfterSocialLogin();
                setNavigationAdapter(drawerModels);
            } else {
                iv_profile_pic.setVisibility(View.VISIBLE);
                if (user.getProfile_pic() != null && !user.getProfile_pic().isEmpty()) {
                    Picasso.with(this)
                            .load(user.getProfile_pic())
                            .transform(new ImageCircleTransform())
                            .error(R.drawable.no_user)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(iv_profile_pic);
                } else {
                    Picasso.with(this)
                            .load(R.drawable.no_user)
                            .transform(new ImageCircleTransform())
                            .error(R.drawable.no_user)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(iv_profile_pic);
                }
                drawerModels = getNavDrawerModelsAfterLogin();
                setNavigationAdapter(drawerModels);
            }
        } else {
            tv_username.setVisibility(View.GONE);
            iv_profile_pic.setVisibility(View.GONE);
            drawerModels = getNavDrawerModelsBeforeLogin();
            setNavigationAdapter(drawerModels);
        }
    }

    @NonNull
    private List<NavDrawerModel> getNavDrawerModelsAfterSocialLogin() {

        //if (drawerModels == null)
        drawerModels = new ArrayList<>();

        drawerModels.add(new NavDrawerModel("Home", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText("Home");
                        changeFragmentFromActivity(new HomeFragment(), 0, 0, "ServiceFragment");
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Book Appointment", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, SearchActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);

            }
        }));

        drawerModels.add(new NavDrawerModel("My Appointments", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, MyAppointmentActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                        finish();
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Favorites", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, FavouritesActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Nearby", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, NearByActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);

            }
        }));

        drawerModels.add(new NavDrawerModel("Blog", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Paper.book().read("user") != null) {
                            toolbar_title.setText("Blog");
                            changeFragmentFromActivity(new BlogFragment(), 0, 0, "BlogFragment");
                        } else {
                            Intent nextPage = new Intent(ContainerActivity.this, SelectLoginActivity.class);
                            nextPage.putExtra("requestCode", "" + BLOG_REQUEST_CODE);
                            startActivityForResult(nextPage, BLOG_REQUEST_CODE);
                            overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                        }
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Edit Profile", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, ProfileEditActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Refer Your Friend", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, ReferralActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("How It Works", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText("How It Works");
                        changeFragmentFromActivity(new FaqFragment(), 0, 0, "FaqFragment");
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Logout", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Paper.book().delete("user");
                        Paper.book().delete("profile");
                        updateNavigationItem(false);
                        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("ServiceFragment");

                        if (currentFragment != null && currentFragment.isVisible()) {

                        } else {
                            changeFragmentFromActivity(new HomeFragment(), 0, 0, "ServiceFragment");
                        }
                    }
                }, 200L);
            }
        }));

        return drawerModels;
    }

    @NonNull
    private List<NavDrawerModel> getNavDrawerModelsAfterLogin() {

        //if (drawerModels == null)
        drawerModels = new ArrayList<>();

        drawerModels.add(new NavDrawerModel("Home", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText("Home");
                        changeFragmentFromActivity(new HomeFragment(), 0, 0, "ServiceFragment");
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Book Appointment", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, SearchActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);

            }
        }));

        drawerModels.add(new NavDrawerModel("My Appointments", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, MyAppointmentActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                        finish();
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Favorites", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, FavouritesActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Nearby", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, NearByActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);

            }
        }));

        drawerModels.add(new NavDrawerModel("Blog", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Paper.book().read("user") != null) {
                            toolbar_title.setText("Blog");
                            changeFragmentFromActivity(new BlogFragment(), 0, 0, "BlogFragment");
                        } else {
                            Intent nextPage = new Intent(ContainerActivity.this, SelectLoginActivity.class);
                            nextPage.putExtra("requestCode", "" + BLOG_REQUEST_CODE);
                            startActivityForResult(nextPage, BLOG_REQUEST_CODE);
                            overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                        }
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Edit Profile", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, ProfileEditActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Refer Your Friend", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, ReferralActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("How It Works", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText("How It Works");
                        changeFragmentFromActivity(new FaqFragment(), 0, 0, "FaqFragment");
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Change Password", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, ChangePasswordActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Logout", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Paper.book().delete("user");
                        Paper.book().delete("profile");// Primitive
                        updateNavigationItem(false);
                        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("ServiceFragment");

                        if (currentFragment != null && currentFragment.isVisible()) {

                        } else {
                            changeFragmentFromActivity(new HomeFragment(), 0, 0, "ServiceFragment");
                        }
                    }
                }, 200L);
            }
        }));

        return drawerModels;
    }

    @NonNull
    private List<NavDrawerModel> getNavDrawerModelsBeforeLogin() {

        //if (drawerModels == null)
        drawerModels = new ArrayList<>();

        drawerModels.add(new NavDrawerModel("Home", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText("Home");
                        changeFragmentFromActivity(new HomeFragment(), 0, 0, "ServiceFragment");
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Nearby", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage1 = new Intent(ContainerActivity.this, NearByActivity.class);
                        startActivity(nextPage1);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);

            }
        }));

        drawerModels.add(new NavDrawerModel("Blog", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Paper.book().read("user") != null) {
                            toolbar_title.setText("Blog");
                            changeFragmentFromActivity(new BlogFragment(), 0, 0, "BlogFragment");
                        } else {
                            Intent nextPage = new Intent(ContainerActivity.this, SelectLoginActivity.class);
                            nextPage.putExtra("requestCode", "" + BLOG_REQUEST_CODE);
                            startActivityForResult(nextPage, BLOG_REQUEST_CODE);
                            overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                        }
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("How It Works", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toolbar_title.setText("How It Works");
                        changeFragmentFromActivity(new FaqFragment(), 0, 0, "FaqFragment");
                    }
                }, 200L);
            }
        }));

        drawerModels.add(new NavDrawerModel("Log In or Register", false, new NavDrawerModel.CallBack() {
            @Override
            public void onCallback() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent nextPage = new Intent(ContainerActivity.this, SelectLoginActivity.class);
                        startActivity(nextPage);
                        overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    }
                }, 200L);
            }
        }));

        return drawerModels;
    }

    public void setNavigationAdapter(final List<NavDrawerModel> drawerModels) {
        navDrawerListAdapter = new NavDrawerListAdapter(this, drawerModels);

        navDrawerListAdapter.setOnItemClickListener(new NavDrawerListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                NavDrawerModel drawerModel = drawerModels.get(position);
                if (null != drawerModel) {


                    drawerModel.setSelectedRow(true);
                    navDrawerListAdapter.notifyDataSetChanged();
                    if (null != drawerModel.getCallBack())
                        drawerModel.getCallBack().onCallback();
                }
            }
        });
        listView.setAdapter(navDrawerListAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BLOG_REQUEST_CODE && Paper.book().read("result_code") != null) {
            Paper.book().delete("result_code");

            toolbar_title.setText("Blog");
            changeFragmentFromActivity(new BlogFragment(), 0, 0, "BlogFragment");
        }
    }
}
