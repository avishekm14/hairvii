package com.karmickdroid.hairvii.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.ServiceListRecycleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.Gallery;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.model.OperationHours;
import com.karmickdroid.hairvii.model.ReviewModel;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SlotTime;
import com.karmickdroid.hairvii.model.Staffs;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class FavouritesActivity extends BaseActivity implements BaseActivity.OnBackClick, RequestCallback,
        ServiceListRecycleAdapter.OnServiceListClickListener, View.OnClickListener {
    RecyclerView service_recycleView;
    ServiceListRecycleAdapter mAdapter;
    ImageView mBack;
    List<ServiceDetailModel> mModelList = new ArrayList<>();
    Intent startLocationService = null;
    ImageView iv_filter;
    int service_women = 0, service_men = 0, service_kids = 0, category_id = -1;
    String title;
    String call_no="";
    private UserModel userModel;
    LocationManager locationManager;
    ProgressDialog PD;
    LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_service_detail);
        //title = getIntent().getExtras().getString("title");
        userModel = Paper.book().read("user");
        init();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, "Favorites");
        setVisible(R.id.toolbarActivity_imvBack, this);
       /* iv_filter = (ImageView) findViewById(R.id.toolbarActivity_imvSettings);
        iv_filter.setVisibility(View.VISIBLE);*/
        service_recycleView = (RecyclerView) findViewById(R.id.service_recycleView);
        service_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        service_recycleView.setLayoutManager(layoutManager);

        mAdapter = new ServiceListRecycleAdapter(this, mModelList, this);
        service_recycleView.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Paper.book().read("lat")==null) {
            showCustomeDialog("Hairvii", "No Favorites Has been Added", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }else {

            if(isDataAvailable()) {
                getFavList();
            }else{
                showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBack() {
        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onServiceClick(int position) {
        requestDetail(position);
    }

    private void requestDetail(int position) {
        UserModel userModel = Paper.book().read("user");
        String customer_id = "0";
        if (userModel != null) {
            customer_id = userModel.getCustomer_id();
        } else {
            customer_id = "0";
        }

        Body.Builder builder = new Body.Builder()
                .add("business_id", "" + mModelList.get(position).getId())
                .add("customer_id", "" + customer_id)
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS_DETAIL)
                .fromString()
                .execute(FavouritesActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("business_details");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                JSONObject jsonData = responceObject.getJSONArray("Details").getJSONObject(0);

                                Gson gson = new Gson();
                                Type listType = new TypeToken<ServiceDetailModel>() {
                                }.getType();

                                String data = jsonData.toString();
                                ServiceDetailModel model = gson.fromJson(data, listType);

                                Type listType1 = new TypeToken<List<Gallery>>() {
                                }.getType();

                                JSONArray jsonObject1 = jsonData.getJSONArray("gallery");

                                String data1 = jsonObject1.toString();
                                DBG.d("LoginActivity", "" + data1);
                                List<Gallery> galleryList = gson.fromJson(data1, listType1);

                                JSONArray jsonObject2 = jsonData.getJSONArray("OperationHours");
                                List<OperationHours> operationHoursList = new ArrayList<OperationHours>();

                                for (int i = 0; i < jsonObject2.length(); i++) {
                                    JSONObject jObj = jsonObject2.getJSONObject(i);

                                    OperationHours operationHours = new OperationHours();
                                    operationHours.setId(jObj.getString("id"));
                                    operationHours.setBusiness_id(jObj.getString("business_id"));
                                    operationHours.setWeekday(jObj.getString("weekday"));
                                    operationHours.setSlot_name(jObj.getString("slot_name"));
                                    operationHours.setDay(jObj.getString("day"));

                                    Type listType3 = new TypeToken<List<SlotTime>>() {
                                    }.getType();

                                    String data3 = jObj.getJSONArray("timeslot").toString();
                                    List<SlotTime> subServicesList = gson.fromJson(data3, listType3);

                                    operationHours.setSlotTimeList(subServicesList);
                                    operationHoursList.add(operationHours);
                                }

                                JSONArray serviceArray = jsonData.getJSONArray("services");
                                List<Services> services = new ArrayList<Services>();

                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject jObj = serviceArray.getJSONObject(i);

                                    Services service = new Services();
                                    service.setCategory_id(jObj.getString("category_id"));
                                    service.setCategory_name(jObj.getString("category_name"));

                                    Type listType3 = new TypeToken<List<SubServices>>() {
                                    }.getType();

                                    String data3 = jObj.getJSONArray("category_details").toString();
                                    List<SubServices> subServicesList = gson.fromJson(data3, listType3);

                                    service.setSubServicesList(subServicesList);
                                    services.add(service);
                                }

                                Type listType4 = new TypeToken<List<Staffs>>() {
                                }.getType();

                                JSONArray jsonObject4 = jsonData.getJSONArray("staffs");

                                String data4 = jsonObject4.toString();
                                List<Staffs> staffsList = gson.fromJson(data4, listType4);

                                Type listType5 = new TypeToken<List<ReviewModel>>() {
                                }.getType();

                                JSONArray jsonObject5 = jsonData.getJSONArray("reviews");

                                String data5 = jsonObject5.toString();
                                List<ReviewModel> reviewModels = gson.fromJson(data5, listType5);

                                ArrayList<String> arrayList = new ArrayList<String>();
                                for (int i = 0; i < jsonData.getJSONArray("cancellation_policies").length(); i++) {
                                    arrayList.add(jsonData.getJSONArray("cancellation_policies").getString(i));
                                }

                                model.setGalleryList(galleryList);
                                model.setOperationHoursList(operationHoursList);
                                model.setServicesList(services);
                                model.setStaffsList(staffsList);
                                model.setReviewList(reviewModels);
                                model.setCancellation_policies(arrayList);

                                Paper.book().write("model_detail", model);

                                Intent nextPage = new Intent(FavouritesActivity.this, DetailActivity.class);
                                startActivity(nextPage);
                                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onCall(int position) {
        call_no = mModelList.get(position).getMobile_no();
        checkCall();
    }

    private void checkCall() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToCall();
        } else {
            makeCall();
        }
    }

    private void makeCall() {
        if(!call_no.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + call_no));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        }else{
            showCustomeDialog("", "Number is not available for this store.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("favourite_list");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                mModelList.clear();

                Gson gson = new Gson();
                Type listType = new TypeToken<List<ServiceDetailModel>>() {
                }.getType();

                String data = responceObject.getJSONArray("Details").toString();
                mModelList = gson.fromJson(data, listType);

                if(mModelList.size()>0){
                    mAdapter.notifyDataChange(mModelList);
                }else{
                    mAdapter.notifyDataChange(mModelList);
                    showCustomeDialog("Hairvii", "No Favorites Has been Added", new OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }

            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    // Identifier for the permission request
    private static final int CALL_PERMISSIONS_REQUEST = 2;

    public void getPermissionToCall() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

            }

            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PERMISSIONS_REQUEST);
        } else {
            makeCall();
        }

    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if(requestCode == CALL_PERMISSIONS_REQUEST){
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {

            }
        }else if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationService();
            } else {
                checkLocation();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void getFavList() {

        Body.Builder builder = new Body.Builder()
                .add("customer_id", ""+userModel.getCustomer_id())
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.FAVOURITES_LIST)
                .fromString()
                .execute(FavouritesActivity.this, "Login", FavouritesActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    public void checkLocation() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToReadLocation();
        } else {
            locationService();
        }
    }

    // Identifier for the permission request
    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;

    // Called when the user is performing an action which requires the app to read the
    // user's Current location

    public void getPermissionToReadLocation() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(FavouritesActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(FavouritesActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {

            }

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    READ_LOCATION_PERMISSIONS_REQUEST);
        } else {
            locationService();
        }
    }

    public void locationService() {
        try {
            final ProgressDialog pd = new ProgressDialog(FavouritesActivity.this);
            pd.setMessage("Loading..");
            //pd.setCancelable(false);
            if(Paper.book().read("first_request")==null)
                pd.show();
            else{
                getFavList();
            }

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            // Define a listener that responds to location updates
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    if(Paper.book().read("first_request")==null)
                        pd.dismiss();

                    if (ActivityCompat.checkSelfPermission(FavouritesActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(FavouritesActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    DBG.d("AAAAAA", location.getLatitude() + " -- " + location.getLongitude());
                    Paper.book().write("lat", location.getLatitude());
                    Paper.book().write("long", location.getLongitude());
                    try {
                        if(locationManager!=null && locationListener!=null) {
                            locationManager.removeUpdates(locationListener);
                        }
                    }catch(SecurityException e){

                    }

                    if(Paper.book().read("first_request")==null) {
                        Paper.book().write("first_request", false);
                        getFavList();
                    }
                    else{
                        Paper.book().write("first_request", false);
                    }
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            // Register the listener with the Location Manager to receive location updates
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        } catch (SecurityException e) {
            DBG.printStackTrace(e);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (checkLocationService()) {
            checkLocation();
        } else {
            showCustomeDialog("Turn on the location service", "This app needs to know your current location.",
                    "Allow", true, new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {
                            openSettings();
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }
    }

}
