package com.karmickdroid.hairvii.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.forms.Form;
import com.karmickdroid.hairvii.libs.forms.SubmitHandler;
import com.karmickdroid.hairvii.libs.forms.validators.RequiredValidator;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import io.paperdb.Paper;

public class ChangePasswordActivity extends BaseActivity implements BaseActivity.OnBackClick, RequestCallback{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forget_password);
        init();
    }

    private void init() {
        setVisible(R.id.toolbarActivity_imvBack, this);
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.change_password));

        doRegister();
    }

    private void doRegister() {
        Form form = new Form((Activity) this);

        form.addFormElement(R.id.et_old_password)
                .setName("Old Password")
                .addValidator(new RequiredValidator());

        form.addFormElement(R.id.et_new_password)
                .setName("New Password")
                .addValidator(new RequiredValidator());

        form.addFormElement(R.id.et_confirm_password)
                .setName("Confirm Password")
                .addValidator(new RequiredValidator());

        form.addSubmit(R.id.btn_change_password, this)
                .setName("Change Password")
                .addSubmitHandler(new SubmitHandler() {
                    @Override
                    public void submit(Form form) {
                        if (getTrim(R.id.et_new_password).equalsIgnoreCase(getTrim(R.id.et_confirm_password))) {
                            setHideSoftKeyboard();

                            doRequestChangePassword();
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, "Confirm password not match with New password.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void doRequestChangePassword() {
        UserModel mModel = Paper.book().read("user");
        Body.Builder builder = new Body.Builder()
                .add("old_password", getTrim(R.id.et_old_password))
                .add("new_password", getTrim(R.id.et_new_password))
                .add("customer_id", mModel.getCustomer_id());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.CHANGE_PASSWORD)
                .fromString()
                .execute(ChangePasswordActivity.this, "ForgetPassword", this);
    }

    @Override
    public void onBack() {

        setHideSoftKeyboard();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("change_password");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                resetField(R.id.et_old_password);
                resetField(R.id.et_new_password);
                resetField(R.id.et_confirm_password);

                showCustomeDialog("Success", "Password change successfully.", new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    private void resetField(int id){
        EditText et = (EditText) findViewById(id);
        et.setText("");
    }
}
