package com.karmickdroid.hairvii.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.ServiceListRecycleAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.AdsModel;
import com.karmickdroid.hairvii.model.Gallery;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.model.OperationHours;
import com.karmickdroid.hairvii.model.ReviewModel;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SlotTime;
import com.karmickdroid.hairvii.model.SpecialModel;
import com.karmickdroid.hairvii.model.Staffs;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.util.HairType;
import com.karmickdroid.hairvii.widgets.CustomDialog;
import com.karmickdroid.hairvii.widgets.SearchData;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.paperdb.Paper;

public class ServiceDetailActivity extends BaseActivity implements BaseActivity.OnBackClick, RequestCallback,
        ServiceListRecycleAdapter.OnServiceListClickListener, CustomDialog.OnSubmitCallback, View.OnClickListener {

    RecyclerView service_recycleView;
    ServiceListRecycleAdapter mAdapter;
    ImageView mBack;
    List<ServiceDetailModel> mModelList = new ArrayList<>();
    Intent startLocationService = null;
    ImageView iv_filter;
    int service_women = 0, service_men = 0, service_kids = 0, category_id = 0;
    String title;
    String call_no = "";
    SearchData searchData;
    private Timer t;
    private List<AdsModel> adsList;
    private ImageView ads;
    private String link = "";
    int i = 0;
    private boolean showFlag = true;
    private String ad_id = "";
    StringBuilder ads_id = new StringBuilder("");
    String flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_service_detail);
        if (getIntent().getExtras() != null) {
            title = getIntent().getExtras().getString("title");
            if(getIntent().getExtras().getString("flag")!=null){
                flag =getIntent().getExtras().getString("flag");
            }
        }

        Paper.book().delete("require_update");
        init();
    }

    private void init() {
        if (title != null && !title.isEmpty()) {
            String str;
            if (title.length() < 23) {
                setHeader(R.id.toolbarActivity_tvHeading, "" + title);
            } else if (title.length() >22 && title.length() < 30){
                setHeader(R.id.toolbarActivity_tvHeading, "" + title, getHorizontalRatio(9));
            }else if(title.length() >29 && title.length() < 39){
                setHeader(R.id.toolbarActivity_tvHeading, "" + title, getHorizontalRatio(8));
            }else if(title.length() >38 && title.length() < 46){
                setHeader(R.id.toolbarActivity_tvHeading, "" + title, getHorizontalRatio(7));
            }else if(title.length() >45 && title.length() < 55){
                setHeader(R.id.toolbarActivity_tvHeading, "" + title, getHorizontalRatio(6));
            }else if(title.length() >54 ){
                setHeader(R.id.toolbarActivity_tvHeading, "" + title, getHorizontalRatio(5));
            }
        }

        setVisible(R.id.toolbarActivity_imvBack, this);

        ads = (ImageView) findViewById(R.id.ads);
        ads.setOnClickListener(this);

        iv_filter = (ImageView) findViewById(R.id.toolbarActivity_imvSettings);
        iv_filter.setVisibility(View.VISIBLE);
        service_recycleView = (RecyclerView) findViewById(R.id.service_recycleView);
        service_recycleView.setHasFixedSize(true);
        //service_recycleView.addItemDecoration(new MarginDecoration(this));
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        service_recycleView.setLayoutManager(layoutManager);
        mModelList = Paper.book().read("selected_list");
        mAdapter = new ServiceListRecycleAdapter(this, mModelList, this);
        service_recycleView.setAdapter(mAdapter);

        iv_filter.setOnClickListener(this);

        if (Paper.book().read("ads") != null) {
            setAds();
        }

        if (Paper.book().read("hairtype") == null) {
            doRequestHairType();
        }
    }

    private void getData() {
        Boolean flag = Paper.book().read("normal_model");
        if (flag) {
            ListModel listModel = Paper.book().read("model");
            if(listModel!=null) {
                category_id = Integer.parseInt(listModel.getId());
            }
            else {
                category_id = 0;
            }

            service_women = 0;
            service_kids = 0;
            service_men = 0;
        } else {
            SpecialModel specialModel = Paper.book().read("model");

            if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.all))) {
                category_id = -1;
                service_women = 0;
                service_kids = 0;
                service_men = 0;
            }else if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.women))) {
                category_id = -1;
                service_women = 1;
                service_kids = 0;
                service_men = 0;
            }else if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.men_salon))) {
                category_id = -1;
                service_men = 1;
                service_women = 0;
                service_kids = 0;
            }else if (specialModel.getServiceType().equalsIgnoreCase(AppUtilities.getStringFromResource(R.string.child_salon))) {
                category_id = -1;
                service_kids = 1;
                service_men = 0;
                service_women = 0;
            }else {
                category_id = 0;
                service_kids = 0;
                service_men = 0;
                service_women = 0;
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        showFlag=true;

        setHideSoftKeyboard();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBack() {
        setHideSoftKeyboard();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onServiceClick(int position) {
        if (isDataAvailable())
            requestDetail(position);
        else
            showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
    }

    private void requestDetail(int position) {
        UserModel userModel = Paper.book().read("user");
        String customer_id = "0";
        if (userModel != null) {
            customer_id = userModel.getCustomer_id();
        } else {
            customer_id = "0";
        }

        Body.Builder builder = new Body.Builder()
                .add("business_id", "" + mModelList.get(position).getId())
                .add("customer_id", "" + customer_id)
                .add("latitude", "" + Paper.book().read("lat"))
                .add("longitude", "" + Paper.book().read("long"));
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS_DETAIL)
                .fromString()
                .execute(ServiceDetailActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("business_details");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                JSONObject jsonData = responceObject.getJSONArray("Details").getJSONObject(0);

                                Gson gson = new Gson();
                                Type listType = new TypeToken<ServiceDetailModel>() {
                                }.getType();

                                String data = jsonData.toString();
                                ServiceDetailModel model = gson.fromJson(data, listType);

                                Type listType1 = new TypeToken<List<Gallery>>() {
                                }.getType();

                                JSONArray jsonObject1 = jsonData.getJSONArray("gallery");

                                String data1 = jsonObject1.toString();
                                DBG.d("LoginActivity", "" + data1);
                                List<Gallery> galleryList = gson.fromJson(data1, listType1);

                                JSONArray jsonObject2 = jsonData.getJSONArray("OperationHours");
                                List<OperationHours> operationHoursList = new ArrayList<OperationHours>();

                                for (int i = 0; i < jsonObject2.length(); i++) {
                                    JSONObject jObj = jsonObject2.getJSONObject(i);

                                    OperationHours operationHours = new OperationHours();
                                    operationHours.setId(jObj.getString("id"));
                                    operationHours.setBusiness_id(jObj.getString("business_id"));
                                    operationHours.setWeekday(jObj.getString("weekday"));
                                    operationHours.setSlot_name(jObj.getString("slot_name"));
                                    operationHours.setDay(jObj.getString("day"));

                                    Type listType3 = new TypeToken<List<SlotTime>>() {
                                    }.getType();

                                    String data3 = jObj.getJSONArray("timeslot").toString();
                                    List<SlotTime> subServicesList = gson.fromJson(data3, listType3);

                                    operationHours.setSlotTimeList(subServicesList);
                                    operationHoursList.add(operationHours);
                                }

                                JSONArray serviceArray = jsonData.getJSONArray("services");
                                List<Services> services = new ArrayList<Services>();

                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject jObj = serviceArray.getJSONObject(i);

                                    Services service = new Services();
                                    service.setCategory_id(jObj.getString("category_id"));
                                    service.setCategory_name(jObj.getString("category_name"));

                                    Type listType3 = new TypeToken<List<SubServices>>() {
                                    }.getType();

                                    String data3 = jObj.getJSONArray("category_details").toString();
                                    List<SubServices> subServicesList = gson.fromJson(data3, listType3);

                                    service.setSubServicesList(subServicesList);
                                    services.add(service);
                                }

                                Type listType4 = new TypeToken<List<Staffs>>() {
                                }.getType();

                                JSONArray jsonObject4 = jsonData.getJSONArray("staffs");

                                String data4 = jsonObject4.toString();
                                List<Staffs> staffsList = gson.fromJson(data4, listType4);

                                Type listType5 = new TypeToken<List<ReviewModel>>() {
                                }.getType();

                                JSONArray jsonObject5 = jsonData.getJSONArray("reviews");

                                String data5 = jsonObject5.toString();
                                List<ReviewModel> reviewModels = gson.fromJson(data5, listType5);

                                ArrayList<String> arrayList = new ArrayList<String>();
                                for(int i=0; i<jsonData.getJSONArray("cancellation_policies").length(); i++){
                                    arrayList.add(jsonData.getJSONArray("cancellation_policies").getString(i));
                                }

                                model.setGalleryList(galleryList);
                                model.setOperationHoursList(operationHoursList);
                                model.setServicesList(services);
                                model.setStaffsList(staffsList);
                                model.setReviewList(reviewModels);
                                model.setCancellation_policies(arrayList);

                                Paper.book().write("model_detail", model);

                                Intent nextPage = new Intent(ServiceDetailActivity.this, DetailActivity.class);
                                startActivity(nextPage);
                                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onCall(int position) {
        call_no = mModelList.get(position).getMobile_no();
        checkCall();
    }

    private void checkCall() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToCall();
        } else {
            makeCall();
        }
    }

    private void makeCall() {
        if (!call_no.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + call_no));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        } else {
            showCustomeDialog("", "Number is not available for this store.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("businesses");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                Gson gson = new Gson();
                Type listType = new TypeToken<List<ServiceDetailModel>>() {
                }.getType();

                String data = responceObject.getJSONArray("Details").toString();
                DBG.d("LoginActivity", "" + responceObject.getJSONArray("Details").toString());
                mModelList.clear();
                mModelList = gson.fromJson(data, listType);

                if (mModelList.size() > 0) {

                    mAdapter.notifyDataChange(mModelList);

                } else {
                    mAdapter.notifyDataChange(mModelList);

                    showCustomeDialog("Hairvii", "Currently There Is No List Available", new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }

            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    // Identifier for the permission request
    private static final int CALL_PERMISSIONS_REQUEST = 2;

    public void getPermissionToCall() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

            }

            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PERMISSIONS_REQUEST);
        } else {
            makeCall();
        }

    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == CALL_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onSubmit(SearchData data) {
        showFlag = true;
        searchData = data;

        getData();

        if (!data.getBusiness().isEmpty() && !data.getBusiness().equalsIgnoreCase("0")) {
            category_id = Integer.parseInt(data.getBusiness());
        }

        if (isDataAvailable())
            requestServer(searchData);
        else
            showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
    }

    private void requestServer(SearchData data) {
        String lat="", longi="";
        if(Paper.book().read("lat1")!=null && Paper.book().read("long1")!=null){
            lat = ""+Paper.book().read("lat1");
            longi = ""+Paper.book().read("long1");
        }else{
            lat = ""+Paper.book().read("lat");
            longi = ""+Paper.book().read("long");
        }

        DBG.d("LoginActivity", "category_id:" + category_id+"kids_friendly:"+ data.getKid_friendly()+
                " parking_available:"+ data.getFree_parking()+ " beer_n_wine_bar:"+ data.getBeer_and_wine()+"country_id:0"+" category_id:"+category_id+" service_men:"+service_men+
                " tv:"+ data.getTv()+" home_service:"+data.getHome_service()+" wifi_access:"+ data.getWifi_access()
                +" handicap_access:"+ data.getHandicap_access()+" accept_walkin:"+ data.getAccept_walkin()+
                " user_rating:"+ data.getRating()+"hair_type:"+ data.getIds()+"distance:"+ data.getDistance()
                +" service_women:"+service_women+" service_kids:"+service_kids+" latitude:"+lat+" longitude:"+longi);

        Body.Builder builder = new Body.Builder()
                .add("latitude", "" +lat)
                .add("longitude", "" +longi)
                .add("country_id", "0")
                .add("category_id", "" + category_id)
                .add("kids_friendly", "" + data.getKid_friendly())
                .add("parking_available", "" + data.getFree_parking())
                .add("beer_n_wine_bar", "" + data.getBeer_and_wine())
                .add("tv", "" + data.getTv())
                .add("home_service", ""+data.getHome_service())
                .add("wifi_access", "" + data.getWifi_access())
                .add("handicap_access", "" + data.getHandicap_access())
                .add("accept_walkin", "" + data.getAccept_walkin())
                .add("service_men", "" + service_men)
                .add("service_women", "" + service_women)
                .add("service_kids", "" + service_kids)
                .add("user_rating", "" + data.getRating())
                .add("hair_type", "" + data.getIds())
                .add("distance", "" + data.getDistance());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.BUSINESS)
                .fromString()
                .execute(ServiceDetailActivity.this, "Login", ServiceDetailActivity.this);
    }

    @Override
    public void onDataReset() {
        showFlag = true;

        mModelList.clear();
        mModelList = Paper.book().read("selected_list");
        searchData = null;
        mAdapter.notifyDataChange(mModelList);
    }

    @Override
    public void onCancel() {
        showFlag = true;
        setHideSoftKeyboard();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbarActivity_imvSettings:
                if (showFlag) {
                    showFlag = false;
                    if(flag==null)
                    CustomDialog.newInstance(this, title).show(getSupportFragmentManager(), "SearchDialog");
                    else
                    CustomDialog.newInstance(this, "").show(getSupportFragmentManager(), "SearchDialog");
                }
                break;
            case R.id.ads:
                if (link != null && !link.isEmpty())
                    openBrowser(link);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        CustomDialog.newInstance(this, title).onDispose();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Paper.book().read("require_update") != null) {

            Paper.book().delete("require_update");

            if (searchData == null) {
                searchData = new SearchData();
            }
            getData();

            if (!searchData.getBusiness().isEmpty() && !searchData.getBusiness().equalsIgnoreCase("0")) {
                category_id = Integer.parseInt(searchData.getBusiness());
            }

            if (isDataAvailable())
                requestServer(searchData);
            else
                showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
        }
    }

    public void doRequestHairType() {
        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.HAIR_TYPE)
                .fromString()
                .execute(ServiceDetailActivity.this, "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("hairtypes");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                JSONArray jsonData = responceObject.getJSONArray("Details");

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<HairType>>() {
                                }.getType();

                                String data = jsonData.toString();
                                List<HairType> hairTypeList = gson.fromJson(data, listType);

                                Paper.book().write("hairtype", hairTypeList);

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void setAds() {
        adsList = Paper.book().read("ads");

        t = new Timer();
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {
                                      //Called each time when 10000 milliseconds (10 second) (the period parameter)
                                      runOnUiThread(new Runnable() {
                                          @Override
                                          public void run() {
                                              ads.setVisibility(View.VISIBLE);

                                              if (ads_id.toString().isEmpty())
                                                  ads_id.append(adsList.get(i).getId());
                                              else
                                                  ads_id.append("," + adsList.get(i).getId());

                                              if(Integer.parseInt(adsList.get(i).getType())==2){
                                                  Picasso.with(ServiceDetailActivity.this)
                                                          .load(R.drawable.add)
                                                          .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                          .into(ads);
                                              }else {
                                                  Picasso.with(ServiceDetailActivity.this)
                                                          .load(adsList.get(i).getAd_content())
                                                          .memoryPolicy(MemoryPolicy.NO_CACHE)
                                                          .into(ads);
                                              }

                                              ad_id = adsList.get(i).getId();
                                              link = adsList.get(i).getUrl();

                                              if (i < adsList.size() - 1) {
                                                  i++;
                                              } else {
                                                  i = 0;
                                              }
                                          }
                                      });
                                  }

                              },
                //Set how long before to start calling the TimerTask (in milliseconds)
                0,
                //Set the amount of time between each execution (in milliseconds)
                10000);
    }

    private void openBrowser(String str) {
        saveAdsCount(ads_id.toString(), str);

        Intent i = new Intent(Intent.ACTION_VIEW);
        if (!str.startsWith("http://") && !str.startsWith("https://"))
            i.setData(Uri.parse("http://" + str));
        else
            i.setData(Uri.parse(str));

        startActivity(i);
    }

    public void saveAdsImpression() {
        DBG.d("AAAAAA", ads_id.toString() + " responce" + getIpAddress());
        Body.Builder builder = new Body.Builder()
                .add("ip_address", "" + getIpAddress())
                .add("imp_date", "" + new SimpleDateFormat("yyyy/MM/dd").format(new Date()))
                .add("addid", "" + ads_id.toString());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.ADS_IMPRESSION)
                .fromString(false)
                .execute(ServiceDetailActivity.this, "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("AAAAAA", "responce" + ((String) o));

                        ads_id = new StringBuilder("");
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    public void saveAdsCount(String ad_id, final String str) {
        DBG.d("AAAAAA", "responce" + ad_id);

        Body.Builder builder = new Body.Builder()
                .add("ip_address", "" + getIpAddress())
                .add("ad_id", "" + ad_id);
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.ADS_CLICK)
                .fromString(false)
                .execute(ServiceDetailActivity.this, "HomeFragment", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("AAAAAA", "responce" + ((String) o));
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveAdsImpression();
    }
}
