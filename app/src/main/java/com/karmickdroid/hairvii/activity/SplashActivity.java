package com.karmickdroid.hairvii.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.karmickdroid.hairvii.R;

import io.paperdb.Paper;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        //Paper.book().write("lat", "22.56670000");
        //Paper.book().write("long", "88.36670000");
        //Paper.book().write("first_request", false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    Intent nextPage = new Intent(SplashActivity.this, ContainerActivity.class);
                    startActivity(nextPage);
                    overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                    finish();
            }
        },2000L);
    }
}
