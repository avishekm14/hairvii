package com.karmickdroid.hairvii.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.model.NearByDetail;

import java.util.List;

import io.paperdb.Paper;

public class NearByDetailActivity extends BaseActivity implements BaseActivity.OnBackClick {
    NearByDetail nearByDetail;
    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;
    TextView address, phone, time_slot;
    LinearLayout llPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nearByDetail = Paper.book().read("nearby_detail");

        setContentView(R.layout.activity_near_by_detail);
        init();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, nearByDetail.getName());
        setVisible(R.id.toolbarActivity_imvBack, this);

        address = (TextView) findViewById(R.id.address);
        phone = (TextView) findViewById(R.id.phone);
        time_slot = (TextView) findViewById(R.id.time_slot);
        llPhone = (LinearLayout) findViewById(R.id.llPhone);

        address.setText(""+nearByDetail.getFormatted_address());
        if(nearByDetail.getFormatted_phone_number()!=null && !nearByDetail.getFormatted_phone_number().isEmpty()) {
            phone.setText("" + nearByDetail.getFormatted_phone_number());
        }else{
            llPhone.setVisibility(View.GONE);
        }

        List<String> slot = nearByDetail.getTimeSlot();
        String time_slot_string="";

        if(slot.size()>0){
            for(int i=0;i<slot.size();i++){
                time_slot_string = time_slot_string+slot.get(i)+"\n";
            }
        }else{
            time_slot_string = "Opening Hours Not Defined";
        }

        time_slot.setText("" + time_slot_string);

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCall();
            }
        });

        initializeMap();
    }

    /**
     * function to load map If map is not created it will create it for you
     */
    private void initializeMap() {
        DBG.w("init >>>>> ", "initializeMap called");

        googleMap = null;
        if (googleMap == null) {
            mapFragment = SupportMapFragment.newInstance();
            FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mapFrame, mapFragment);
            fragmentTransaction.commit();
            // Changed on 02/06/2015
            googleMap = mapFragment.getMap();


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setMapDefaults();
                    updateMarkerOnResume();
                }
            }, 10L);
        }
    }

    private void updateMarkerOnResume() {
        try {
            Log.v("TAG", "############updateMarkerOnResume called############# " + googleMap);
            if (googleMap == null) {
                googleMap = mapFragment.getMap();
                Log.v("TAG", "@@@@@@@@@@@updateMarkerOnResume if block @@@@@@@@@@@ " + googleMap);
            }

            LatLng curentLatLong = new LatLng(Double.parseDouble(nearByDetail.getLat()), Double.parseDouble(nearByDetail.getLng()));
            googleMap.addMarker(new MarkerOptions()
                    .position(curentLatLong)
                    .title("We are here"));
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            float zoomLevel = 16.0f;
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curentLatLong, zoomLevel));
            googleMap.getUiSettings().setScrollGesturesEnabled(true);

        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            DBG.printStackTrace(e);
        }
    }

    private void setMapDefaults() {
        try {
            Log.v("setMapDefaults", "############updateMarkerOnResume called############# " + googleMap);
            if (googleMap == null) {
                googleMap = mapFragment.getMap();
                Log.v("setMapDefaults", "@@@@@@@@@@@updateMarkerOnResume if block @@@@@@@@@@@ " + googleMap);
            }
            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            googleMap.setMyLocationEnabled(true);

            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);

            // Enable / Disable my location button
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);

            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(true);

            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);

            // Enable / Disable Rotate blue marker
            // googleMap.getUiSettings().setMyLocationButtonEnabled(false);

            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBack() {

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    private void checkCall() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToCall();
        } else {
            makeCall();
        }
    }

    // Identifier for the permission request
    private static final int CALL_PERMISSIONS_REQUEST = 2;

    public void getPermissionToCall() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

            }

            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PERMISSIONS_REQUEST);
        } else {
            makeCall();
        }

    }

    private void makeCall() {
        if (nearByDetail.getFormatted_phone_number()!=null && !nearByDetail.getFormatted_phone_number().isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + nearByDetail.getFormatted_phone_number()));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        } else {
            showCustomeDialog("", "Number is not available for this store.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == CALL_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
