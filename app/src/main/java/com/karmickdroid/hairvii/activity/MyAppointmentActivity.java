package com.karmickdroid.hairvii.activity;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.fragment.CanceldAppointmentFragment;
import com.karmickdroid.hairvii.fragment.CompleteAppointmentFragment;
import com.karmickdroid.hairvii.fragment.UpcomingAppointmentFragment;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.CancelAppointment;
import com.karmickdroid.hairvii.model.CompleteAppointment;
import com.karmickdroid.hairvii.model.PendingAppointment;
import com.karmickdroid.hairvii.model.TimeSlot;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import io.paperdb.Paper;

public class MyAppointmentActivity extends BaseActivity implements BaseActivity.OnBackClick {

    private UserModel user;
    private ViewPager viewPager;
    private SmartTabLayout viewPagerTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = Paper.book().read("user");
        if(user==null){
            Intent back = new Intent(this, ContainerActivity.class);
            startActivity(back);
            finish();
            overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        }else {
            setContentView(R.layout.activity_my_appointment);
            init();
        }
    }

    private void init() {
        setVisible(R.id.toolbarActivity_imvBack, this);
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.my_appointment));

        viewPager = (ViewPager) findViewById(R.id.appointment_viewPager);
        viewPager.setVisibility(View.GONE);
        viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setVisibility(View.GONE);
        //viewPager.setAdapter(adapter);
        //viewPagerTab.setViewPager(viewPager);
        if(user==null){
            Intent back = new Intent(this, ContainerActivity.class);
            startActivity(back);
            finish();
            overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        }else {
            requestJson();
        }
    }

    public void requestJson(){

        Body.Builder builder = new Body.Builder()
                .add("customer_id", user.getCustomer_id());
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.APPOINTMENT_LISTING)
                .fromString(true)
                .execute(MyAppointmentActivity.this, "TimeSlot", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));

                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("appointment_listing");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {

                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<PendingAppointment>>() {
                                }.getType();

                                String data = responceObject.getJSONObject("Details").getJSONArray("pending").toString();
                                List<PendingAppointment> pendingAppointmentList = gson.fromJson(data, listType);

                                Gson gson1 = new Gson();
                                Type listType1 = new TypeToken<List<CancelAppointment>>() {
                                }.getType();

                                String data1 = responceObject.getJSONObject("Details").getJSONArray("cancelled").toString();
                                List<CancelAppointment> cancelAppointmentList = gson1.fromJson(data1, listType1);

                                Gson gson2 = new Gson();
                                Type listType2 = new TypeToken<List<CompleteAppointment>>() {
                                }.getType();

                                String data2 = responceObject.getJSONObject("Details").getJSONArray("completed").toString();
                                List<CompleteAppointment> completeAppointmentList = gson2.fromJson(data2, listType2);

                                Paper.book().write("pendingAppointmentList", pendingAppointmentList);
                                Paper.book().write("cancelAppointmentList", cancelAppointmentList);
                                Paper.book().write("completeAppointmentList", completeAppointmentList);

                                setPagerAdapter();

                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new BaseActivity.OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            showCustomeDialog("Error", "Network error. Please try again.", new BaseActivity.OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void setPagerAdapter() {
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.titleA, UpcomingAppointmentFragment.class)
                .add(R.string.titleB, CanceldAppointmentFragment.class)
                .add(R.string.titleC, CompleteAppointmentFragment.class)
                .create());

        viewPager.setVisibility(View.VISIBLE);
        viewPagerTab.setVisibility(View.VISIBLE);

        viewPager.setAdapter(adapter);
        viewPagerTab.setViewPager(viewPager);
    }

    @Override
    public void onBack() {
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if(extras.containsKey("NotificationMessage"))
            {
                Intent back = new Intent(this, ContainerActivity.class);
                startActivity(back);
                finish();
                overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
            }
        }else {
            Intent back = new Intent(this, ContainerActivity.class);
            startActivity(back);
            finish();
            overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if(extras.containsKey("NotificationMessage"))
            {
                Intent back = new Intent(this, ContainerActivity.class);
                startActivity(back);
                finish();
                overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
            }
        }else {
            Intent back = new Intent(this, ContainerActivity.class);
            startActivity(back);
            finish();
            overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        }
    }
}
