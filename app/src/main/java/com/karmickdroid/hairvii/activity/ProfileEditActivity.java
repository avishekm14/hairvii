package com.karmickdroid.hairvii.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.application.MultipartUtility;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.forms.Form;
import com.karmickdroid.hairvii.libs.forms.SubmitHandler;
import com.karmickdroid.hairvii.libs.forms.validators.EmailValidator;
import com.karmickdroid.hairvii.libs.forms.validators.PhoneValidator;
import com.karmickdroid.hairvii.libs.forms.validators.RequiredValidator;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.CountryModel;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.util.Dialogs;
import com.karmickdroid.hairvii.util.MarshmallowPermissionHelper;
import com.karmickdroid.hairvii.widgets.ImageCircleTransform;
import com.karmickdroid.hairvii.widgets.SearchKeyValue;
import com.karmickdroid.hairvii.widgets.SearchSpinner;
import com.karmickdroid.hairvii.widgets.SearchSpinnerOnTop;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.paperdb.Paper;

public class ProfileEditActivity extends BaseActivity implements View.OnClickListener, Dialogs.OnOptionSelect, BaseActivity.OnBackClick {
    EditText et_first_name, et_last_name, et_country, et_pincode, et_phone;
    EditText et_address1, et_address2, et_area_name, et_city, et_location;
    TextView tv_change;

    Button btn_register;
    UserModel mModel;
    private ProgressDialog pd;
    private File file;
    private ImageView iv_profile_pic;

    public static final int REQUEST_CAMERA_STORAGE_PERMISSION = 0;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int REQUEST_OPEN_GALLERY = 2;
    private String countryId = "1";
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModel = Paper.book().read("user");

        setContentView(R.layout.activity_profile_edit);
        init();
    }

    private void init() {
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.edit_profile));
        setVisible(R.id.toolbarActivity_imvBack, this);
        iv_profile_pic = (ImageView) findViewById(R.id.iv_profile_pic);
        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_country = (EditText) findViewById(R.id.et_country);
        et_pincode = (EditText) findViewById(R.id.et_pincode);
        et_address1 = (EditText) findViewById(R.id.et_address1);
        et_address2 = (EditText) findViewById(R.id.et_address2);
        et_area_name = (EditText) findViewById(R.id.et_area_name);
        et_city = (EditText) findViewById(R.id.et_city);
        et_location = (EditText) findViewById(R.id.et_location);
        et_phone = (EditText) findViewById(R.id.et_phone);
        tv_change = (TextView) findViewById(R.id.tv_change);

        if (mModel.getLogin_via().equalsIgnoreCase("F") || mModel.getLogin_via().equalsIgnoreCase("G")) {
            et_phone.setVisibility(View.VISIBLE);
            tv_change.setVisibility(View.GONE);
        }else{
            tv_change.setVisibility(View.VISIBLE);
        }
        
        if (mModel.getProfile_pic() != null && !mModel.getProfile_pic().isEmpty()) {
            Picasso.with(this)
                    .load(mModel.getProfile_pic())
                    .transform(new ImageCircleTransform())
                    .error(R.drawable.no_user)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(iv_profile_pic);
        } else {
            Picasso.with(this)
                    .load(R.drawable.no_user)
                    .transform(new ImageCircleTransform())
                    .error(R.drawable.no_user)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(iv_profile_pic);
        }
        

        btn_register = (Button) findViewById(R.id.btn_register);

        et_country.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        iv_profile_pic.setOnClickListener(this);

        if (mModel.getCustomer_name() != null)
            et_first_name.setText("" + mModel.getCustomer_name());

        if (mModel.getCountry_name() != null)
            et_country.setText("" + mModel.getCountry_name());

        if (mModel.getCountry_id() != null)
            countryId = mModel.getCountry_id();

        if (mModel.getPincode() != null)
            et_pincode.setText("" + mModel.getPincode());

        if (mModel.getAddr1() != null)
            et_address1.setText("" + mModel.getAddr1());

        if (mModel.getAddr2() != null)
            et_address2.setText("" + mModel.getAddr2());

        if (mModel.getCity() != null)
            et_city.setText("" + mModel.getCity());

        if (mModel.getMobile_no() != null)
            et_phone.setText("" + mModel.getMobile_no());

        if (mModel.getArea_name() != null)
            et_area_name.setText("" + mModel.getArea_name());

        if (mModel.getLocation_name() != null)
            et_location.setText("" + mModel.getLocation_name());

        if (Paper.book().read("country") == null) {
            doRequestCountry();
        }
    }


    private void doRequestUpdate() {
        if (checkError())
            new ServiceTask().execute(CONST.REST_API.EDIT_PROFILE);
    }

    private boolean checkError() {
        if (getTrim(R.id.et_first_name).isEmpty()) {
            et_first_name.requestFocus();

            Toast.makeText(getApplicationContext(), "Name Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_country).isEmpty()) {
            Toast.makeText(getApplicationContext(), "Country Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_pincode).isEmpty()) {
            et_pincode.requestFocus();

            Toast.makeText(getApplicationContext(), "Zipcode/Postcode Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_address1).isEmpty()) {
            et_address1.requestFocus();

            Toast.makeText(getApplicationContext(), "Address1 Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (getTrim(R.id.et_city).isEmpty()) {
            et_city.requestFocus();

            Toast.makeText(getApplicationContext(), "City Should Not Be Empty.",
                    Toast.LENGTH_LONG).show();
            return false;
        } else if (mModel.getLogin_via().equalsIgnoreCase("F")|| mModel.getLogin_via().equalsIgnoreCase("G")) {
            if (!validate1(getTrim(R.id.et_phone)) && getTrim(R.id.et_phone).isEmpty()) {
                et_phone.requestFocus();

                Toast.makeText(getApplicationContext(), "Phone Number Should Be Valid.",
                        Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    @Override
    public void openCamera() {
        takePhoto();
    }

    @Override
    public void openGallery() {
        getFromGallery();
    }

    @Override
    public void onBack() {

        setHideSoftKeyboard();

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

    private class ServiceTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = PD(false, "Please Wait");
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            String charset = "UTF-8";
            try {
                MultipartUtility multipart = new MultipartUtility(url, charset);

                multipart.addFormField("customer_id", mModel.getCustomer_id());
                multipart.addFormField("cust_fname", "" + getTrim(R.id.et_first_name));
                multipart.addFormField("cust_lname", "");
                multipart.addFormField("country_id", ""+countryId);
                multipart.addFormField("pincode", "" + getTrim(R.id.et_pincode));
                if (mModel.getLogin_via().equalsIgnoreCase("F")|| mModel.getLogin_via().equalsIgnoreCase("G")) {
                    multipart.addFormField("mobile_no", "" + getTrim(R.id.et_phone));
                } else {
                    multipart.addFormField("mobile_no", "" + mModel.getMobile_no());
                }
                multipart.addFormField("addr1", "" + getTrim(R.id.et_address1));
                multipart.addFormField("addr2", "" + getTrim(R.id.et_address2));
                multipart.addFormField("area_name", "" + getTrim(R.id.et_area_name));
                multipart.addFormField("city", "" + getTrim(R.id.et_city));
                multipart.addFormField("location_name", "" + getTrim(R.id.et_location));
                if (file != null)
                    multipart.addFilePart("upload_profile_pic", file);

                List<String> response = multipart.finish();

                System.out.println("SERVER REPLIED:");

                StringBuilder str = new StringBuilder("");
                for (String line : response) {
                    str.append(line);
                    System.out.println(line);
                }

                return str.toString();
            } catch (IOException e) {
                e.printStackTrace();
                pd.dismiss();
            }

            return "All Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.dismiss();
            try {
                JSONObject object = new JSONObject(result);
                JSONObject responce = object.getJSONObject("edit_profile");

                if (Integer.parseInt(responce.optString("success")) == 1) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<UserModel>() {
                    }.getType();

                    String data = responce.getJSONObject("Details").toString();
                    UserModel user = gson.fromJson(data, listType);
                    user.setLogin_via(mModel.getLogin_via());

                    Paper.book().write("user", user);

                    showCustomeDialog("Hairvii", responce.optString("message"), new OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                } else {
                    JSONObject innerJSONObject = responce.getJSONObject("Details");
                    JSONArray errorArray = innerJSONObject.getJSONArray("error");

                    showCustomeDialog("Error", errorArray.getString(0), new OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_country:
                showCountryList();
                break;
            case R.id.iv_profile_pic:
                if (mModel.getLogin_via().equalsIgnoreCase("F") || mModel.getLogin_via().equalsIgnoreCase("G")) {
                    Toast.makeText(ProfileEditActivity.this, "You Will Not be Able To Change Your Picture", Toast.LENGTH_LONG).show();
                }else {
                    checkPermission();
                }
                break;
            case R.id.btn_register:
                setHideSoftKeyboard();

                if (isDataAvailable()) {
                    doRequestUpdate();
                } else {
                    showCustomeDialog("Hairvii", getResources().getString(R.string.network_error), new BaseActivity.OnOkCancelListner() {
                        @Override
                        public void onOk() {

                        }

                        @Override
                        public void onCancel() {

                        }
                    });
                }
                break;
        }
    }

    public void showCountryList() {
        List<CountryModel> disciplines = Paper.book().read("country");

        ArrayList<SearchKeyValue> keyValList = new ArrayList<>();

        if (null != disciplines && !disciplines.isEmpty()) {
            for (CountryModel discipline : disciplines) {
                keyValList.add(new SearchKeyValue("" + discipline.getId(), "" + discipline.getName()));
            }

            SearchSpinnerOnTop sObj = new SearchSpinnerOnTop();
            sObj.searchSpinner(this, et_country, keyValList, true, new SearchSpinnerOnTop.CustomSearchDialogCallback() {
                @Override
                public void onItemClick(SearchKeyValue svObj) {
                    et_country.setText("" + svObj.getValue());
                    countryId = svObj.getKey();
                }
            });
        }
    }

    public void doRequestCountry() {
        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.COUNTRY)
                .fromString()
                .execute(ProfileEditActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));
                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("country_list");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<CountryModel>>() {
                                }.getType();

                                String data = responceObject.getJSONArray("Details").toString();
                                List<CountryModel> countryList = gson.fromJson(data, listType);

                                Paper.book().write("country", countryList); // Primitive
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private File bitmapToFile(Bitmap bitmapImage) {
        //File file = new File(context.getFilesDir(), filename);
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        directory.setExecutable(true);
        // Create imageDir
        File myPath = new File(directory, "mProfile_" + timeStamp + ".png");
        myPath.setExecutable(true);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myPath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            DBG.printStackTrace(e);
        } finally {
            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    DBG.printStackTrace(e);
                }
            }
        }

        return myPath;
    }


    private void takePhoto() {
        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePicIntent.putExtra("outputX", 96);
        takePicIntent.putExtra("outputY", 96);
        takePicIntent.putExtra("aspectX", 1);
        takePicIntent.putExtra("aspectY", 1);
        if (takePicIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void getFromGallery() {
        Intent getFromGallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        getFromGallery.setType("image/*");
        getFromGallery.putExtra("outputX", 96);
        getFromGallery.putExtra("outputY", 96);
        getFromGallery.putExtra("aspectX", 1);
        getFromGallery.putExtra("aspectY", 1);
        if (getFromGallery.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(getFromGallery, REQUEST_OPEN_GALLERY);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir.getAbsoluteFile()      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "" + image.getAbsolutePath();
        return image;
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT > 22) {
            if (MarshmallowPermissionHelper.getStorageAndCameraPermission(null
                    , this, REQUEST_CAMERA_STORAGE_PERMISSION)) {
                Dialogs.dialogFetchImage(this, this);
            }
        } else {
            Dialogs.dialogFetchImage(this, this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:

                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    file = bitmapToFile(imageBitmap);
                    Picasso.with(this).load(file).transform(new ImageCircleTransform()).into(iv_profile_pic);
                    break;

                case REQUEST_OPEN_GALLERY:

                    Uri uri = data.getData();
                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    String picturePath = cursor.getString(columnIndex); // returns null
                    cursor.close();
                    file = new File(picturePath);
                    Picasso.with(this).load(file).transform(new ImageCircleTransform()).into(iv_profile_pic);
                    break;
            }
        }
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        switch (requestCode) {
            case REQUEST_CAMERA_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Dialogs.dialogFetchImage(this, this);
                }
                return;
            }

            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setHideSoftKeyboard();

        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
        finish();
    }

}
