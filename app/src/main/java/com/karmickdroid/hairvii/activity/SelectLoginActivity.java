package com.karmickdroid.hairvii.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.libs.icenet.Body;
import com.karmickdroid.hairvii.libs.icenet.Header;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.RequestCallback;
import com.karmickdroid.hairvii.libs.icenet.RequestError;
import com.karmickdroid.hairvii.model.UserModel;
import com.karmickdroid.hairvii.util.AppUtilities;
import com.karmickdroid.hairvii.util.CONST;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import io.paperdb.Paper;

public class SelectLoginActivity extends BaseActivity implements View.OnClickListener,
        BaseActivity.OnBackClick, RequestCallback,GoogleApiClient.OnConnectionFailedListener {
    Button mLogin;
    TextView tv_create_account, tv_dont_hv_account;
    RelativeLayout login_rlFbButton;
    String TAG = "Login";
    //FB
    CallbackManager callbackManager;
    String fb_access_token = "";
    String USER_EMAIL = "";
    String request_code = "";
    RelativeLayout buttonGpLogin;
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtilities.setContext(this);

        if (getIntent().getExtras() != null)
            request_code = getIntent().getExtras().getString("requestCode");

        setContentView(R.layout.activity_login);
        init();
        initGPlus();
    }

    private void init() {
        setVisible(R.id.toolbarActivity_imvBack, this);
        setHeader(R.id.toolbarActivity_tvHeading, AppUtilities.getStringFromResource(R.string.login_or_register));

        mLogin = (Button) findViewById(R.id.btn_login);
        buttonGpLogin = (RelativeLayout) findViewById(R.id.buttonGpLogin);
        tv_create_account = (TextView) findViewById(R.id.tv_create_account);
        tv_dont_hv_account = (TextView) findViewById(R.id.tv_dont_hv_account);
        login_rlFbButton = (RelativeLayout) findViewById(R.id.login_rlFbButton);

        mLogin.setOnClickListener(this);
        login_rlFbButton.setOnClickListener(this);
        tv_create_account.setOnClickListener(this);
        tv_dont_hv_account.setOnClickListener(this);

        keyHasFB();
        fbLoginAndInfo();
    }

    private void keyHasFB() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.karmickdroid.hairvii",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                DBG.d("KeyHash:", "" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            DBG.printStackTrace(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                Intent nextPage = new Intent(SelectLoginActivity.this, LoginActivity.class);
                if (request_code != null && !request_code.isEmpty()) {
                    startActivityForResult(nextPage, Integer.parseInt(request_code));
                } else {
                    startActivity(nextPage);
                }
                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                finish();
                break;
            case R.id.tv_dont_hv_account:
            case R.id.tv_create_account:
                Intent nextPage1 = new Intent(SelectLoginActivity.this, RegistrationActivity.class);
                if (request_code != null && !request_code.isEmpty()) {
                    startActivityForResult(nextPage1, Integer.parseInt(request_code));
                } else {
                    startActivity(nextPage1);
                }
                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                finish();
                break;
            case R.id.login_rlFbButton:
                LoginManager.getInstance().logInWithReadPermissions(SelectLoginActivity.this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.buttonGpLogin:
                signIn();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent nextPage = new Intent(SelectLoginActivity.this, ContainerActivity.class);
        startActivity(nextPage);*/
        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onBack() {
       /* Intent nextPage = new Intent(SelectLoginActivity.this, ContainerActivity.class);
        startActivity(nextPage);*/
        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    private void fbLoginAndInfo() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                fb_access_token = loginResult.getAccessToken().getToken().toString().trim();
                DBG.d(TAG, "Fb access token: " + fb_access_token);

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code

                                if (response.getError() != null) {
                                    Toast.makeText(SelectLoginActivity.this, response.getError().getErrorMessage(),
                                            Toast.LENGTH_LONG).show();
                                    DBG.d(TAG, "Error: " + response.getError().getErrorMessage());
                                    return;
                                }

                                Profile mUser = Profile.getCurrentProfile();
                                if (null != mUser) {
                                    DBG.d(TAG, "FB_USER");
                                    DBG.d(TAG, "User id: " + mUser.getId());
                                    String img_value = null;
                                    img_value = "http://graph.facebook.com/" + mUser.getId() + "/picture?type=large";
                                    DBG.d(TAG, "User name: " + mUser.getName());
                                    DBG.d(TAG, "User Image: " + mUser.getProfilePictureUri(100, 100));
                                    if (mUser.getProfilePictureUri(100, 100) != null) {
                                        Paper.book().write("profile", mUser.getProfilePictureUri(100, 100).toString());
                                    } else {
                                        Paper.book().write("profile", "");
                                    }
                                    DBG.d(TAG, "User Image link : " + img_value);
                                    DBG.d(TAG, "User first name: " + mUser.getFirstName());
                                    DBG.d(TAG, "User last name: " + mUser.getLastName());
                                    USER_EMAIL = object.optString("email");
                                    DBG.d(TAG, "EMAIL: " + USER_EMAIL);
                                    String profile_img = "https://graph.facebook.com/"+mUser.getId()+"/picture?type=large";

                                    doRequestLogin(USER_EMAIL, mUser.getId(), mUser.getFirstName(), mUser.getLastName(), profile_img);
                                } else {
                                    DBG.d(TAG, "FB_USER");
                                    DBG.d(TAG, "User Null");
                                    login_rlFbButton.performClick();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,picture");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                DBG.d(TAG, "####onCancel()");
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException e) {
                DBG.printStackTrace(e);
                Toast.makeText(SelectLoginActivity.this, "Please try again!", Toast.LENGTH_SHORT).show();
                LoginManager.getInstance().logOut();
            }
        });
    }

    //handling result from callback from facebook and google+ after login
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (null != callbackManager) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void doRequestLogin(String USER_EMAIL, String fbId, String fName, String lName, String imageUrl) {
        Body.Builder builder = new Body.Builder()
                .add("email_mobile", "" + USER_EMAIL)
                .add("cust_fname", fName)
                .add("cust_lname", lName)
                .add("facebookID", fbId)
                .add("profile_pic", "" + imageUrl)
                .add("device_id", (String) Paper.book().read("token"))
                .add("device_type", "A");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.FB_LOGIN)
                .fromString()
                .execute(SelectLoginActivity.this, "Login", this);
    }

    @Override
    public void onRequestSuccess(Object o) {
        DBG.d("LoginActivity", "" + ((String) o));

        try {
            JSONObject jsonObject = new JSONObject(((String) o));
            JSONObject responceObject = jsonObject.getJSONObject("fblogin");

            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                Gson gson = new Gson();
                Type listType = new TypeToken<UserModel>() {
                }.getType();

                String data = responceObject.getJSONObject("Details").toString();
                UserModel user = gson.fromJson(data, listType);

                Paper.book().write("user", user); // Primitive

                goToNextScreen();
            } else {
                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                    @Override
                    public void onOk() {

                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();

            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                @Override
                public void onOk() {

                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    private void goToNextScreen() {
        Paper.book().write("result_code",true);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK);
        } else {
            getParent().setResult(Activity.RESULT_OK);
        }
        finish();
        overridePendingTransition(R.anim.slideinfromleft, R.anim.slideouttoright);
    }

    @Override
    public void onRequestError(RequestError error) {

    }

    //this method is use to initialize google+ sign in
    private void initGPlus() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestProfile()
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        //buttonGpLogin.setSize(SignInButton.SIZE_STANDARD);
        //buttonGpLogin.setScopes(new Scope[]{Plus.SCOPE_PLUS_LOGIN});
        buttonGpLogin.setOnClickListener(this);
    }

    //following code invoke user for select user account with which he or she want to sign up
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //fetching information from google+ like user id, name, email
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String imageUrl="";
            if(acct.getPhotoUrl()==null) {
                Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                Person.Image image = person.getImage();
                imageUrl = image.getUrl();
            }else{
                imageUrl = acct.getPhotoUrl().toString();
            }

            Log.d("TAG", "token:" + acct.getId()+" "+acct.getEmail()+" "+acct.getDisplayName()+" "+imageUrl);
            doRequestGPlusLogin(acct.getEmail(), acct.getId(), acct.getDisplayName(), "", imageUrl);
        } else {

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void doRequestGPlusLogin(String USER_EMAIL, String Id, String fName, String lName, final String imageUrl) {
        Body.Builder builder = new Body.Builder()
                .add("email_mobile", "" + USER_EMAIL)
                .add("cust_fname", fName)
                .add("cust_lname", lName)
                .add("googleID", Id)
                .add("profile_pic", ""+imageUrl)
                .add("device_id", (String) Paper.book().read("token"))
                .add("device_type", "A");
        Body bodyRequest = new Body(builder);

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.GPLUS_LOGIN)
                .fromString()
                .execute(SelectLoginActivity.this, "Login", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.d("LoginActivity", "" + ((String) o));
                        try {
                            JSONObject jsonObject = new JSONObject(((String) o));
                            JSONObject responceObject = jsonObject.getJSONObject("gpluslogin");

                            if (Integer.parseInt(responceObject.optString("success")) == 1) {
                                if(imageUrl !=null) {
                                    Paper.book().write("profile", imageUrl);
                                }
                                Gson gson = new Gson();
                                Type listType = new TypeToken<UserModel>() {
                                }.getType();

                                String data = responceObject.getJSONObject("Details").toString();
                                UserModel user = gson.fromJson(data, listType);

                                Paper.book().write("user", user); // Primitive

                                goToNextScreen();
                            } else {
                                showCustomeDialog("Error", responceObject.optString("message"), new OnOkCancelListner() {
                                    @Override
                                    public void onOk() {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            showCustomeDialog("Error", "Network error. Please try again.", new OnOkCancelListner() {
                                @Override
                                public void onOk() {

                                }

                                @Override
                                public void onCancel() {

                                }
                            });
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }
}
