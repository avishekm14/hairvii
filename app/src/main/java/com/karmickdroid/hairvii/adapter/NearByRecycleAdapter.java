package com.karmickdroid.hairvii.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.NearBy;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 3/2/2016.
 */
public class NearByRecycleAdapter extends RecyclerView.Adapter<NearByRecycleAdapter.ServiceViewHolder>{

    OnServiceListClickListener onServiceClickListener;
    Context mContext;
    List<NearBy> mModelList;

    public NearByRecycleAdapter(Context mContext, List<NearBy> mModelList, OnServiceListClickListener mListener){
        this.onServiceClickListener = mListener;
        this.mContext = mContext;
        this.mModelList = mModelList;
    }

    @Override
    public NearByRecycleAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_nearby_list,
                parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NearByRecycleAdapter.ServiceViewHolder holder, final int position) {
        NearBy model = mModelList.get(position);

        Picasso.with(mContext)
                .load(model.getIcon())
                .error(R.drawable.logo)
                .into(holder.iv_profile_pic);

        if (model.getBusiness_name() != null && !model.getBusiness_name().isEmpty()) {
            String str;
            if (model.getBusiness_name().length() >= 40) {
                str = model.getBusiness_name().substring(0, 37) + "...";
            } else {
                str = model.getBusiness_name();
            }

            holder.service_provider_name.setText("" + str);
        }

        if(!model.getDistance().isEmpty())
            holder.tv_distance.setText(""+model.getDistance());

        if(!model.getAddress().isEmpty())
            holder.tv_address.setText(""+model.getAddress());
        else
            holder.tv_address.setVisibility(View.GONE);

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onServiceClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    public void notifyDataChange(List<NearBy> mModelList) {
        this.mModelList = mModelList;
        notifyDataSetChanged();
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{

        LinearLayout ll=null;
        LinearLayout ll_service_container=null;
        ImageView iv_profile_pic;
        TextView service_provider_name,tv_distance, tv_address, tv_share, tv_review;
        RatingBar ratingbar;
        RelativeLayout rl_call;

        public ServiceViewHolder(View itemView) {
            super(itemView);

            ll = (LinearLayout) itemView;
            iv_profile_pic = (ImageView) itemView.findViewById(R.id.iv_profile_pic);
            service_provider_name = (TextView) itemView.findViewById(R.id.service_provider_name);
            tv_distance = (TextView) itemView.findViewById(R.id.tv_distance);
            tv_address = (TextView) itemView.findViewById(R.id.address);
        }
    }

    public interface OnServiceListClickListener{
        public void onServiceClick(int position);
    }
}
