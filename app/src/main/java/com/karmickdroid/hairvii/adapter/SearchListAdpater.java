package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.widgets.SearchKeyValue;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Administrator on 4/28/2016.
 */
public class SearchListAdpater extends BaseAdapter {

    private Activity activity;
    private ArrayList<SubServices> data;
    private ArrayList<SearchKeyValue> arraylist;

    OnCheckChangeListener mListener;

    private LayoutInflater inflater = null;
    SubServices tempValues = null;
    int i = 0;

    public SearchListAdpater(Context a, ArrayList<SubServices> d, OnCheckChangeListener mListener) {

        activity = (Activity) a;

        Log.d("SearchListAdpater", "" + d.toString());
        this.data = d;
        this.mListener = mListener;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {

        if (data != null) {
            if (data.size() <= 0)
                return 1;
            else
                return data.size();
        }
        return 1;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {

        public TextView key;
        public TextView value;
        public CheckBox ck_select;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        tempValues = data.get(position);

        if (convertView == null) {

            vi = inflater.inflate(R.layout.dialog_searchlist_row1, null);

            holder = new ViewHolder();
            holder.key = (TextView) vi.findViewById(R.id.key);
            holder.value = (TextView) vi.findViewById(R.id.value);
            holder.ck_select = (CheckBox) vi.findViewById(R.id.ck_select);

            holder.ck_select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mListener.OnCheckChange(position,isChecked);
                }
            });

            vi.setTag(holder);

        } else
            holder = (ViewHolder) vi.getTag();

        holder.key.setText("" + tempValues.getId());
        holder.value.setText(tempValues.getService_name());
        holder.ck_select.setChecked(tempValues.isChecked());

        return vi;
    }

    public interface OnCheckChangeListener {
        void OnCheckChange(int position, boolean flag);
    }
}
