package com.karmickdroid.hairvii.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Administrator on 3/2/2016.
 */
public class ServiceListRecycleAdapter extends RecyclerView.Adapter<ServiceListRecycleAdapter.ServiceViewHolder>{

    OnServiceListClickListener onServiceClickListener;
    Context mContext;
    List<ServiceDetailModel> mModelList;

    private static final int ITEM_VIEW_TYPE_LEFT = 0;
    private static final int ITEM_VIEW_TYPE_RIGHT = 1;

    public ServiceListRecycleAdapter(Context mContext,List<ServiceDetailModel> mModelList, OnServiceListClickListener mListener){
        this.onServiceClickListener = mListener;
        this.mContext = mContext;
        this.mModelList = mModelList;
    }

    @Override
    public ServiceListRecycleAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service_detail,
                parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiceListRecycleAdapter.ServiceViewHolder holder, final int position) {
        ServiceDetailModel model = mModelList.get(position);

        String flag1=model.getBusiness_logo();
        URI uri = null;
        try {
            uri = new URI(flag1.replaceAll(" ", "%20"));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(!uri.toString().isEmpty()) {
            Picasso.with(mContext)
                    .load(uri.toString())
                    .error(R.drawable.logo)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.iv_profile_pic);
        }else{
            Picasso.with(mContext)
                    .load(R.drawable.logo)
                    .error(R.drawable.logo)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.iv_profile_pic);
        }

        if (model.getBusiness_name() != null && !model.getBusiness_name().isEmpty()) {
            String str;
            if (model.getBusiness_name().length() >= 25) {
                str = model.getBusiness_name().substring(0, 23) + "...";
            } else {
                str = model.getBusiness_name();
            }

            holder.service_provider_name.setText("" + str);
        }

        if(!model.getRating().isEmpty())
        holder.ratingbar.setRating(Float.parseFloat(model.getRating()));

        if(!model.getFavourite_count().isEmpty())
            holder.tv_fav.setText("" + model.getFavourite_count()+" likes");

        if(!model.getDistance().isEmpty())
            holder.tv_distance.setText(""+model.getDistance()+" mileage");

        if(!model.getApp_share_count().isEmpty())
            holder.tv_share.setText(""+model.getApp_share_count());

        if(!model.getReview_count().isEmpty())
            holder.tv_review.setText(""+model.getReview_count()+" Reviews");

        holder.rl_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onCall(position);
            }
        });
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onServiceClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    public void notifyDataChange(List<ServiceDetailModel> mModelList) {
        this.mModelList = mModelList;
        notifyDataSetChanged();
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{

        LinearLayout ll=null;
        LinearLayout ll_service_container=null;
        ImageView iv_profile_pic;
        TextView service_provider_name,tv_distance, tv_fav, tv_share, tv_review;
        RatingBar ratingbar;
        RelativeLayout rl_call;

        public ServiceViewHolder(View itemView) {
            super(itemView);

            ll = (LinearLayout) itemView;
            iv_profile_pic = (ImageView) itemView.findViewById(R.id.iv_profile_pic);
            service_provider_name = (TextView) itemView.findViewById(R.id.service_provider_name);
            ratingbar = (RatingBar) itemView.findViewById(R.id.ratingbar);
            tv_distance = (TextView) itemView.findViewById(R.id.tv_distance);
            tv_fav = (TextView) itemView.findViewById(R.id.tv_fav);
            tv_share = (TextView) itemView.findViewById(R.id.tv_share);
            tv_review = (TextView) itemView.findViewById(R.id.tv_review);
            rl_call = (RelativeLayout) itemView.findViewById(R.id.rl_call);
        }
    }

    public interface OnServiceListClickListener{
        public void onServiceClick(int position);
        public void onCall(int position);
    }
}
