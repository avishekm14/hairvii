package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.AppointmentService;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class ServicesBookedLinearAdapter extends BaseAdapter {
    private Context context;
    private List<AppointmentService> listItems;
    private OnItemClickListener clickListener;

    public ServicesBookedLinearAdapter(Context context, List<AppointmentService> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.booked_service, null);
        }

        TextView service_name = (TextView) convertView.findViewById(R.id.service_name);
        TextView service_price = (TextView) convertView.findViewById(R.id.service_price);

        service_name.setText(""+listItems.get(position).getService_name());
        service_price.setText("$"+listItems.get(position).getService_amount());

        RelativeLayout extraHomeService = (RelativeLayout) convertView.findViewById(R.id.extraHomeService);
        TextView extraHourCharge = (TextView) convertView.findViewById(R.id.extraHourCharge);

        return convertView;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
        void onBookNow(String id, String name);
    }

    public void notifyDataChanged(List<AppointmentService> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
