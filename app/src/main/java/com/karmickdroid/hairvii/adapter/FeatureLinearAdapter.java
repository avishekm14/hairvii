package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.util.HairType;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class FeatureLinearAdapter extends BaseAdapter{

    private Context context;
    private List<Services> listItems;
    private List<HairType> listItem1;
    private OnItemClickListener clickListener;
    ExpandableLayout expandableLayout;
    TextView service_name;
    AdapterLinearLayout sub_service;
    SubServicesLinearAdapter mAdapter;
    boolean flag=false;

    public FeatureLinearAdapter(Context context, boolean flag, List<Services> listItems, List<HairType> listItem1) {
        this.context = context;
        this.flag = flag;

        if(this.flag){
            this.listItems = listItems;
        }else{
            this.listItem1 = listItem1;
        }
    }

    @Override
    public int getCount() {
        if(this.flag){
            return listItems.size();
        }else{
            return listItem1.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if(this.flag){
            return listItems.get(position);
        }else{
            return listItem1.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.simple_text, null);
        }

        service_name = (TextView) convertView.findViewById(R.id.service_name);

        if(flag){
            Services services = listItems.get(position);
            service_name.setText(""+services.getCategory_name());
        }else{
            HairType hairType = listItem1.get(position);
            service_name.setText(""+hairType.getName());
        }

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onBookNow(String id, String name);
    }

    public void notifyDataChanged(List<Services> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
