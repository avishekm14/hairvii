package com.karmickdroid.hairvii.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.ReviewModel;
import com.karmickdroid.hairvii.model.Staffs;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Administrator on 3/2/2016.
 */
public class ReviewRecycleAdapter extends RecyclerView.Adapter<ReviewRecycleAdapter.ServiceViewHolder>{

    OnServiceListClickListener onServiceClickListener;
    Context mContext;
    List<ReviewModel> mModelList;

    private static final int ITEM_VIEW_TYPE_LEFT = 0;
    private static final int ITEM_VIEW_TYPE_RIGHT = 1;

    public ReviewRecycleAdapter(Context mContext, List<ReviewModel> mModelList){
        this.mContext = mContext;
        this.mModelList = mModelList;
    }

    @Override
    public ReviewRecycleAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_review,
                parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewRecycleAdapter.ServiceViewHolder holder, final int position) {
        ReviewModel model = mModelList.get(position);

        URI uri = null;
        try {
            uri = new URI(model.getCustomer_pic().replaceAll(" ", "%20"));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(!uri.toString().isEmpty()){
            Picasso.with(mContext)
                    .load(uri.toString())
                    .error(R.drawable.logo)
                    .into(holder.iv_profile_pic);
        }else {
            Picasso.with(mContext)
                    .load(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(holder.iv_profile_pic);
        }

        if (model.getCustomer_name() != null && !model.getCustomer_name().isEmpty()) {
            String str;
            if (model.getEmployee_name().length() >= 25) {
                str = model.getCustomer_name().substring(0, 23) + "...";
            } else {
                str = model.getCustomer_name();
            }

            holder.service_provider_name.setText("" + model.getCustomer_name());
        }

        holder.rating_date.setText("["+model.getCreated_at()+"]");

        if(!model.getOverall_rating().isEmpty())
        holder.ratingbar.setRating(Float.parseFloat(model.getOverall_rating()));

        if(!model.getReview_note().isEmpty()){
            holder.staff_description.setText(""+model.getReview_note());
        }else{
            holder.staff_description.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    public void notifyDataChange(List<ReviewModel> mModelList) {
        this.mModelList = mModelList;
        notifyDataSetChanged();
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{

        LinearLayout ll=null;
        LinearLayout ll_service_container=null;
        ImageView iv_profile_pic;
        TextView service_provider_name,staff_description, rating_date;
        RatingBar ratingbar;
        Button btn_book_now;
        RelativeLayout rl_call;

        public ServiceViewHolder(View itemView) {
            super(itemView);

            ll = (LinearLayout) itemView;
            iv_profile_pic = (ImageView) itemView.findViewById(R.id.iv_profile_pic);
            service_provider_name = (TextView) itemView.findViewById(R.id.service_provider_name);
            ratingbar = (RatingBar) itemView.findViewById(R.id.ratingbar);
            staff_description = (TextView) itemView.findViewById(R.id.staff_description);
            rating_date = (TextView) itemView.findViewById(R.id.rating_date);
        }
    }

    public interface OnServiceListClickListener{
        public void onServiceClick(int position);
        public void onCall(int position);
    }
}
