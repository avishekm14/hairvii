package com.karmickdroid.hairvii.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.model.SpecialModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Administrator on 3/2/2016.
 */
public class ServiceRecycleAdapter extends RecyclerView.Adapter<ServiceRecycleAdapter.ServiceViewHolder>{

    OnServiceClickListener onServiceClickListener;
    Context mContext;
    List<ListModel> listModels;
    List<SpecialModel> specialList;

    private static final int ITEM_VIEW_TYPE_LEFT = 0;
    private static final int ITEM_VIEW_TYPE_RIGHT = 1;

    public ServiceRecycleAdapter(Context mContext,List<ListModel> listModels,  List<SpecialModel> specialList, OnServiceClickListener mListener){
        this.onServiceClickListener = mListener;
        this.mContext = mContext;
        this.listModels = listModels;
        this.specialList = specialList;
    }

    @Override
    public ServiceRecycleAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                viewType == ITEM_VIEW_TYPE_LEFT ? R.layout.row_service_left : R.layout.row_service_right,
                parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiceRecycleAdapter.ServiceViewHolder holder, final int position) {
        if(position< listModels.size()) {
            ListModel mModel = listModels.get(position);
            holder.ll_service_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onServiceClickListener.onServiceClick(position);
                }
            });

            String flag1=mModel.getCategory_icon();
            URI uri = null;
            try {
                uri = new URI(flag1.replaceAll(" ", "%20"));
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if(!uri.toString().isEmpty()) {
                Picasso.with(mContext)
                        .load(uri.toString())
                        .error(R.drawable.logo)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .into(holder.service_image);
            }else {
                Picasso.with(mContext)
                        .load(R.drawable.logo)
                        .error(R.drawable.logo)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .into(holder.service_image);
            }

            if (mModel.getCategory_name() != null && !mModel.getCategory_name().isEmpty()) {
                String str;
                if (mModel.getCategory_name().length() >= 35) {
                    str = mModel.getCategory_name().substring(0, 33) + "...";
                } else {
                    str = mModel.getCategory_name();
                }

                holder.service_name.setText("" + str);
            }

        }else{
            SpecialModel specialModel = specialList.get(position-listModels.size());

            holder.ll_service_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onServiceClickListener.onSpecialClick(position-listModels.size());
                }
            });

            Picasso.with(mContext)
                    .load(specialModel.getCategory_icon())
                    .error(R.drawable.logo)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .into(holder.service_image);

            holder.service_name.setText("" + specialModel.getCategory_name());
        }
    }

    @Override
    public int getItemCount() {
        return (listModels.size() + specialList.size());
    }

    @Override
    public int getItemViewType(int position) {
        return position%2 == 0 ? ITEM_VIEW_TYPE_LEFT : ITEM_VIEW_TYPE_RIGHT;
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout rl=null;
        LinearLayout ll_service_container=null;
        ImageView service_image;
        TextView service_name;

        public ServiceViewHolder(View itemView) {
            super(itemView);
            rl = (RelativeLayout)itemView;
            ll_service_container = (LinearLayout) itemView.findViewById(R.id.ll_service_container);
            service_image = (ImageView) itemView.findViewById(R.id.service_image);
            service_name = (TextView) itemView.findViewById(R.id.service_name);
        }
    }

    public void notifyDataChange(List<ListModel> listModels, List<SpecialModel> specialList){
        this.listModels = listModels;
        this.specialList = specialList;
        notifyDataSetChanged();
    }

    public interface OnServiceClickListener{
        public void onServiceClick(int position);
        public void onSpecialClick(int position);
    }
}
