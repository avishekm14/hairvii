package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.OperationHours;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class CancelPolicyLinearAdapter extends BaseAdapter {

    private Context context;
    private List<String> listItems;

    public CancelPolicyLinearAdapter(Context context, List<String> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.simple_text_with_checkbox, null);
        }

        TextView description = (TextView) convertView.findViewById(R.id.description);
        description.setText((position+1)+". "+listItems.get(position));

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onBookNow(String id, String name);
    }

    public void notifyDataChanged(List<String> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
