package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Gallery;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class ServicesLinearAdapter extends BaseAdapter implements SubServicesLinearAdapter.OnItemClickListener {

    private Context context;
    private List<Services> listItems;
    private OnItemClickListener clickListener;
    ExpandableLayout expandableLayout;
    TextView service_name;
    AdapterLinearLayout sub_service;
    SubServicesLinearAdapter mAdapter;

    public ServicesLinearAdapter(Context context, List<Services> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.exp_service, null);
        }

        expandableLayout = (ExpandableLayout) convertView.findViewById(R.id.expandableLayout);

        Services services = listItems.get(position);

        if(!services.isFlag()){
            expandableLayout.hide();
        }

        service_name = (TextView) expandableLayout.getHeaderLayout().findViewById(R.id.service_name);
        service_name.setText(""+services.getCategory_name());

        sub_service = (AdapterLinearLayout) expandableLayout.getContentLayout().findViewById(R.id.sub_service);

        mAdapter = new SubServicesLinearAdapter(context,services.getSubServicesList());
        mAdapter.setOnItemClickListener(this);
        sub_service.setAdapter(mAdapter);

        expandableLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener!=null)
                clickListener.onItemClick(position);
            }
        });

        return convertView;
    }

    @Override
    public void onItemClick(String id, String name) {
        if(clickListener!=null)
            clickListener.onBookNow(id, name);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onBookNow(String id, String name);
    }

    public void notifyDataChanged(List<Services> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }


}
