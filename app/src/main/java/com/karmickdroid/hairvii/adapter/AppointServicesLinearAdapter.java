package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.AppointServiceModel;
import com.karmickdroid.hairvii.model.SubServices;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class AppointServicesLinearAdapter extends BaseAdapter {

    private Context context;
    private List<AppointServiceModel> listItems;
    private OnItemClickListener clickListener;
    TextView service_name, start_time, end_time, employee_name;

    public AppointServicesLinearAdapter(Context context, List<AppointServiceModel> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_appoint_service, null);
        }

        AppointServiceModel mModel = listItems.get(position);

        service_name = (TextView) convertView.findViewById(R.id.service_name);
        start_time = (TextView) convertView.findViewById(R.id.start_time);
        end_time = (TextView) convertView.findViewById(R.id.end_time);
        employee_name = (TextView) convertView.findViewById(R.id.employee_name);

        service_name.setText(""+mModel.getService_name());
        start_time.setText(""+mModel.getStart_time());
        end_time.setText(""+mModel.getEnd_time());

        if(mModel.getEmployee_name()!=null) {
            employee_name.setText("" + mModel.getEmployee_name());
        }else{
            employee_name.setText("");
        }

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(String id, String name);
    }

    public void notifyDataChanged(List<AppointServiceModel> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
