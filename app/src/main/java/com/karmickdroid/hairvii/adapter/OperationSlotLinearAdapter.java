package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.OperationHours;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.util.HairType;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class OperationSlotLinearAdapter extends BaseAdapter {

    private Context context;
    private List<OperationHours> listItems;
    private SlotTimeLinearAdapter mAdapter;

    public OperationSlotLinearAdapter(Context context, List<OperationHours> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.simple_text1, null);
        }

        TextView day = (TextView) convertView.findViewById(R.id.day);
        AdapterLinearLayout time = (AdapterLinearLayout) convertView.findViewById(R.id.time);

        OperationHours operationHours = listItems.get(position);
        day.setText("" +operationHours.getDay());

        mAdapter = new SlotTimeLinearAdapter(context, operationHours.getSlotTimeList());
        time.setAdapter(mAdapter);
       /* try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm:ss");
            final Date dateObj = sdf.parse(operationHours.getStart_time());
            final Date dateObj1 = sdf.parse(operationHours.getEnd_time());

            String start_time =new SimpleDateFormat("K:mm a").format(dateObj);
            String end_time =new SimpleDateFormat("K:mm a").format(dateObj1);

            time.setText(""+start_time+" - "+end_time);

        } catch (final ParseException e) {
            e.printStackTrace();
        }
*/
        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onBookNow(String id, String name);
    }

    public void notifyDataChanged(List<OperationHours> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
