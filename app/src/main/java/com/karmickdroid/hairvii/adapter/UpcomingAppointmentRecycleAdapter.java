package com.karmickdroid.hairvii.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.model.PendingAppointment;
import com.karmickdroid.hairvii.model.SpecialModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 3/2/2016.
 */
public class UpcomingAppointmentRecycleAdapter extends RecyclerView.Adapter<UpcomingAppointmentRecycleAdapter.ServiceViewHolder>{

    OnServiceClickListener onServiceClickListener;
    Context mContext;
    List<PendingAppointment> listModels;

    public UpcomingAppointmentRecycleAdapter(Context mContext, List<PendingAppointment> listModels, OnServiceClickListener mListener){
        this.onServiceClickListener = mListener;
        this.mContext = mContext;
        this.listModels = listModels;
    }

    @Override
    public UpcomingAppointmentRecycleAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pending_appointment,
                parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UpcomingAppointmentRecycleAdapter.ServiceViewHolder holder, final int position) {
        PendingAppointment mModel = listModels.get(position);

        holder.business_name.setText(""+mModel.getBusiness_name());
        holder.appointment_date.setText(""+mModel.getAppointment_date());
        holder.appointment_time.setText(""+mModel.getAppointment_time());
        holder.reference_no.setText("" + mModel.getReference_id());
        holder.status.setText("" + mModel.getStatus());

        if(!mModel.getHome_service_address().isEmpty()) {
            holder.home_service_address.setText("" + mModel.getHome_service_address());
            holder.ll_tv_home_service_address.setVisibility(View.VISIBLE);
        }
        else {
            holder.ll_tv_home_service_address.setVisibility(View.GONE);
        }

        holder.btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onViewClick(position);
            }
        });

        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onCancelClick(position);
            }
        });

        holder.btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onConfirm(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{

        LinearLayout ll=null,ll_tv_home_service_address;
        TextView business_name,appointment_date,appointment_time,reference_no,home_service_address,tv_home_service_address,status;
        Button btn_view, btn_cancel, btn_confirm;

        public ServiceViewHolder(View itemView) {
            super(itemView);
            ll = (LinearLayout)itemView;
            ll_tv_home_service_address = (LinearLayout) ll.findViewById(R.id.ll_tv_home_service_address);
            home_service_address = (TextView) ll.findViewById(R.id.home_service_address);
            tv_home_service_address = (TextView) ll.findViewById(R.id.tv_home_service_address);
            business_name = (TextView) ll.findViewById(R.id.business_name);
            appointment_date = (TextView) ll.findViewById(R.id.appointment_date);
            appointment_time = (TextView) ll.findViewById(R.id.appointment_time);
            reference_no = (TextView) ll.findViewById(R.id.reference_no);
            status = (TextView) ll.findViewById(R.id.status);
            btn_view = (Button) ll.findViewById(R.id.btn_view);
            btn_cancel = (Button) ll.findViewById(R.id.btn_cancel);
            btn_confirm = (Button) ll.findViewById(R.id.btn_confirm);
        }
    }

    public void notifyDataChange(List<PendingAppointment> listModels){
        this.listModels = listModels;
        notifyDataSetChanged();
    }

    public interface OnServiceClickListener{
        void onViewClick(int position);
        void onCancelClick(int position);
        void onConfirm(int position);
    }
}
