package com.karmickdroid.hairvii.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.ServiceDetailModel;
import com.karmickdroid.hairvii.model.Staffs;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Administrator on 3/2/2016.
 */
public class OurStaffRecycleAdapter extends RecyclerView.Adapter<OurStaffRecycleAdapter.ServiceViewHolder>{

    OnServiceListClickListener onServiceClickListener;
    Context mContext;
    List<Staffs> mModelList;

    private static final int ITEM_VIEW_TYPE_LEFT = 0;
    private static final int ITEM_VIEW_TYPE_RIGHT = 1;

    public OurStaffRecycleAdapter(Context mContext, List<Staffs> mModelList, OnServiceListClickListener mListener){
        this.onServiceClickListener = mListener;
        this.mContext = mContext;
        this.mModelList = mModelList;
    }

    @Override
    public OurStaffRecycleAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_our_staff,
                parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OurStaffRecycleAdapter.ServiceViewHolder holder, final int position) {
        Staffs model = mModelList.get(position);

        URI uri = null;
        try {
            uri = new URI(model.getProfile_image().replaceAll(" ", "%20"));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(!uri.toString().isEmpty()) {
            Picasso.with(mContext)
                    .load(uri.toString())
                    .error(R.drawable.logo)
                    .into(holder.iv_profile_pic);
        }else{
            Picasso.with(mContext)
                    .load(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(holder.iv_profile_pic);
        }

        if (model.getEmployee_name() != null && !model.getEmployee_name().isEmpty()) {
            String str;
            if (model.getEmployee_name().length() >= 25) {
                str = model.getEmployee_name().substring(0, 23) + "...";
            } else {
                str = model.getEmployee_name();
            }

            holder.service_provider_name.setText("" + str);
        }

        if(!model.getRating().isEmpty())
        holder.ratingbar.setRating(Float.parseFloat(model.getRating()));

        if(!model.getDescription().isEmpty()){
            holder.staff_description.setText(""+model.getDescription());
        }else{
            holder.staff_description.setVisibility(View.GONE);
        }


        holder.btn_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onServiceClickListener.onServiceClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    public void notifyDataChange(List<Staffs> mModelList) {
        this.mModelList = mModelList;
        notifyDataSetChanged();
    }

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{

        LinearLayout ll=null;
        LinearLayout ll_service_container=null;
        ImageView iv_profile_pic;
        TextView service_provider_name,staff_description, rating_date;
        RatingBar ratingbar;
        Button btn_book_now;
        RelativeLayout rl_call;

        public ServiceViewHolder(View itemView) {
            super(itemView);

            ll = (LinearLayout) itemView;
            iv_profile_pic = (ImageView) itemView.findViewById(R.id.iv_profile_pic);
            service_provider_name = (TextView) itemView.findViewById(R.id.service_provider_name);
            ratingbar = (RatingBar) itemView.findViewById(R.id.ratingbar);
            staff_description = (TextView) itemView.findViewById(R.id.staff_description);
            rating_date = (TextView) itemView.findViewById(R.id.rating_date);
            btn_book_now = (Button) itemView.findViewById(R.id.btn_book_now);
        }
    }

    public interface OnServiceListClickListener{
        public void onServiceClick(int position);
        public void onCall(int position);
    }
}
