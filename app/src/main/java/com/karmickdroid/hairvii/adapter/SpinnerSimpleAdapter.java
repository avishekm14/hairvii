package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Staffs;
import com.karmickdroid.hairvii.util.CustomTextView;

import java.util.List;

/**
 * Created by dina on 19/1/16.
 */
public class SpinnerSimpleAdapter extends ArrayAdapter<Staffs> {
    private Activity mActivity = null;
    private int xmlLayoutResource = 0;
    private int COUNT = 0;
    private List<Staffs> arrSpinner;
    private LayoutInflater mLayoutInflater = null;
    String title;
    TextView tvName;
    boolean flag = false;

    public SpinnerSimpleAdapter(Activity activity, int resource, String title, List<Staffs> arr) {
        super(activity, resource);
        mActivity = activity;
        xmlLayoutResource = resource;
        arrSpinner = arr;
        COUNT = arrSpinner.size();
        this.title = title;
        mLayoutInflater = LayoutInflater.from(mActivity);
    }

    public SpinnerSimpleAdapter(Activity activity, List<Staffs> arr) {
        super(activity, R.layout.row_spinner);
        this.mActivity = activity;
        xmlLayoutResource = R.layout.row_spinner;
        arrSpinner = arr;
        COUNT = arrSpinner.size();
        mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Staffs getItem(int position) {
        return arrSpinner.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolderSpinner holder;
        if (convertView == null) {
            holder = new ViewHolderSpinner();
            convertView = mLayoutInflater.inflate(xmlLayoutResource, null);
            holder.tvName = (TextView) convertView
                    .findViewById(R.id.row_spinner_tvName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderSpinner) convertView.getTag();
        }


        Staffs objModel = getItem(position);
        if (null != objModel) {
            holder.tvName.setText(objModel.getEmployee_name());
        }

        tvName = holder.tvName;

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = null;

        if (position == 0) {
            convertView = mLayoutInflater.inflate(R.layout.row_dropdown1, null);
            CustomTextView tvDropDownName = (CustomTextView) convertView.findViewById(R.id.row_dropdown_tvName);
            tvDropDownName.setHeight(0);
            tvDropDownName.setVisibility(View.GONE);
            v = convertView;
        } else {
            convertView = mLayoutInflater.inflate(R.layout.row_dropdown1, null);
            CustomTextView tvDropDownName = (CustomTextView) convertView.findViewById(R.id.row_dropdown_tvName);
            Staffs objModel = getItem(position);
            if (null != objModel) {
                tvDropDownName.setText(objModel.getEmployee_name());
            }
            v = convertView;
        }

        parent.setVerticalScrollBarEnabled(false);
        return v;
    }

    public void notifyAdapter(List<Staffs> arr) {
        arrSpinner = arr;
        COUNT = arrSpinner.size();
        notifyDataSetChanged();
    }

    private static class ViewHolderSpinner {
        TextView tvName;
    }

    private static class ViewHolderDropDown {
        TextView tvDropDownName;

        public ViewHolderDropDown(View view) {
            LinearLayout llParent = (LinearLayout) view;
            this.tvDropDownName = (TextView) llParent.findViewById(R.id.row_dropdown_tvName);
        }
    }

}
