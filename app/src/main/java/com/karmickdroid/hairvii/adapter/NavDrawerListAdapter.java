package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.widgets.NavDrawerModel;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private List<NavDrawerModel> navDrawerItems;
    private OnItemClickListener clickListener;

    public NavDrawerListAdapter(Context context, List<NavDrawerModel> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_drawer_item, null);
        }

        RelativeLayout click_area_rl = (RelativeLayout) convertView.findViewById(R.id.click_area_rl);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.drawer_tvItemName);

        NavDrawerModel navDrawerModel = navDrawerItems.get(position);

        txtTitle.setText(navDrawerModel.getTitle());


        if (null != this.clickListener) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemClick(position);
                }
            });
        }


        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void notifyDataChanged(List<NavDrawerModel> navDrawerItems) {
        this.navDrawerItems = navDrawerItems;
        notifyDataSetChanged();
    }


}
