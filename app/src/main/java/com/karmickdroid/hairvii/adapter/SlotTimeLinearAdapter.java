package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SlotTime;
import com.karmickdroid.hairvii.util.HairType;
import com.karmickdroid.hairvii.widgets.AdapterLinearLayout;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class SlotTimeLinearAdapter extends BaseAdapter {

    private Context context;
    private List<SlotTime> listItems;
    private TextView time_slot;

    public SlotTimeLinearAdapter(Context context, List<SlotTime> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.simple_text, null);
        }

        SlotTime slotTime = listItems.get(position);

        time_slot = (TextView) convertView.findViewById(R.id.service_name);
        time_slot.setText(""+slotTime.getStart_time()+" - "+slotTime.getEnd_time());

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onBookNow(String id, String name);
    }

    public void notifyDataChanged(List<SlotTime> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
