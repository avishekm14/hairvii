package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.SubServices;

import java.util.List;

/**
 * Created by dina on 19/1/16.
 */
public class SpinnerAdapter extends ArrayAdapter<SubServices> {
    private Activity mActivity = null;
    private int xmlLayoutResource = 0;
    private int COUNT = 0;
    private List<SubServices> arrSpinner;
    private LayoutInflater mLayoutInflater = null;
    CustomSearchDialogCallback obj;
    private StringBuilder services = new StringBuilder("");
    TextView tvName;
    String title;

    public SpinnerAdapter(Activity activity, int resource,String title, List<SubServices> arr, CustomSearchDialogCallback obj) {
        super(activity, resource);
        mActivity = activity;
        xmlLayoutResource = resource;
        arrSpinner = arr;
        COUNT = arrSpinner.size();
        this.title= title;
        this.obj = obj;
        mLayoutInflater = LayoutInflater.from(mActivity);
    }

    public SpinnerAdapter(Activity activity,String title, List<SubServices> arr, CustomSearchDialogCallback obj) {
        super(activity, R.layout.row_spinner);
        this.mActivity = activity;
        xmlLayoutResource = R.layout.row_spinner;
        arrSpinner = arr;
        COUNT = arrSpinner.size();
        this.obj = obj;
        this.title= title;
        mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public SubServices getItem(int position) {
        return arrSpinner.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolderSpinner holder;
        if (convertView == null) {
            holder = new ViewHolderSpinner();
            convertView = mLayoutInflater.inflate(xmlLayoutResource, null);
            holder.tvName = (TextView) convertView
                    .findViewById(R.id.row_spinner_tvName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderSpinner) convertView.getTag();
        }

        if(!title.isEmpty()) {
            holder.tvName.setText("" + title);
        }else{
            holder.tvName.setText("Select Service");
        }
        tvName =holder.tvName;

        return convertView;
    }

    @Override
    public View getDropDownView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderDropDown holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.row_dropdown, null);
            holder = new ViewHolderDropDown(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderDropDown) convertView.getTag();
        }
        SubServices objModel = arrSpinner.get(position);
        if (null != objModel) {
            holder.tvDropDownName.setText(objModel.getService_name());
            holder.checkBox.setChecked(objModel.isChecked());
            holder.checkBox.setTag(position);
        }

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrSpinner.get((int)holder.checkBox.getTag()).setChecked(holder.checkBox.isChecked());

                services = new StringBuilder("");
                StringBuilder servicesIds = new StringBuilder("");

                int i = 0;
                for (SubServices service : arrSpinner) {

                    if (service.isChecked()) {
                        if (i == 0) {
                            services.append("" + service.getService_name());
                            servicesIds.append("" + service.getId());
                            i++;
                        } else {
                            services.append("; " + service.getService_name());
                            servicesIds.append("," + service.getId());
                        }
                    }
                }

                tvName.setText(""+services.toString());
                obj.onItemClick(services.toString(), servicesIds.toString());
            }
        });
        return convertView;
    }

    public void notifyAdapter(List<SubServices> arr) {
        arrSpinner = arr;
        COUNT = arrSpinner.size();
        notifyDataSetChanged();
    }

    private static class ViewHolderSpinner {
        TextView tvName;
    }

    private static class ViewHolderDropDown {
        TextView tvDropDownName;
        CheckBox checkBox;
        TextView tvName;

        public ViewHolderDropDown(View view) {
            LinearLayout llParent = (LinearLayout) view;
            this.tvDropDownName = (TextView) llParent.findViewById(R.id.row_dropdown_tvName);
            this.tvName = (TextView) llParent.findViewById(R.id.row_spinner_tvName);
            this.checkBox = (CheckBox) llParent.findViewById(R.id.checkBox);
        }
    }

    public interface CustomSearchDialogCallback {
        public void onItemClick(String Service, String ids);
    }

}
