package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SubServices;

import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class SubServicesLinearAdapter extends BaseAdapter {

    private Context context;
    private List<SubServices> listItems;
    private OnItemClickListener clickListener;

    public SubServicesLinearAdapter(Context context, List<SubServices> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.view_containt, null);
        }

        final SubServices services = listItems.get(position);

        TextView services_description = (TextView) convertView.findViewById(R.id.services_description);

        TextView time = (TextView) convertView.findViewById(R.id.time);

        TextView price = (TextView) convertView.findViewById(R.id.price);

        Button btn_book_now = (Button) convertView.findViewById(R.id.btn_book_now);

        if (services.getService_name() != null && !services.getService_name().isEmpty()) {
            String str;
            if (services.getService_name().length() >= 25) {
                str = services.getService_name().substring(0, 23) + "...";
            } else {
                str = services.getService_name();
            }

            services_description.setText("" + str);
        }

        time.setText(""+services.getService_time());
        price.setText(""+services.getDiscount_price()+" ("+services.getDiscount()+" Saving)");

        btn_book_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener!=null){
                    clickListener.onItemClick(services.getId(), services.getService_name());
                }
            }
        });

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(String id, String name);
    }

    public void notifyDataChanged(List<SubServices> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

}
