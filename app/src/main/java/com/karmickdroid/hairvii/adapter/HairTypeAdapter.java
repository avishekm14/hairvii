package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.util.HairType;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class HairTypeAdapter extends BaseAdapter {

    private Context context;
    private List<HairType> hairTypeList;
    private OnItemClickListener clickListener;
    TextView txtTitle;
    CheckBox checkBox;

    List<CheckBox> checkBoxList = new ArrayList<>();

    LinkedList<String> ids = new LinkedList<>();

    public HairTypeAdapter(Context context, List<HairType> hairTypeList) {
        this.context = context;
        this.hairTypeList = hairTypeList;
    }

    @Override
    public int getCount() {
        return hairTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return hairTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.checkbox_with_text, null);

        txtTitle = (TextView) convertView.findViewById(R.id.text);
        checkBox = (CheckBox) convertView.findViewById(R.id.id);
        checkBox.setTag(position);

        HairType hairType = hairTypeList.get(position);

        txtTitle.setText(""+hairType.getName());

        if(hairType.isCheckBoolean()){
            checkBox.setChecked(true);
        }

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer pos = (Integer) ((CheckBox) v).getTag();

                if (clickListener != null)
                    clickListener.onItemChecked(pos);
            }
        });

        checkBoxList.add(checkBox);

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onItemChecked(int position);
    }

    public void notifyDataChanged(List<HairType> hairTypeList) {
        this.hairTypeList = hairTypeList;
        notifyDataSetChanged();
    }

}
