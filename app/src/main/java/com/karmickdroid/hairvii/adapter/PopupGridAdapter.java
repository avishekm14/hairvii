package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.model.TimeSlot;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Administrator on 2/5/2016.
 */
public class PopupGridAdapter extends BaseAdapter {

    BaseActivity mActivity;
    Context mContext;
    List<TimeSlot> mProductDetailList;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    OnItemClick mListener;

    public PopupGridAdapter() {

    }

    public PopupGridAdapter(Activity mActivity, List<TimeSlot> mProductDetailList, OnItemClick mListener) {
        this.mContext = mActivity;
        this.mProductDetailList = mProductDetailList;
        this.mListener=mListener;
    }

    @Override
    public int getCount() {
        return mProductDetailList.size();
    }

    @Override
    public Object getItem(int position) {
        return mProductDetailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mProductDetailList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.avg_single_cell, parent, false);
        TextView time = (TextView) mView.findViewById(R.id.time);
        ImageView img = (ImageView) mView.findViewById(R.id.img);
        LinearLayout cell = (LinearLayout) mView.findViewById(R.id.cell);

        time.setText(""+mProductDetailList.get(position).getTime());

        /*if(mProductDetailList.get(position).getExtra_time().equalsIgnoreCase("y")){
            img.setImageResource(R.drawable.ic_time1);
            time.setTextColor(mContext.getResources().getColor(R.color.red));
        }*/

        cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });

        return mView;
    }

    public interface OnItemClick{
        void onItemClick(int position);
    }
}
