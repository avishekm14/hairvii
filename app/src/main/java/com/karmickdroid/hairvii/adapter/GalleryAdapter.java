package com.karmickdroid.hairvii.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.model.Gallery;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Administrator on 5/26/2015.
 */
public class GalleryAdapter extends BaseAdapter {

    private Context context;
    private List<Gallery> galleryItems;
    private OnItemClickListener clickListener;

    public GalleryAdapter(Context context, List<Gallery> galleryItems) {
        this.context = context;
        this.galleryItems = galleryItems;
    }

    @Override
    public int getCount() {
        return galleryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return galleryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.gallery_img, null);
        }

        final ImageView iv_profile_pic = (ImageView) convertView.findViewById(R.id.iv_profile_pic);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.tv_description);

        Gallery gallery = galleryItems.get(position);

        String flag1=gallery.getGallery_image();
        URI uri = null;
        try {
            uri = new URI(flag1.replaceAll(" ", "%20"));
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(!uri.toString().isEmpty())
        Picasso.with(context).load(uri.toString()).error(R.drawable.banner).into(iv_profile_pic);
        else
            Picasso.with(context).load(R.drawable.banner).error(R.drawable.banner).into(iv_profile_pic);

        if(gallery.getLinked_to_service().equalsIgnoreCase("y")) {
            txtTitle.setText("" + gallery.getService_description()+"["+gallery.getService_online_price()+"]");
        }else{
            txtTitle.setText("" +gallery.getDescription());
        }

        return convertView;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void notifyDataChanged(List<Gallery> galleryItems) {
        this.galleryItems = galleryItems;
        notifyDataSetChanged();
    }


}
