package com.karmickdroid.hairvii.libs.forms;

public abstract class SubmitHandler {
	public abstract void submit(Form form);
}
