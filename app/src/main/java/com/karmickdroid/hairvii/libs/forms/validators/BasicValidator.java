package com.karmickdroid.hairvii.libs.forms.validators;


import com.karmickdroid.hairvii.libs.forms.Form;

public abstract class BasicValidator {
	public abstract String validate(Form form, String thisname);
}
