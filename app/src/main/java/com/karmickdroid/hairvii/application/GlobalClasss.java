package com.karmickdroid.hairvii.application;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.karmickdroid.hairvii.libs.icenet.IceNet;
import com.karmickdroid.hairvii.libs.icenet.IceNetConfig;
import com.karmickdroid.hairvii.util.CONST;
import com.karmickdroid.hairvii.util.FontsSeter;

import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;

/**
 * Created by techierishi on 8/11/15.
 */

public class GlobalClasss extends Application implements CONST {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        FontsSeter.setDefaultFont(this, "DEFAULT", "fonts/Dosis-Regular.ttf");
        FontsSeter.setDefaultFont(this, "MONOSPACE", "fonts/Dosis-Regular.ttf");
        FontsSeter.setDefaultFont(this, "SERIF", "fonts/Dosis-Regular.ttf");
        FontsSeter.setDefaultFont(this, "SANS_SERIF", "fonts/Dosis-Regular.ttf");

        initIceNet();
        Paper.init(getApplicationContext());
    }

    public void initIceNet() {
        IceNetConfig config = new IceNetConfig.Builder()
                .setBaseUrl(REST_API.BASE_URL)
                //.setBaseUrl("http://192.168.1.32/upload/")
                .setContext(getApplicationContext())
                .build();
        IceNet.init(config);
    }
}
