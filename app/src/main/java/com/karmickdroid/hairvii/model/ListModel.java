package com.karmickdroid.hairvii.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Administrator on 3/8/2016.
 */
public class ListModel {
    String id, category_icon, category_name, pin_to_mobile_app, left, right;

    public String getPin_to_mobile_app() {
        return pin_to_mobile_app;
    }

    public void setPin_to_mobile_app(String pin_to_mobile_app) {
        this.pin_to_mobile_app = pin_to_mobile_app;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_icon() {
        return category_icon;
    }

    public void setCategory_icon(String category_icon) {
        this.category_icon = category_icon;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "ListModel{" +
                "id='" + id + '\'' +
                ", category_icon='" + category_icon + '\'' +
                ", category_name='" + category_name + '\'' +
                ", parent_id='" + pin_to_mobile_app + '\'' +
                ", left='" + left + '\'' +
                ", right='" + right + '\'' +
                '}';
    }
}
