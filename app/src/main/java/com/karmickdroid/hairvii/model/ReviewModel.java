package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 4/11/2016.
 */
public class ReviewModel {
    String id, customer_id, business_id, employee_id, review_note, created_at;
    String is_approved,punctuality_rate,value_rate,service_rate,overall_rating;
    String customer_name,customer_pic,employee_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getReview_note() {
        return review_note;
    }

    public void setReview_note(String review_note) {
        this.review_note = review_note;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIs_approved() {
        return is_approved;
    }

    public void setIs_approved(String is_approved) {
        this.is_approved = is_approved;
    }

    public String getPunctuality_rate() {
        return punctuality_rate;
    }

    public void setPunctuality_rate(String punctuality_rate) {
        this.punctuality_rate = punctuality_rate;
    }

    public String getValue_rate() {
        return value_rate;
    }

    public void setValue_rate(String value_rate) {
        this.value_rate = value_rate;
    }

    public String getService_rate() {
        return service_rate;
    }

    public void setService_rate(String service_rate) {
        this.service_rate = service_rate;
    }

    public String getOverall_rating() {
        return overall_rating;
    }

    public void setOverall_rating(String overall_rating) {
        this.overall_rating = overall_rating;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_pic() {
        if(customer_pic!=null)
        return customer_pic;
        else
            return "";
    }

    public void setCustomer_pic(String customer_pic) {
        this.customer_pic = customer_pic;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }
}
