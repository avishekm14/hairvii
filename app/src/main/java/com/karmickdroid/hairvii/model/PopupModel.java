package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 4/2/2016.
 */
public class PopupModel {
    int res;
    String title;

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
