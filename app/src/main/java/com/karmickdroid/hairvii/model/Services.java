package com.karmickdroid.hairvii.model;

import java.util.List;

/**
 * Created by Administrator on 3/30/2016.
 */
public class Services {
    String category_id, category_name;
    List<SubServices> subServicesList;
    boolean flag=false;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public List<SubServices> getSubServicesList() {
        return subServicesList;
    }

    public void setSubServicesList(List<SubServices> subServicesList) {
        this.subServicesList = subServicesList;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
