package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 3/16/2016.
 */
public class SpecialModel {
    String id, category_name, parent_id, serviceType;
    int category_icon;

    public int getCategory_icon() {
        return category_icon;
    }

    public void setCategory_icon(int category_icon) {
        this.category_icon = category_icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
