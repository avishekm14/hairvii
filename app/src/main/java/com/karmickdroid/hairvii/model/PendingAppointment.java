package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 4/29/2016.
 */
public class PendingAppointment {
    String id, business_id, business_name, appointment_date,appointment_time,is_complete_customer;
    String reference_id,booking_date, base_amount, processing_fee, total_amount,status,home_service_address,appointment_endtime;

    public String getIs_complete_customer() {
        return is_complete_customer;
    }

    public void setIs_complete_customer(String is_complete_customer) {
        this.is_complete_customer = is_complete_customer;
    }

    public String getAppointment_endtime() {
        return appointment_endtime;
    }

    public void setAppointment_endtime(String appointment_endtime) {
        this.appointment_endtime = appointment_endtime;
    }

    public String getHome_service_address() {
        if(home_service_address!=null)
            return home_service_address;
        else
            return "";
    }

    public void setHome_service_address(String home_service_address) {
        this.home_service_address = home_service_address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getBase_amount() {
        return base_amount;
    }

    public void setBase_amount(String base_amount) {
        this.base_amount = base_amount;
    }

    public String getProcessing_fee() {
        return processing_fee;
    }

    public void setProcessing_fee(String processing_fee) {
        this.processing_fee = processing_fee;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
