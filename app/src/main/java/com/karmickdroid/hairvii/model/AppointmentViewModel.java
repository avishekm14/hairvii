package com.karmickdroid.hairvii.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Administrator on 4/29/2016.
 */
public class AppointmentViewModel {
    String id, appointment_date, appointment_time, reference_id, customer_id, business_id;
    String business_name, total_amount, service_amount, processing_fee, booking_date, is_complete;
    String cancelled_by_customer, cancelled_by_business, no_show_cancellation, refund_transaction_id;
    String cancellation_date, refund_date, refund_amount, note, refund_flag, cancellation_reason,extra_hours_charge;
    String cancellation_policy_string,policy_text, cancellation_action, cancellation_hours, deduction_percent;

    public String getCancellation_policy_string() {
        return cancellation_policy_string;
    }

    public void setCancellation_policy_string(JSONObject jsonObject) {
            setPolicy_text(jsonObject.optString("policy_text"));
            setCancellation_action(jsonObject.optString("cancellation_action"));
            setCancellation_hours(jsonObject.optString("cancellation_hours"));
            setDeduction_percent(jsonObject.optString("deduction_percent"));
    }

    public String getPolicy_text() {
        return policy_text;
    }

    public void setPolicy_text(String policy_text) {
        this.policy_text = policy_text;
    }

    public String getCancellation_action() {
        return cancellation_action;
    }

    public void setCancellation_action(String cancellation_action) {
        this.cancellation_action = cancellation_action;
    }

    public String getCancellation_hours() {
        return cancellation_hours;
    }

    public void setCancellation_hours(String cancellation_hours) {
        this.cancellation_hours = cancellation_hours;
    }

    public String getDeduction_percent() {
        return deduction_percent;
    }

    public void setDeduction_percent(String deduction_percent) {
        this.deduction_percent = deduction_percent;
    }

    public String getExtra_hours_charge() {
        return extra_hours_charge;
    }

    public void setExtra_hours_charge(String extra_hours_charge) {
        this.extra_hours_charge = extra_hours_charge;
    }

    List<AppointServiceModel> appointmentServiceList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getAppointment_time() {
        return appointment_time;
    }

    public void setAppointment_time(String appointment_time) {
        this.appointment_time = appointment_time;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getService_amount() {
        return service_amount;
    }

    public void setService_amount(String service_amount) {
        this.service_amount = service_amount;
    }

    public String getProcessing_fee() {
        return processing_fee;
    }

    public void setProcessing_fee(String processing_fee) {
        this.processing_fee = processing_fee;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getIs_complete() {
        return is_complete;
    }

    public void setIs_complete(String is_complete) {
        this.is_complete = is_complete;
    }

    public String getCancelled_by_customer() {
        return cancelled_by_customer;
    }

    public void setCancelled_by_customer(String cancelled_by_customer) {
        this.cancelled_by_customer = cancelled_by_customer;
    }

    public String getCancelled_by_business() {
        return cancelled_by_business;
    }

    public void setCancelled_by_business(String cancelled_by_business) {
        this.cancelled_by_business = cancelled_by_business;
    }

    public String getNo_show_cancellation() {
        return no_show_cancellation;
    }

    public void setNo_show_cancellation(String no_show_cancellation) {
        this.no_show_cancellation = no_show_cancellation;
    }

    public String getRefund_transaction_id() {
        return refund_transaction_id;
    }

    public void setRefund_transaction_id(String refund_transaction_id) {
        this.refund_transaction_id = refund_transaction_id;
    }

    public String getCancellation_date() {
        return cancellation_date;
    }

    public void setCancellation_date(String cancellation_date) {
        this.cancellation_date = cancellation_date;
    }

    public String getRefund_date() {
        return refund_date;
    }

    public void setRefund_date(String refund_date) {
        this.refund_date = refund_date;
    }

    public String getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(String refund_amount) {
        this.refund_amount = refund_amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRefund_flag() {
        return refund_flag;
    }

    public void setRefund_flag(String refund_flag) {
        this.refund_flag = refund_flag;
    }

    public String getCancellation_reason() {
        return cancellation_reason;
    }

    public void setCancellation_reason(String cancellation_reason) {
        this.cancellation_reason = cancellation_reason;
    }

    public List<AppointServiceModel> getAppointmentServiceList() {
        return appointmentServiceList;
    }

    public void setAppointmentServiceList(List<AppointServiceModel> appointmentServiceList) {
        this.appointmentServiceList = appointmentServiceList;
    }
}
