package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 3/30/2016.
 */
public class Gallery {
    String id, business_id, gallery_image, uploaded_on, is_default, image_title, description;
    String linked_to_service, service_id, service_name, service_online_price, service_description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public String getGallery_image() {
        if (gallery_image != null)
            return gallery_image;
        else
            return "";
    }

    public void setGallery_image(String gallery_image) {
        this.gallery_image = gallery_image;
    }

    public String getUploaded_on() {
        return uploaded_on;
    }

    public void setUploaded_on(String uploaded_on) {
        this.uploaded_on = uploaded_on;
    }

    public String getIs_default() {
        return is_default;
    }

    public void setIs_default(String is_default) {
        this.is_default = is_default;
    }

    public String getImage_title() {
        return image_title;
    }

    public void setImage_title(String image_title) {
        this.image_title = image_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLinked_to_service() {
        return linked_to_service;
    }

    public void setLinked_to_service(String linked_to_service) {
        this.linked_to_service = linked_to_service;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_online_price() {
        return service_online_price;
    }

    public void setService_online_price(String service_online_price) {
        this.service_online_price = service_online_price;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }
}
