package com.karmickdroid.hairvii.model;

import java.util.List;

/**
 * Created by Administrator on 3/16/2016.
 */
public class ServiceDetailModel {
    String id,business_name,business_type,user_id, owner_name,accept_walkin, parking_available, wifi_access;
    String handicap_access,tv,beer_n_wine_bar,kids_friendly,service_men,service_women,service_kids;
    String creation_date,distance,rating,review_count,website,about_note,business_logo;
    String email_id,mobile_no,addr1,addr2,area_name,location_name,city,pincode,country_id,country_name,latitude,longitude;
    String membership_plan_id,membership_plan_name,membership_plan_expiry,app_share_count,favourite_count;
    String license_no;
    String license_reg_state;
    String paid_promotion_active;
    String banner_image,extra_hours_flag,extra_hours_charge,share_url ;

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getExtra_hours_charge() {
        return extra_hours_charge;
    }

    public void setExtra_hours_charge(String extra_hours_charge) {
        this.extra_hours_charge = extra_hours_charge;
    }

    public String getExtra_hours_flag() {
        return extra_hours_flag;
    }

    public void setExtra_hours_flag(String extra_hours_flag) {
        this.extra_hours_flag = extra_hours_flag;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    String is_favourite;

    List<Gallery> galleryList;
    List<OperationHours> operationHoursList;
    List<Services> servicesList;
    List<Staffs> staffsList;
    List<ReviewModel> reviewList;

    public List<String> getCancellation_policies() {
        return cancellation_policies;
    }

    public void setCancellation_policies(List<String> cancellation_policies) {
        this.cancellation_policies = cancellation_policies;
    }

    List<String> cancellation_policies;

    public List<ReviewModel> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<ReviewModel> reviewList) {
        this.reviewList = reviewList;
    }

    public List<Gallery> getGalleryList() {
        return galleryList;
    }

    public void setGalleryList(List<Gallery> galleryList) {
        this.galleryList = galleryList;
    }

    public List<OperationHours> getOperationHoursList() {
        return operationHoursList;
    }

    public void setOperationHoursList(List<OperationHours> operationHoursList) {
        this.operationHoursList = operationHoursList;
    }

    public List<Services> getServicesList() {
        return servicesList;
    }

    public void setServicesList(List<Services> servicesList) {
        this.servicesList = servicesList;
    }

    public List<Staffs> getStaffsList() {
        return staffsList;
    }

    public void setStaffsList(List<Staffs> staffsList) {
        this.staffsList = staffsList;
    }

    public String getLicense_no() {
        return license_no;
    }

    public void setLicense_no(String license_no) {
        this.license_no = license_no;
    }

    public String getLicense_reg_state() {
        return license_reg_state;
    }

    public void setLicense_reg_state(String license_reg_state) {
        this.license_reg_state = license_reg_state;
    }

    public String getPaid_promotion_active() {
        return paid_promotion_active;
    }

    public void setPaid_promotion_active(String paid_promotion_active) {
        this.paid_promotion_active = paid_promotion_active;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getAccept_walkin() {
        return accept_walkin;
    }

    public void setAccept_walkin(String accept_walkin) {
        this.accept_walkin = accept_walkin;
    }

    public String getParking_available() {
        return parking_available;
    }

    public void setParking_available(String parking_available) {
        this.parking_available = parking_available;
    }

    public String getWifi_access() {
        return wifi_access;
    }

    public void setWifi_access(String wifi_access) {
        this.wifi_access = wifi_access;
    }

    public String getHandicap_access() {
        return handicap_access;
    }

    public void setHandicap_access(String handicap_access) {
        this.handicap_access = handicap_access;
    }

    public String getTv() {
        return tv;
    }

    public void setTv(String tv) {
        this.tv = tv;
    }

    public String getBeer_n_wine_bar() {
        return beer_n_wine_bar;
    }

    public void setBeer_n_wine_bar(String beer_n_wine_bar) {
        this.beer_n_wine_bar = beer_n_wine_bar;
    }

    public String getKids_friendly() {
        return kids_friendly;
    }

    public void setKids_friendly(String kids_friendly) {
        this.kids_friendly = kids_friendly;
    }

    public String getService_men() {
        return service_men;
    }

    public void setService_men(String service_men) {
        this.service_men = service_men;
    }

    public String getService_women() {
        return service_women;
    }

    public void setService_women(String service_women) {
        this.service_women = service_women;
    }

    public String getService_kids() {
        return service_kids;
    }

    public void setService_kids(String service_kids) {
        this.service_kids = service_kids;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview_count() {
        return review_count;
    }

    public void setReview_count(String review_count) {
        this.review_count = review_count;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAbout_note() {
        return about_note;
    }

    public void setAbout_note(String about_note) {
        this.about_note = about_note;
    }

    public String getBusiness_logo() {
        return business_logo;
    }

    public void setBusiness_logo(String business_logo) {
        this.business_logo = business_logo;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMembership_plan_id() {
        return membership_plan_id;
    }

    public void setMembership_plan_id(String membership_plan_id) {
        this.membership_plan_id = membership_plan_id;
    }

    public String getMembership_plan_name() {
        return membership_plan_name;
    }

    public void setMembership_plan_name(String membership_plan_name) {
        this.membership_plan_name = membership_plan_name;
    }

    public String getMembership_plan_expiry() {
        return membership_plan_expiry;
    }

    public void setMembership_plan_expiry(String membership_plan_expiry) {
        this.membership_plan_expiry = membership_plan_expiry;
    }

    public String getApp_share_count() {
        return app_share_count;
    }

    public void setApp_share_count(String app_share_count) {
        this.app_share_count = app_share_count;
    }

    public String getFavourite_count() {
        return favourite_count;
    }

    public void setFavourite_count(String favourite_count) {
        this.favourite_count = favourite_count;
    }
}
