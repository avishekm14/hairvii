package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 4/28/2016.
 */
public class MonthModel {
    String monthId;
    String monthName;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getMonthId() {
        return monthId;
    }

    public void setMonthId(String monthId) {
        this.monthId = monthId;
    }
}
