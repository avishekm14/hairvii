package com.karmickdroid.hairvii.model;

import java.util.List;

/**
 * Created by Administrator on 3/30/2016.
 */
public class OperationHours {
    String id, business_id, weekday, slot_name, day;
    List<SlotTime> slotTimeList;

    public List<SlotTime> getSlotTimeList() {
        return slotTimeList;
    }

    public void setSlotTimeList(List<SlotTime> slotTimeList) {
        this.slotTimeList = slotTimeList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getSlot_name() {
        return slot_name;
    }

    public void setSlot_name(String slot_name) {
        this.slot_name = slot_name;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
