package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 4/27/2016.
 */
public class TimeSlot {
    String time, employee_id, extra_time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getExtra_time() {
        return extra_time;
    }

    public void setExtra_time(String extra_time) {
        this.extra_time = extra_time;
    }
}
