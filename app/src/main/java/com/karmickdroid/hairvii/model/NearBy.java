package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 5/12/2016.
 */
public class NearBy {
    String id="", icon="", business_name="", type="", address="";
    String place_id="", distance="", search_latlon="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSearch_latlon() {
        return search_latlon;
    }

    public void setSearch_latlon(String search_latlon) {
        this.search_latlon = search_latlon;
    }
}
