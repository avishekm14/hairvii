package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 4/27/2016.
 */
public class AppointmentService {
    String service_id, service_date, extratime, extraHourCharge, employee_id, business_id;
    String day_number, time, end_time, service_name, service_amount;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_date() {
        return service_date;
    }

    public void setService_date(String service_date) {
        this.service_date = service_date;
    }

    public String getExtratime() {
        return extratime;
    }

    public void setExtratime(String extratime) {
        this.extratime = extratime;
    }

    public String getExtraHourCharge() {
        return extraHourCharge;
    }

    public void setExtraHourCharge(String extraHourCharge) {
        this.extraHourCharge = extraHourCharge;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public String getDay_number() {
        return day_number;
    }

    public void setDay_number(String day_number) {
        this.day_number = day_number;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_amount() {
        return service_amount;
    }

    public void setService_amount(String service_amount) {
        this.service_amount = service_amount;
    }
}
