package com.karmickdroid.hairvii.model;

/**
 * Created by Administrator on 5/9/2016.
 */
public class SlotTime {
    String start_time, end_time;

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
