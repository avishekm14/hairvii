package com.karmickdroid.hairvii.widgets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.PopupGridAdapter;
import com.karmickdroid.hairvii.adapter.ServicesBookedLinearAdapter;
import com.karmickdroid.hairvii.model.AppointmentService;
import com.karmickdroid.hairvii.model.TimeSlot;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Administrator on 4/27/2016.
 */
public class CustomTimeSlotDialog extends DialogFragment {

    private View mView;
    List<TimeSlot> mList;
    OnSubmitClickListener mListener;
    private PopupGridAdapter popupGridAdapter;
    GridView timeslot_gridview;
    ImageView iv_cancel;
    TextView tv_top_indicator;
    String flag;

    public CustomTimeSlotDialog() {

    }

    public void setOnSubmitClickListener(List<TimeSlot> mList,String flag, OnSubmitClickListener mListener) {
        this.mList = mList;
        this.flag = flag;
        this.mListener = mListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.time_slot, container);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        timeslot_gridview = (GridView) mView.findViewById(R.id.timeslot_gridview);
        iv_cancel = (ImageView) mView.findViewById(R.id.iv_cancel);
        tv_top_indicator = (TextView) mView.findViewById(R.id.tv_top_indicator);
        setData();
        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void setData() {
        if(flag.equalsIgnoreCase("Y")){
            tv_top_indicator.setText("Home Service Timing");
        }else{
            tv_top_indicator.setText("Shop Service Timing");
        }

        popupGridAdapter = new PopupGridAdapter(getActivity(), mList, new PopupGridAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position) {
                dismiss();
                mListener.onClick(position);
            }
        });
        timeslot_gridview.setAdapter(popupGridAdapter);
    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Fetch arguments from bundle and set title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Show soft keyboard automatically and request focus to field
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

    }

    public interface OnSubmitClickListener {
        void onClick(int position);
    }
}
