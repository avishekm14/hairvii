package com.karmickdroid.hairvii.widgets;

/**
 * Created by Administrator on 3/18/2016.
 */
public class SearchData {
    int rating=0;
    String business="0";
    String distance="";
    int free_parking=0, beer_and_wine=0, kid_friendly=0, wifi_access=0, handicap_access=0, accept_walkin=0, tv=0, home_service=0;
    String ids="";

    public int getHome_service() {
        return home_service;
    }

    public void setHome_service(int home_service) {
        this.home_service = home_service;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getFree_parking() {
        return free_parking;
    }

    public void setFree_parking(int free_parking) {
        this.free_parking = free_parking;
    }

    public int getBeer_and_wine() {
        return beer_and_wine;
    }

    public void setBeer_and_wine(int beer_and_wine) {
        this.beer_and_wine = beer_and_wine;
    }

    public int getKid_friendly() {
        return kid_friendly;
    }

    public void setKid_friendly(int kid_friendly) {
        this.kid_friendly = kid_friendly;
    }

    public int getWifi_access() {
        return wifi_access;
    }

    public void setWifi_access(int wifi_access) {
        this.wifi_access = wifi_access;
    }

    public int getHandicap_access() {
        return handicap_access;
    }

    public void setHandicap_access(int handicap_access) {
        this.handicap_access = handicap_access;
    }

    public int getAccept_walkin() {
        return accept_walkin;
    }

    public void setAccept_walkin(int accept_walkin) {
        this.accept_walkin = accept_walkin;
    }

    public int getTv() {
        return tv;
    }

    public void setTv(int tv) {
        this.tv = tv;
    }

    @Override
    public String toString() {
        return "SearchData{" +
                "rating=" + rating +
                ", business=" + business +
                ", distance=" + distance +
                ", free_parking=" + free_parking +
                ", beer_and_wine=" + beer_and_wine +
                ", kid_friendly=" + kid_friendly +
                ", wifi_access=" + wifi_access +
                ", handicap_access=" + handicap_access +
                ", accept_walkin=" + accept_walkin +
                ", tv=" + tv +
                ", home_service"+home_service+
                ", ids=" + ids +
                '}';
    }
}
