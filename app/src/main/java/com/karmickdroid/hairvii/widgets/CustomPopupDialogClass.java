package com.karmickdroid.hairvii.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.model.PopupModel;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/2/2016.
 */
public class CustomPopupDialogClass {
    PopupWindow popupWindow;
    ListAdpater dataAdapter;
    ArrayList<PopupModel> mList;
    Context ctx;
    CustomDialogCallback obj;

    public CustomPopupDialogClass(Context mContext, ArrayList<PopupModel> modelArrayList){
        this.ctx = mContext;
        this.mList = modelArrayList;
    }

    public void onPopupButtonClick(View button) {
        LayoutInflater layoutInflater
                = (LayoutInflater) ctx
                .getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.drop_down, null);
        popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.update();
        popupWindow.setWidth(((DetailActivity)ctx).getHorizontalRatio(160));

        //popupWindow.setAnimationStyle(R.style.spinner_animation);

        ListView listView = (ListView) popupView.findViewById(R.id.listView1);

        dataAdapter = new ListAdpater(ctx, mList);
        listView.setAdapter(dataAdapter);
        listView.setDivider(null);
        listView.setDividerHeight(0);

        listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view,
                                    int position, long id) {
                popupWindow.dismiss();

                if(obj!=null)
                obj.onItemClick(position);
            }
        });

        popupWindow.showAsDropDown(button, -(popupWindow.getWidth()-((DetailActivity)ctx).getHorizontalRatio(40)), -10);

    }

    public void setOnCustomDialogCallback(CustomDialogCallback obj){
        this.obj = obj;
    }

    public interface CustomDialogCallback {
        public void onItemClick(int pos);
    }

    class ListAdpater extends BaseAdapter {

        private Activity activity;
        private ArrayList<PopupModel> data;
        private ArrayList<PopupModel> arraylist;

        private LayoutInflater inflater = null;
        PopupModel tempValues = null;
        int i = 0;

        public ListAdpater(Context a, ArrayList<PopupModel> d) {

            activity = (Activity) a;

            Log.d("SearchListAdpater", "" + d.toString());
            this.data = d;

            this.arraylist = new ArrayList<PopupModel>();
            this.arraylist.addAll(d);

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        public int getCount() {

            if (data != null) {
                if (data.size() <= 0)
                    return 0;
                else
                    return data.size();
            }
            return 1;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public class ViewHolder {

            public ImageView icon;
            public TextView text;

        }

        public View getView(int position, View convertView, ViewGroup parent) {
            PopupModel model = data.get(position);

            View vi = convertView;
            ViewHolder holder;

            if (convertView == null) {

                vi = inflater.inflate(R.layout.dialog_list_row, null);

                holder = new ViewHolder();
                holder.icon = (ImageView) vi.findViewById(R.id.icon);
                holder.text = (TextView) vi.findViewById(R.id.text);

                holder.icon.setImageResource(model.getRes());
                holder.text.setText(""+model.getTitle());

                vi.setTag(holder);

            } else
                holder = (ViewHolder) vi.getTag();

            return vi;
        }

    }
}
