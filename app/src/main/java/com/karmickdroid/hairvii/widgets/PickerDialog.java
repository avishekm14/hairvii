package com.karmickdroid.hairvii.widgets;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.libs.debug.DBG;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 9/28/2015.
 */
public class PickerDialog {
    final static String KEY ="time";
    final static String EXTRA_TIME="extra_time";
    static TextView button_left;
    static TextView button_right,action_bar_title;
    static RelativeLayout button_right_rl;
    static RelativeLayout button_left_rl;

    public static void showPickerOnePopup(final Context ctx,String title,String yes,String no, ArrayList<HashMap<String,String>> kvArr,
                                          final PickerOneCallback obj) {
        final Dialog dialog = new Dialog(ctx, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.custom_dialog_picker_one);
        Window window = dialog.getWindow();
        window.getAttributes().windowAnimations = R.style.dialog_animation;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);

        button_left = (TextView) dialog.findViewById(R.id.ab_left_button);
        button_right = (TextView) dialog.findViewById(R.id.ab_right_button);
        button_left_rl = (RelativeLayout) dialog.findViewById(R.id.ab_left_button_rl);
        button_right_rl = (RelativeLayout) dialog.findViewById(R.id.ab_right_button_rl);
        action_bar_title= (TextView) dialog.findViewById(R.id.action_bar_title);


        button_left.setText(no);
        button_right.setText(yes);
        action_bar_title.setText(title);

        final NumberPicker picker = (NumberPicker) dialog.findViewById(R.id.numberPicker1);

        button_left_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        button_right_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                obj.onOkClick(picker.getValue());
            }
        });


        String[] values = {"-No Item-"};
        if (kvArr != null && !kvArr.isEmpty()) {
            picker.setMinValue(0);
            picker.setMaxValue(kvArr.size() - 1);
            values = getStrArray(kvArr);
        } else {
            picker.setMinValue(0);
            picker.setMaxValue(1);
        }

        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                EditText et = ((EditText) picker.getChildAt(0));
                et.setTextColor(ctx.getResources().getColor(R.color.red));
            }
        });
        picker.setDisplayedValues(values);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color, String[] array)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if((child instanceof EditText) && array[i].equalsIgnoreCase("y")){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                }
                catch(NoSuchFieldException e){
                    DBG.printStackTrace( e);
                }
                catch(IllegalArgumentException e){
                    DBG.printStackTrace(e);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public interface PickerOneCallback {
        public void onOkClick(int i);
    }

    public static String[] getStrArray(ArrayList<HashMap<String, String>> country_list){
        String[] key_name= new String[country_list.size()];

        for(int i=0; i<country_list.size(); i++){
            HashMap<String, String> country_details=country_list.get(i);
            key_name[i]=country_details.get(KEY);
        }
        return key_name;
    }

    public static String[] getExtraTimeArray(ArrayList<HashMap<String, String>> country_list){
        String[] key_name= new String[country_list.size()];

        for(int i=0; i<country_list.size(); i++){
            HashMap<String, String> country_details=country_list.get(i);
            key_name[i]=country_details.get(EXTRA_TIME);
        }
        return key_name;
    }

}
