package com.karmickdroid.hairvii.widgets;

/**
 * Created by Administrator on 3/1/2016.
 */
public class SearchKeyValue {
    private String key;
    private String value;

    public SearchKeyValue(String k, String v) {
        this.key = k;
        this.value = v;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[{\" key\" : \"" + key + "\"},{\" value\" : \"" + value + "\"}]";
    }
}
