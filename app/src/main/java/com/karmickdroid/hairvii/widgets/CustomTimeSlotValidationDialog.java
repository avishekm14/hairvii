package com.karmickdroid.hairvii.widgets;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.ServicesBookedLinearAdapter;
import com.karmickdroid.hairvii.model.AppointmentService;
import com.karmickdroid.hairvii.model.TimeSlot;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Administrator on 4/27/2016.
 */
public class CustomTimeSlotValidationDialog extends DialogFragment {

    private View mView;
    private AdapterLinearLayout service_detail;
    TextView book_time_slot, book_date, total_price_price,extraHourCharge,tvExtraTimeCharge;
    EditText et_special_comment, et_address_for_home_service;
    ImageView iv_cancel;
    Button btn_search;
    ServicesBookedLinearAdapter mAdapter;
    List<AppointmentService> mList;
    OnSubmitClickListener mListener;
    String json, home_service_business;

    public CustomTimeSlotValidationDialog() {

    }

    public void setOnSubmitClickListener(OnSubmitClickListener mListener, String json) {
        this.mListener = mListener;
        this.json = json;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.time_slot_detail, container);
        init(mView);
        parseJson();
        return mView;
    }

    private void parseJson() {
        try {
            DecimalFormat decimal = new DecimalFormat("0.00");

            JSONObject jsonObject = new JSONObject(((String) json));
            JSONObject responceObject = jsonObject.getJSONObject("validate_appotime");
            JSONObject responceObject1 = responceObject.getJSONObject("Details");

            float total_cost = Float.parseFloat(responceObject1.getJSONObject("Appointment").optString("total_amount"));

            book_date.setText(""+responceObject1.getJSONObject("Appointment").optString("appointment_date"));
            total_price_price.setText("$"+decimal.format(total_cost));
            book_time_slot.setText(""+responceObject.getJSONObject("postvar").optString("appo_time"));
            home_service_business=responceObject.optString("home_service_business");

            Gson gson = new Gson();
            Type listType = new TypeToken<List<AppointmentService>>() {
            }.getType();

            String data = responceObject1.getJSONArray("AppointmentService").toString();
            mList = gson.fromJson(data, listType);

            mAdapter = new ServicesBookedLinearAdapter(getActivity(), mList);
            service_detail.setAdapter(mAdapter);

            float cost=0;
            for(AppointmentService mModel: mList){
                if(mModel.getExtraHourCharge()!=null && !mModel.getExtraHourCharge().isEmpty()){
                    cost = cost + Float.parseFloat(mModel.getExtraHourCharge());
                }
            }

            if(cost>0){
                extraHourCharge.setText("$"+decimal.format(cost));
            }else{
                extraHourCharge.setVisibility(View.GONE);
                tvExtraTimeCharge.setVisibility(View.GONE);
            }

            if(home_service_business.equalsIgnoreCase("Y")){
                et_address_for_home_service.setVisibility(View.VISIBLE);
            }else{
                et_address_for_home_service.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void init(View mView) {
        service_detail = (AdapterLinearLayout) mView.findViewById(R.id.service_detail);
        book_time_slot = (TextView) mView.findViewById(R.id.book_time_slot);
        extraHourCharge = (TextView) mView.findViewById(R.id.extraHourCharge);
        tvExtraTimeCharge = (TextView) mView.findViewById(R.id.tvExtraTimeCharge);
        book_date = (TextView) mView.findViewById(R.id.book_date);
        total_price_price = (TextView) mView.findViewById(R.id.total_price_price);
        iv_cancel = (ImageView) mView.findViewById(R.id.iv_cancel);
        btn_search  = (Button) mView.findViewById(R.id.btn_search);
        et_special_comment = (EditText) mView.findViewById(R.id.et_special_comment);
        et_address_for_home_service = (EditText) mView.findViewById(R.id.et_address_for_home_service);

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHideSoftKeyboard();
                dismiss();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(home_service_business.equalsIgnoreCase("Y")) {
                    setHideSoftKeyboard();
                    if(!et_address_for_home_service.getText().toString().trim().isEmpty()) {
                        dismiss();
                        mListener.onClick(json, et_special_comment.getText().toString().trim(),
                                et_address_for_home_service.getText().toString().trim());
                    }else {
                        Toast.makeText(getContext(), "Address Should Not Be Empty.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    dismiss();
                    mListener.onClick(json, et_special_comment.getText().toString().trim(),
                            et_address_for_home_service.getText().toString().trim());
                }
            }
        });

    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Fetch arguments from bundle and set title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Show soft keyboard automatically and request focus to field
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }

    public interface OnSubmitClickListener {
        void onClick(String json, String specialComment, String addres);
    }

    public void setHideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_special_comment.getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
