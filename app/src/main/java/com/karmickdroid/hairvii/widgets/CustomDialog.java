package com.karmickdroid.hairvii.widgets;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.BaseActivity;
import com.karmickdroid.hairvii.activity.ServiceDetailActivity;
import com.karmickdroid.hairvii.adapter.HairTypeAdapter;
import com.karmickdroid.hairvii.libs.debug.DBG;
import com.karmickdroid.hairvii.model.ListModel;
import com.karmickdroid.hairvii.util.BoldTextView;
import com.karmickdroid.hairvii.util.HairType;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

/**
 * Created by Administrator on 2/5/2016.
 */
public class CustomDialog extends DialogFragment implements View.OnClickListener, HairTypeAdapter.OnItemClickListener {
    static CustomDialog frag;
    BaseActivity mActivity;
    Context mContext;
    View mView;
    RatingBar ratingbar;
    EditText et_business, et_distance;
    CheckBox ck_free_parking, ck_beer_parking, ck_kid_friendly, ck_tv, ck_wifi_access,
            ck_accept_walkin, ck_handicap_access, ck_home_delivery;
    Button btn_search, btn_cancel;
    ImageView iv_cancel;
    static OnSubmitCallback onSubmitClick;
    SearchSpinner sObj;
    String business_Id = "";
    TextView clear_selection;
    static String business = "";
    LinearLayout hairType,checkList;
    HairTypeAdapter mAdapter;
    static List<HairType> hairTypeList;
    int[] ids = {100,101,102,103,104,105,106,107};
    CheckBox[] checkBoxes = {ck_free_parking, ck_wifi_access,ck_beer_parking,ck_handicap_access,
            ck_kid_friendly,ck_accept_walkin,ck_home_delivery,ck_tv};
    String[] strings = {"Free Parking","WIFI access","Beer and wine bar","Handicap Access",
    "Kid Friendly","Accepts Walk-in","Home Service","T.V"};

    public CustomDialog() {

    }

    public static CustomDialog newInstance(OnSubmitCallback onSubmitListner, String title) {
        onSubmitClick = onSubmitListner;
        business = title;

        if (frag == null) {
            frag = new CustomDialog();
            frag.setCancelable(false);
            Bundle args = new Bundle();
            frag.setArguments(args);
            hairTypeList = Paper.book().read("hairtype");
        }
        return frag;
    }

    public CustomDialog setData(BaseActivity mActivity) {
        if (frag == null) {
            frag = new CustomDialog();
            frag.setCancelable(false);
            Bundle args = new Bundle();
            frag.setArguments(args);
            hairTypeList = Paper.book().read("hairtype");
        }
        return frag;
    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.filter_layout, container);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        ratingbar = (RatingBar) mView.findViewById(R.id.ratingbar);
        et_business = (EditText) mView.findViewById(R.id.et_business);
        et_distance = (EditText) mView.findViewById(R.id.et_distance);
        iv_cancel = (ImageView) mView.findViewById(R.id.iv_cancel);
        hairType = (LinearLayout) mView.findViewById(R.id.hairType);
        checkList = (LinearLayout) mView.findViewById(R.id.checkList);

        /*mAdapter = new HairTypeAdapter(getActivity(), hairTypeList);
        mAdapter.setOnItemClickListener(this);
        hairType.setAdapter(mAdapter);*/

        createCheckBox();

        for (HairType hairTypeModel : hairTypeList) {
            RelativeLayout parent = new RelativeLayout(getContext());
            parent.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            //parent.setOrientation(LinearLayout.HORIZONTAL);

            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            CheckBox checkBox=(CheckBox)getActivity().getLayoutInflater().inflate(R.layout.checkbox, null);
            checkBox.setLayoutParams(lparams);
            checkBox.setId(Integer.parseInt(hairTypeModel.getId()));
            parent.addView(checkBox);

            RelativeLayout.LayoutParams lparams1 = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            lparams1.addRule(RelativeLayout.RIGHT_OF, checkBox.getId());
            lparams1.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

            BoldTextView textView = new BoldTextView(getContext());
            textView.setLayoutParams(lparams1);
            textView.setText("" + hairTypeModel.getName());
            parent.addView(textView);

            hairType.addView(parent);
        }

        btn_search = (Button) mView.findViewById(R.id.btn_search);
        btn_cancel = (Button) mView.findViewById(R.id.btn_cancel);
        iv_cancel = (ImageView) mView.findViewById(R.id.iv_cancel);

        btn_search.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        iv_cancel.setOnClickListener(this);
        et_business.setOnClickListener(this);

        if(!business.equalsIgnoreCase("Recommendation"))
        et_business.setText("" + business);
    }

    private void createCheckBox() {
        for(int i=0;i<ids.length;i++) {
            RelativeLayout parent = new RelativeLayout(getContext());
            parent.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            //parent.setOrientation(LinearLayout.HORIZONTAL);

            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            checkBoxes[i] = (CheckBox) getActivity().getLayoutInflater().inflate(R.layout.checkbox, null);
            checkBoxes[i].setLayoutParams(lparams);
            checkBoxes[i].setId(ids[i]);
            parent.addView(checkBoxes[i]);

            RelativeLayout.LayoutParams lparams1 = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            lparams1.addRule(RelativeLayout.RIGHT_OF, checkBoxes[i].getId());
            lparams1.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

            BoldTextView textView = new BoldTextView(getContext());
            textView.setLayoutParams(lparams1);
            textView.setText(""+strings[i]);
            parent.addView(textView);

            checkList.addView(parent);
        }

        ck_free_parking = (CheckBox)checkList.findViewById(ids[0]);
        ck_wifi_access = (CheckBox)checkList.findViewById(ids[1]);
        ck_beer_parking = (CheckBox)checkList.findViewById(ids[2]);
        ck_handicap_access = (CheckBox)checkList.findViewById(ids[3]);
        ck_kid_friendly = (CheckBox)checkList.findViewById(ids[4]);
        ck_accept_walkin = (CheckBox)checkList.findViewById(ids[5]);
        ck_home_delivery = (CheckBox)checkList.findViewById(ids[6]);
        ck_tv = (CheckBox)checkList.findViewById(ids[7]);
    }

    private void resetValue() {
        ratingbar.setRating(0);
        et_business.setText("");
        et_distance.setText("");
        ck_free_parking.setChecked(false);
        ck_beer_parking.setChecked(false);
        ck_kid_friendly.setChecked(false);
        ck_home_delivery.setChecked(false);
        ck_tv.setChecked(false);
        ck_wifi_access.setChecked(false);
        ck_accept_walkin.setChecked(false);
        ck_handicap_access.setChecked(false);

        for (HairType hairTypeModel : hairTypeList) {
            CheckBox checkBox = (CheckBox) hairType.findViewById(Integer.parseInt(hairTypeModel.getId()));
            checkBox.setChecked(false);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Fetch arguments from bundle and set title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Show soft keyboard automatically and request focus to field
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search:
                setHideSoftKeyboard();
                SearchData data = new SearchData();

                data.setRating((int)ratingbar.getRating());

                if(!business_Id.equalsIgnoreCase("")) {
                    data.setBusiness("" + business_Id);
                }else{
                    data.setBusiness("0");
                }

                if (!et_distance.getText().toString().trim().isEmpty())
                    data.setDistance(et_distance.getText().toString().trim());
                else{
                    data.setDistance("");
                }

                if (ck_accept_walkin.isChecked()) {
                    data.setAccept_walkin(1);
                }

                if (ck_beer_parking.isChecked()) {
                    data.setBeer_and_wine(1);
                }

                if (ck_free_parking.isChecked()) {
                    data.setFree_parking(1);
                }

                if (ck_handicap_access.isChecked()) {
                    data.setHandicap_access(1);
                }

                if (ck_kid_friendly.isChecked()) {
                    data.setKid_friendly(1);
                }

                if (ck_home_delivery.isChecked()) {
                    data.setHome_service(1);
                }

                if (ck_tv.isChecked()) {
                    data.setTv(1);
                }

                if (ck_wifi_access.isChecked()) {
                    data.setWifi_access(1);
                }

                StringBuilder str = new StringBuilder("");
                int i = 0;
                for (HairType hairTypeModel : hairTypeList) {
                    CheckBox checkBox = (CheckBox) hairType.findViewById(Integer.parseInt(hairTypeModel.getId()));
                    if (checkBox.isChecked()) {
                        if (i == 0) {
                            str.append(hairTypeModel.getId());
                            i++;
                        } else {
                            str.append("," + hairTypeModel.getId());
                        }
                    }
                }

                data.setIds(str.toString());
                onSubmitClick.onSubmit(data);
                frag.dismiss();
                break;
            case R.id.iv_cancel:
                setHideSoftKeyboard();
                frag.dismiss();
                onSubmitClick.onCancel();
                break;
            case R.id.btn_cancel:
                setHideSoftKeyboard();
                resetValue();
                onSubmitClick.onDataReset();
                frag.dismiss();
                break;
            case R.id.et_business:
                setHideSoftKeyboard();
                showBusinessList();
                break;
        }
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onItemChecked(int position) {

    }

    public interface OnSubmitCallback {
        void onSubmit(SearchData data);
        void onDataReset();
        void onCancel();
    }

    public void showBusinessList() {
        List<ListModel> businessList = Paper.book().read("business");

        ArrayList<SearchKeyValue> keyValList = new ArrayList<>();

        if (null != businessList && !businessList.isEmpty()) {
            for (ListModel model : businessList) {
                keyValList.add(new SearchKeyValue("" + model.getId(), "" + model.getCategory_name()));

                if(!business.equalsIgnoreCase("Recommendation")) {
                    if (business.equalsIgnoreCase(model.getCategory_name())) {
                        business_Id = model.getId();
                        DBG.d("AAAAAA", "Business_id" + business_Id);
                    }
                }
            }

            sObj = new SearchSpinner();
            sObj.searchSpinner(getActivity(), et_business, keyValList, false, new SearchSpinner.CustomSearchDialogCallback() {
                @Override
                public void onItemClick(SearchKeyValue svObj) {
                    et_business.setText("" + svObj.getValue());
                    business_Id = svObj.getKey();
                }
            });
        }
    }

    public void onDispose() {
        frag = null;
    }

    public void setHideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_distance.getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
