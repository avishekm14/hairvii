package com.karmickdroid.hairvii.widgets;

import android.support.v4.app.Fragment;

/**
 * Created by Administrator on 5/26/2015.
 */
public class NavDrawerModel {
    private String title, pageTag;
    private int leftIcon, rightIcon;
    private Fragment fragment;
    private boolean isSelectedRow;
    private CallBack callBack;

    public interface CallBack {
       void onCallback();
    }

    public NavDrawerModel() {
    }

    public NavDrawerModel(String title) {
        this.title = title;
    }

    public NavDrawerModel(String title, boolean isSelectedRow, CallBack callBack) {
        this.title = title;
        this.callBack = callBack;
        this.isSelectedRow = isSelectedRow;
    }

    public NavDrawerModel(int leftIcon, String title, boolean isSelectedRow, CallBack callBack) {
        this.title = title;
        this.leftIcon = leftIcon;
        this.callBack = callBack;
        this.isSelectedRow = isSelectedRow;
    }

    public NavDrawerModel(int leftIcon, String title, int rightIcon) {
        this.title = title;
        this.leftIcon = leftIcon;
        this.rightIcon = rightIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPageTag() {
        return pageTag;
    }

    public void setPageTag(String pageTag) {
        this.pageTag = pageTag;
    }

    public int getLeftIcon() {
        return leftIcon;
    }

    public void setLeftIcon(int leftIcon) {
        this.leftIcon = leftIcon;
    }

    public int getRightIcon() {
        return rightIcon;
    }

    public void setRightIcon(int rightIcon) {
        this.rightIcon = rightIcon;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public boolean getIsSelectedRow() {
        return isSelectedRow;
    }

    public void setSelectedRow(boolean isSelectedRow) {
        this.isSelectedRow = isSelectedRow;
    }

    public CallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }
}