package com.karmickdroid.hairvii.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.adapter.SearchListAdpater;
import com.karmickdroid.hairvii.model.Services;
import com.karmickdroid.hairvii.model.SubServices;
import com.karmickdroid.hairvii.util.BoldTextView;
import com.karmickdroid.hairvii.util.HairType;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Administrator on 3/1/2016.
 */
public class SearchSpinnerWithCheckbox {

    private SearchListAdpater dataAdapter = null;
    PopupWindow popupWindow;
    View search_spinner;
    ArrayList<SubServices> keyValList;
    CustomSearchDialogCallback obj;
    private LinearLayout listView;

    public void searchSpinner(final Context ctx, View search_spinner, final ArrayList<SubServices> keyValList, final boolean showEdiText, final CustomSearchDialogCallback obj) {
        LayoutInflater layoutInflater
                = (LayoutInflater) ctx
                .getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.search_spinner1, null);
        popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        this.keyValList = keyValList;
        this.obj = obj;
        this.search_spinner = search_spinner;

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.update();
        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        popupWindow.setWidth(search_spinner.getWidth() + 20);

        //popupWindow.setAnimationStyle(R.style.spinner_animation);

        listView = (LinearLayout) popupView.findViewById(R.id.listView1);

        for (int i=0;i<keyValList.size();i++) {
            SubServices subServices = keyValList.get(i);

            RelativeLayout parent = new RelativeLayout(ctx);
            parent.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            //parent.setOrientation(LinearLayout.HORIZONTAL);

            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            final CheckBox checkBox =(CheckBox)((DetailActivity)ctx).getLayoutInflater().inflate(R.layout.checkbox, null);
            checkBox.setLayoutParams(lparams);
            checkBox.setId(i+1);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    keyValList.get(checkBox.getId()-1).setChecked(isChecked);

                    StringBuilder services = new StringBuilder("");
                    StringBuilder servicesIds = new StringBuilder("");

                    int i=0;
                    for (SubServices service:keyValList) {

                        if (service.isChecked()) {
                            if (i == 0) {
                                services.append("" + service.getService_name());
                                servicesIds.append("" + service.getId());
                                i++;
                            } else {
                                services.append("; " + service.getService_name());
                                servicesIds.append("," + service.getId());
                            }
                        }
                    }

                    obj.onItemClick(services.toString(), servicesIds.toString());
                }
            });
            checkBox.setChecked(subServices.isChecked());
            parent.addView(checkBox);

            RelativeLayout.LayoutParams lparams1 = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            lparams1.addRule(RelativeLayout.RIGHT_OF, checkBox.getId());
            lparams1.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);

            BoldTextView textView = new BoldTextView(ctx);
            textView.setLayoutParams(lparams1);
            textView.setText("" + subServices.getService_name());
            parent.addView(textView);

            listView.addView(parent);
        }


        final EditText myFilter = (EditText) popupView.findViewById(R.id.myFilter);
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });

        if (!showEdiText) {
            myFilter.setVisibility(View.GONE);
            myFilter.setFocusable(false);
        }

        popupWindow.showAsDropDown(search_spinner, -10, -ctx.getResources().getDimensionPixelSize(R.dimen.edittext_height));
    }

    public void show(Context ctx){
        popupWindow.showAsDropDown(search_spinner, -10, -ctx.getResources().getDimensionPixelSize(R.dimen.edittext_height));
    }

    public void closePopup(){
        if(popupWindow !=null){
            popupWindow.dismiss();
        }
        popupWindow= null;
    }

    public interface CustomSearchDialogCallback {
        public void onItemClick(String Service, String ids);
    }

}