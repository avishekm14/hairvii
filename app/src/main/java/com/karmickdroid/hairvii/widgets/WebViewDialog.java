package com.karmickdroid.hairvii.widgets;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;

import com.karmickdroid.hairvii.R;


public class WebViewDialog extends Dialog{


    DialogBuiler dialogBuiler;


    public WebViewDialog(Context context) {
        super(context);
    }

    public WebViewDialog(Context context, int themeResId) {
        super(context, themeResId);

        setContentView(R.layout.terms);
        Window window = getWindow();
        //window.getAttributes().windowAnimations = R.style.dialog_animation;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

    protected WebViewDialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public DialogBuiler getDialogBuiler() {
        return dialogBuiler;
    }

    public void setDialogBuiler(DialogBuiler dialogBuiler) {
        this.dialogBuiler = dialogBuiler;
    }

    private void setDynamicLayout(String str) {
        final Dialog mView = WebViewDialog.this;
        WebView browser = (WebView) mView.findViewById(R.id.terms);
        //browser.getSettings().setJavaScriptEnabled(true);
        browser.loadData(str, "text/html", "UTF-8");
    }

    public static class DialogBuiler {

        private int position;
        private Context context;
        private String str;

        public DialogBuiler setServiceList(String str) {
            this.str = str;
            return this;

        }

        public DialogBuiler setPosition(int position) {
            this.position = position;
            return this;
        }

        public DialogBuiler setContext(Context context) {
            this.context = context;
            return this;
        }

        public void showDialog() {
            WebViewDialog dialogObj = new WebViewDialog(this.context, R.style.FullHeightDialog);
            dialogObj.setDynamicLayout(str);
            dialogObj.setDialogBuiler(this);
            dialogObj.show();
        }
    }

}
