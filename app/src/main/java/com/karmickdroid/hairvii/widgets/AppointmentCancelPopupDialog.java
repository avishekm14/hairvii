package com.karmickdroid.hairvii.widgets;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.activity.DetailActivity;
import com.karmickdroid.hairvii.model.AppointmentCancelModel;
import com.karmickdroid.hairvii.model.PopupModel;

import java.util.ArrayList;

/**
 * Created by Administrator on 4/2/2016.
 */
public class AppointmentCancelPopupDialog extends DialogFragment {
    AppointmentCancelModel mModel;
    Context ctx;
    CustomDialogCallback obj;
    View mView;
    TextView service_amount, cancellation_fee, procesing_fee, refund_amount, transaction_id;
    EditText et_cancellation_comment;
    ImageView iv_cancel;
    Button btn_search;

    public AppointmentCancelPopupDialog(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.appointment_cancel_layout, container);
        init(mView);
        return mView;
    }

    private void init(View mView) {
        service_amount = (TextView) mView.findViewById(R.id.service_amount);
        cancellation_fee = (TextView) mView.findViewById(R.id.cancellation_fee);
        procesing_fee = (TextView) mView.findViewById(R.id.procesing_fee);
        refund_amount = (TextView) mView.findViewById(R.id.refund_amount);
        transaction_id = (TextView) mView.findViewById(R.id.transaction_id);
        et_cancellation_comment = (EditText) mView.findViewById(R.id.et_cancellation_comment);
        btn_search = (Button) mView.findViewById(R.id.btn_search);
        iv_cancel = (ImageView) mView.findViewById(R.id.iv_cancel);

        service_amount.setText(""+mModel.getService_amount());
        cancellation_fee.setText(""+mModel.getCancellation_fee());
        procesing_fee.setText(""+mModel.getProcessing_fee());
        refund_amount.setText(""+mModel.getRefund_amount());
        transaction_id.setText(""+mModel.getReference_id());

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHideSoftKeyboard();
                dismiss();
                if (obj != null) {
                    obj.onItemClick(mModel, et_cancellation_comment.getText().toString().trim());
                }
            }
        });

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHideSoftKeyboard();
                dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Fetch arguments from bundle and set title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Show soft keyboard automatically and request focus to field
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }

    public void setOnCustomDialogCallback(CustomDialogCallback obj,Context mContext, AppointmentCancelModel mModel){
        this.ctx = mContext;
        this.mModel = mModel;
        this.obj = obj;
    }

    public interface CustomDialogCallback {
        void onItemClick(AppointmentCancelModel mModel, String msg);
    }

    public void setHideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_cancellation_comment.getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
