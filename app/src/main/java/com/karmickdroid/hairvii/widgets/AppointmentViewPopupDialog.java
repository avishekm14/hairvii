package com.karmickdroid.hairvii.widgets;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;
import com.karmickdroid.hairvii.adapter.AppointServicesLinearAdapter;
import com.karmickdroid.hairvii.model.AppointmentCancelModel;
import com.karmickdroid.hairvii.model.AppointmentViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 4/2/2016.
 */
public class AppointmentViewPopupDialog extends DialogFragment {
    AppointmentViewModel mModel;
    Context ctx;
    CustomDialogCallback obj;
    View mView;
    TextView appointment_date, booking_date, basic_amount, processing_fee, total_amount,special_comment;
    TextView cancellation_date, refund_amount, cancelled_by, refund_transaction_id, cancellation_reason,home_service;
    TextView policy_detail,business_name, tx_extra_money;
    ImageView iv_cancel;
    LinearLayout ll_cancellation;
    AdapterLinearLayout services_list;
    private AppointServicesLinearAdapter mAdapter;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

    public AppointmentViewPopupDialog(){

    }

    public void setModelData(Context mContext, AppointmentViewModel mModel){
        this.ctx = mContext;
        this.mModel = mModel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.appointment_view_layout, container);
        try {
            init(mView);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mView;
    }

    private void init(View mView) throws ParseException {
        ll_cancellation = (LinearLayout) mView.findViewById(R.id.ll_cancellation);
        appointment_date = (TextView) mView.findViewById(R.id.appointment_date);
        booking_date = (TextView) mView.findViewById(R.id.booking_date);
        basic_amount = (TextView) mView.findViewById(R.id.basic_amount);
        processing_fee = (TextView) mView.findViewById(R.id.processing_fee);
        total_amount = (TextView) mView.findViewById(R.id.total_amount);
        home_service = (TextView) mView.findViewById(R.id.home_service);
        business_name = (TextView) mView.findViewById(R.id.business_name);
        tx_extra_money = (TextView) mView.findViewById(R.id.tx_extra_money);
        special_comment = (TextView) mView.findViewById(R.id.special_comment);
        iv_cancel = (ImageView) mView.findViewById(R.id.iv_cancel);

        services_list = (AdapterLinearLayout) mView.findViewById(R.id.services_list);

        cancellation_date = (TextView) mView.findViewById(R.id.cancellation_date);
        refund_amount = (TextView) mView.findViewById(R.id.refund_amount);
        cancelled_by = (TextView) mView.findViewById(R.id.cancelled_by);
        refund_transaction_id = (TextView) mView.findViewById(R.id.refund_transaction_id);
        cancellation_reason = (TextView) mView.findViewById(R.id.cancellation_reason);
        policy_detail = (TextView) mView.findViewById(R.id.policy_detail);

        appointment_date.setText(""+mModel.getAppointment_date());
        booking_date.setText(""+mModel.getBooking_date());
        basic_amount.setText(""+mModel.getService_amount());
        processing_fee.setText(""+mModel.getProcessing_fee());
        total_amount.setText("" + mModel.getTotal_amount());
        business_name.setText("" + mModel.getBusiness_name());

        if(mModel.getNote()!=null && !mModel.getNote().isEmpty()){
            special_comment.setText(""+mModel.getNote());
        }else {
            special_comment.setText("No Instruction Available");
        }

        if(mModel.getExtra_hours_charge()!=null && !mModel.getExtra_hours_charge().isEmpty()){
                if(!mModel.getExtra_hours_charge().equalsIgnoreCase("0")) {
                    home_service.setText("" + mModel.getExtra_hours_charge());
                }else{
                    home_service.setVisibility(View.GONE);
                    tx_extra_money.setVisibility(View.GONE);
                }
        }else{
            home_service.setVisibility(View.GONE);
            tx_extra_money.setVisibility(View.GONE);
        }

        if(mModel.getCancelled_by_customer().equalsIgnoreCase("Y") || mModel.getCancelled_by_business().equalsIgnoreCase("Y")){
            ll_cancellation.setVisibility(View.VISIBLE);
            Date date = simpleDateFormat.parse(mModel.getCancellation_date());
            cancellation_date.setText("" +simpleDateFormat1.format(date) );
            refund_amount.setText(""+ mModel.getRefund_amount());
            if(mModel.getCancelled_by_customer().equalsIgnoreCase("Y")) {
                cancelled_by.setText("Customer");
            }else{
                cancelled_by.setText("Service Provider");
            }

            if(mModel.getRefund_flag().equalsIgnoreCase("0")) {
                refund_transaction_id.setText("Refund On Process.");
            }else{
                refund_transaction_id.setText("" + mModel.getRefund_transaction_id());
            }

            cancellation_reason.setText(""+ mModel.getCancellation_reason());

            if(mModel.getPolicy_text()!=null && !mModel.getPolicy_text().isEmpty()){
                policy_detail.setText(""+mModel.getPolicy_text());
            }else {
                policy_detail.setText("No Policy Found.");
            }

        } else{
            ll_cancellation.setVisibility(View.GONE);
        }

        mAdapter = new AppointServicesLinearAdapter(getActivity(),mModel.getAppointmentServiceList());
        services_list.setAdapter(mAdapter);

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);

        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Fetch arguments from bundle and set title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // Show soft keyboard automatically and request focus to field
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    public void setOnCustomDialogCallback(CustomDialogCallback obj){
        this.obj = obj;
    }

    public interface CustomDialogCallback {
        public void onItemClick(AppointmentCancelModel mModel, String msg);
    }

}
