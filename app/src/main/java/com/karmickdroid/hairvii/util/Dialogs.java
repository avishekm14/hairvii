package com.karmickdroid.hairvii.util;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.hairvii.R;

/**
 * Created by Ajijul Mondal on 02-Feb-16.
 */
public class Dialogs {

    public static void dialogFetchImage(Context context,
                                        final OnOptionSelect _callback) {


        final Dialog dialog = new Dialog(context, R.style.dialog);
        dialog.setContentView(R.layout.dialog_fetch_image);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        dialog.setCanceledOnTouchOutside(false);


        final TextView fetchImage_tvOpenCamera = (TextView) dialog.findViewById(R.id.fetchImage_tvOpenCamera);
        final TextView fetchImage_tvOpenGallery = (TextView) dialog.findViewById(R.id.fetchImage_tvOpenGallery);
        final TextView fetchImage_tvCancel = (TextView) dialog.findViewById(R.id.fetchImage_tvCancel);

        fetchImage_tvOpenCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                _callback.openCamera();
            }
        });
        fetchImage_tvOpenGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                _callback.openGallery();
            }
        });
        fetchImage_tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public interface onDialogOkButtonClickListener {
        void onDialogOkButtonClick();
    }

    public interface YesNoClickListener {
        void onYesClick();

        void onNoClick();

    }

    public interface OnProjectSelect {
        void onSelect(String fNo);
    }

    public interface OnNumberSelect {
        void onSelect(String fNo);
    }

    public interface onProjectCreationDialogNextButtonClickListener {
        void onProjectCreationDialogNextButtonClick(String projectName);
    }

    public interface OnOptionSelect {
        void openCamera();

        void openGallery();
    }
}
