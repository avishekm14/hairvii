package com.karmickdroid.hairvii.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.Log;
import android.view.Window;

import com.karmickdroid.hairvii.libs.debug.DBG;

/**
 * Created by Ajijul Mondal on 27-Jan-16.
 */
public class AppUtilities {
    private static Context context = null;
    public static int DEVICE_SCREEN_WIDTH=480;
    /**
     * Recently set context will be returned.
     * If not set it from current class it will
     * be null.
     *
     * @return Context
     */
    public static final Context getContext() {
        return AppUtilities.context;
    }

    /**
     * First set context from every activity
     * before use any static method of AppUtils class.
     *
     * @param ctx
     */
    public static final void setContext(Context ctx) {
        AppUtilities.context = ctx;
    }

    /**
     * Get String from resource id
     *
     * @param res
     * @return
     */
    public static final String getStringFromResource(int res) {
        Context _ctx = getContext();
        if (null != _ctx) {
            try {
                return _ctx.getResources().getString(res);
            } catch (Resources.NotFoundException e) {
                DBG.printStackTrace(e);
                return "";
            }
        } else {
            DBG.d("CONTEXT null", "");
            return "";
        }
    }

    /**
     * Check for email validation using android default
     * email validator.
     *
     * @param target
     * @return boolean
     */
    public static final boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivity(Class cls) {
        Context _ctx = getContext();
        if (null != _ctx) {
            Intent intent = new Intent(_ctx, cls);
            _ctx.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            DBG.d("CONTEXT null", "");
        }
    }

    public static int getStatusBarHeight() {

        Rect rect = new Rect();
        Window win = ((Activity) getContext()).getWindow();
        win.getDecorView().

                getWindowVisibleDisplayFrame(rect);

        int statusBarHeight = rect.top;
        int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        Log.d("ID-ANDROID-CONTENT", "################################statusBarHeight = " + statusBarHeight);
        if (statusBarHeight == 0) {
            statusBarHeight = 40;
        }
        return statusBarHeight;
    }

    public static int getDimension(int size){
       return getContext().getResources().getDimensionPixelSize(size);
    }
}
