package com.karmickdroid.hairvii.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/** A RelativeLayout that will always be square -- same width and height,
 * where the height is based off the width. */
public class SquareImageLayout extends ImageView {

    public SquareImageLayout(Context context) {
        super(context);
    }

    public SquareImageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SquareImageLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Set a square layout.
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }


}
