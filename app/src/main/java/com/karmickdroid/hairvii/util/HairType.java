package com.karmickdroid.hairvii.util;

/**
 * Created by Administrator on 4/1/2016.
 */
public class HairType {
    String id, name;
    boolean checkBoolean=false;

    public boolean isCheckBoolean() {
        return checkBoolean;
    }

    public void setCheckBoolean(boolean checkBoolean) {
        this.checkBoolean = checkBoolean;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
