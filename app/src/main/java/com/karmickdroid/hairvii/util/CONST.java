package com.karmickdroid.hairvii.util;

/**
 * Created by techierishi on 8/11/15.
 */
public interface CONST {

    String PREF_NAME = "Hairvii";

    public interface REST_API {
        //String BASE_URL = "http://testyourprojects.net/matrix/project/hairvii/webservice/"; //live link
        String BASE_URL = " http://demo.hairvii.com/webservice/";
        String FB_LOGIN = BASE_URL + "customers/fblogin";
        String GPLUS_LOGIN = BASE_URL + "customers/gpluslogin";
        String LOGIN_URL= BASE_URL+ "customers/login";
        String REGISTRATION = BASE_URL+ "customers/registration";
        String COUNTRY = BASE_URL+ "customers/country_list";
        String LIST = BASE_URL+ "categories/list";
        String CHANGE_PASSWORD = BASE_URL+ "customers/change_password";
        String BUSINESS = BASE_URL + "customers/businesses";
        String RATING = BASE_URL+ "customers/rate_business";
        String FAVOURITES = BASE_URL+ "customers/set_unset_favourites";
        String SHARE = BASE_URL+"customers/share_app";
        String FAVOURITES_LIST = BASE_URL+"customers/favourite_list";
        String BUSINESS_DETAIL = BASE_URL+"customers/business_details";
        String HAIR_TYPE= BASE_URL+"customers/hairtypes";
        String EDIT_PROFILE = BASE_URL+"customers/edit_profile";
        String FAQ= BASE_URL +"pages/showpage/how-it-works";
        String GET_PASSWORD = BASE_URL +"customers/forgot_password";
        String ADS = BASE_URL+ "customers/mobileads";
        String ADS_IMPRESSION = BASE_URL + "customers/adv_impression";
        String ADS_CLICK = BASE_URL+ "customers/adv_clickcount";
        String REFFERAL = BASE_URL+ "customers/add_referral";
        String TIME_SLOT = BASE_URL+ "customers/get_available_timeslots";
        String VALIDATE_TIME_SLOT = BASE_URL+ "customers/validate_appotime";
        String APPOINTMENT_BOOKING = BASE_URL+ "customers/appointment_booking";
        String APPOINTMENT_LISTING = BASE_URL+ "customers/appointment_listing";
        String APPOINTMENT_CANCEL_VIEW = BASE_URL+ "customers/appointment_cancel_view";
        String APPOINTMENT_CANCEL = BASE_URL+"customers/appointment_cancel";
        String APPOINTMENT_VIEW = BASE_URL+"customers/appointment_view";
        String BLOG = "http://demo.hairvii.com/customers/autologin/";
        String NEARBY_PLACE = BASE_URL+"customers/nearby";
        String SET_DEVICE_ID = BASE_URL+"customers/set_customer_device";
        String APPOINTMENT_COMPLETE = BASE_URL+ "customers/appointment_complete";
        //String BUSINESS = "http://192.168.1.33/upload/upload.php";
    }

    public interface DB {
        public interface PROFILE {
            String TABLE = "user_profile";
            String USER_ID = "user_id";
            String GENDER = "user_gender";
            String WEIGHT = "user_weight";
            String HEIGHT = "user_height";
            String DOB = "user_dob";

            // Extra
            String USER_NAME = "user_name";
            String FNAME = "f_name";
            String LNAME = "l_name";
            String EMAIL = "user_email";
        }
    }

}
